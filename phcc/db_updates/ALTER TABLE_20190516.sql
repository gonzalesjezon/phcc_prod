ALTER TABLE `pms_specialpayroll_transactions`
	ADD COLUMN `me_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `cna_amount`;