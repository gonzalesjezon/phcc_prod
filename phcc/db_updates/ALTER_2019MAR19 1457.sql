ALTER TABLE `pms_rata`
	ADD COLUMN `hold` INT NULL DEFAULT '0' AFTER `remarks`;

ALTER TABLE `pms_specialpayroll_transactions`
	ADD COLUMN `add_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `amount`,
	ADD COLUMN `less_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `add_amount`;

ALTER TABLE `pms_specialpayroll_transactions`
	CHANGE COLUMN `remarks` `special_remarks` VARCHAR(225) NULL DEFAULT NULL AFTER `month`;