-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table ovp_hris.pms_signatory
DROP TABLE IF EXISTS `pms_signatory`;
CREATE TABLE IF NOT EXISTS `pms_signatory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `signatory_one` varchar(225) DEFAULT NULL,
  `signatory_two` varchar(225) DEFAULT NULL,
  `signatory_three` varchar(225) DEFAULT NULL,
  `signatory_four` varchar(225) DEFAULT NULL,
  `signatory_five` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

ALTER TABLE `pms_signatory`
  ADD COLUMN `signatory_six` VARCHAR(225) NULL DEFAULT NULL AFTER `signatory_five`;

ALTER TABLE `pms_signatory`
  ADD COLUMN `add_degree` VARCHAR(225) NULL DEFAULT NULL AFTER `signatory_six`;
-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
