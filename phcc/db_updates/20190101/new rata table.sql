-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pccqa.pms_rata
DROP TABLE IF EXISTS `pms_rata`;
CREATE TABLE IF NOT EXISTS `pms_rata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `responsibility_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `number_of_actual_work` int(11) DEFAULT NULL,
  `number_of_leave_filed` varchar(225) DEFAULT NULL,
  `percentage_of_rata` varchar(225) DEFAULT NULL,
  `transportation_amount` decimal(9,2) DEFAULT NULL,
  `representation_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pccqa.pms_rata: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_rata` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_rata` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
