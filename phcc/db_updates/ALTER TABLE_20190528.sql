ALTER TABLE `pms_transactions`
	ADD COLUMN `for_month` VARCHAR(225) NULL DEFAULT NULL AFTER `month`;

ALTER TABLE `pms_transactions`
	ADD COLUMN `for_pay_period` VARCHAR(225) NULL DEFAULT NULL AFTER `for_month`;