ALTER TABLE `pms_specialpayroll_transactions`
	ADD COLUMN `job_grade` INT(11) NULL DEFAULT NULL AFTER `employee_number`;
ALTER TABLE `pms_rata`
	ADD COLUMN `job_grade` INT(11) NULL DEFAULT NULL AFTER `position_item_id`;