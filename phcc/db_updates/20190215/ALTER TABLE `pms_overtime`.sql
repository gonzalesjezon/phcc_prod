ALTER TABLE `pms_overtime`
	ADD COLUMN `balance_previous_month` DECIMAL(9,2) NULL DEFAULT NULL AFTER `overtime_pay_for_period`;