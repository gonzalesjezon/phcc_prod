-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table hris.office
DROP TABLE IF EXISTS `office`;
CREATE TABLE IF NOT EXISTS `office` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `IsInterim` int(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `MFO_PAP` varchar(50) DEFAULT NULL,
  `sort_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.office: ~7 rows (approximately)
/*!40000 ALTER TABLE `office` DISABLE KEYS */;
INSERT INTO `office` (`RefId`, `Code`, `Name`, `IsInterim`, `Remarks`, `LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`, `MFO_PAP`, `sort_by`) VALUES
	(1, '', 'Office of the Chairman', 1, 'auto', '2018-04-16', '13:03:16', 'PHP', 'E', NULL, 6),
	(4, '', 'Competition Enforcement Office', 1, 'auto', '2018-04-16', '13:03:16', 'PHP', 'E', NULL, 3),
	(5, '', 'Mergers and Acquisitions Office', 1, 'auto', '2018-04-16', '13:03:16', 'PHP', 'E', NULL, 5),
	(9, '', 'Office of the Executive Director', 1, 'auto', '2018-04-16', '13:03:16', 'PHP', 'E', NULL, 7),
	(10, '', 'Policy Research and Knowledge Management Office', NULL, 'auto', '2017-12-28', '14:29:22', 'PHP', 'M', NULL, 2),
	(12, 'FPMO', 'Finance, Planning and Management Office', NULL, '2018-10-01:auto\nauto', '2018-10-01', '15:23:17', 'rdevaristo.hr', 'E', NULL, 4),
	(21, NULL, 'Administrative and Legal Office', NULL, NULL, '2018-10-31', '11:32:34', 'Admin', 'A', NULL, 1);
/*!40000 ALTER TABLE `office` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
