ALTER TABLE `pms_transactions`
	CHANGE COLUMN `hold` `hold` INT(11) NULL DEFAULT '0' AFTER `pay_period`,
	ADD COLUMN `hold_status` VARCHAR(225) NULL DEFAULT NULL AFTER `hold`;