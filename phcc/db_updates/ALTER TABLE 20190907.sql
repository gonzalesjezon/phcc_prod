ALTER TABLE `pms_transactions`
	CHANGE COLUMN `remittance_remarks` `additional_remarks` VARCHAR(225) NULL DEFAULT NULL AFTER `over_remittance`,
	ADD COLUMN `less_remarks` VARCHAR(225) NULL DEFAULT NULL AFTER `additional_remarks`;