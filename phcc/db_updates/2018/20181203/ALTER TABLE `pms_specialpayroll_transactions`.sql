ALTER TABLE `pms_specialpayroll_transactions`
	ADD COLUMN `cna_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `cash_gift_amount`;