ALTER TABLE `pms_transactions`
	CHANGE COLUMN `actual_absences` `actual_absences` DECIMAL(10,3) NULL DEFAULT NULL AFTER `adjust_workdays`;