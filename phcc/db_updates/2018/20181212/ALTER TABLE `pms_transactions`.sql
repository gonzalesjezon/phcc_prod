ALTER TABLE `pms_transactions`
	ADD COLUMN `additional_tax_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `tax_amount`;