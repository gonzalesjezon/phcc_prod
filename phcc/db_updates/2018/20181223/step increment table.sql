-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pids_hris.pms_step_increments
DROP TABLE IF EXISTS `pms_step_increments`;
CREATE TABLE IF NOT EXISTS `pms_step_increments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `actual_workdays` decimal(10,2) DEFAULT NULL,
  `old_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `new_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `salary_adjustment_amount` decimal(13,2) DEFAULT NULL,
  `gross_pay_amount` decimal(13,2) DEFAULT NULL,
  `gsis_cont_amount` decimal(13,2) DEFAULT NULL,
  `philhealth_cont_amount` decimal(13,2) DEFAULT NULL,
  `provident_fund_amount` decimal(13,2) DEFAULT NULL,
  `wtax_amount` decimal(13,2) DEFAULT NULL,
  `first_deduction_amount` decimal(13,2) DEFAULT NULL,
  `second_deduction_amount` decimal(13,2) DEFAULT NULL,
  `third_deduction_amount` decimal(13,2) DEFAULT NULL,
  `fourth_deduction_amount` decimal(13,2) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
