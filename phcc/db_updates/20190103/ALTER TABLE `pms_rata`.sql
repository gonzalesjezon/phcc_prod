ALTER TABLE `pms_rata`
	ADD COLUMN `ra_diff_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `representation_amount`;
ALTER TABLE `pms_rata`
	ADD COLUMN `ta_diff_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `ra_diff_amount`;