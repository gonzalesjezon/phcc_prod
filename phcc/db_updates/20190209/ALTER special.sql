ALTER TABLE `pms_specialpayroll_transactions`
	ADD COLUMN `rate` DECIMAL(9,3) NULL DEFAULT NULL AFTER `percentage`;