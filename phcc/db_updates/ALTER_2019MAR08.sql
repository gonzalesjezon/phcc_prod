ALTER TABLE `pms_transactions`
	ADD COLUMN `net_first_half` DECIMAL(9,2) NULL DEFAULT NULL AFTER `net_pay`,
	ADD COLUMN `net_second_half` DECIMAL(9,2) NULL DEFAULT NULL AFTER `net_first_half`;