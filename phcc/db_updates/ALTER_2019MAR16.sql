ALTER TABLE `pms_rata`
	ADD COLUMN `ra_diff_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `representation_amount`;
	ADD COLUMN `ta_diff_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `ra_diff_amount`;
	ADD COLUMN `number_of_leave_filed` VARCHAR(225) NULL DEFAULT NULL AFTER `ta_diff_amount`;
	ADD COLUMN `remarks` VARCHAR(225) NULL DEFAULT NULL AFTER `number_of_leave_filed`;