ALTER TABLE `pms_transactions`
	CHANGE COLUMN `over_remittance` `additional_adj` DECIMAL(9,2) NULL DEFAULT NULL AFTER `gsis_loan`,
	ADD COLUMN `deduction_adj` DECIMAL(9,2) NULL DEFAULT NULL AFTER `additional_adj`;