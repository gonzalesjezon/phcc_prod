ALTER TABLE `pms_overtime`
	ADD COLUMN `used_amount` DECIMAL(9,2) NULL DEFAULT NULL AFTER `max_amount_to_avail`;
ALTER TABLE `pms_loansinfo`
	ADD COLUMN `terminated` INT NULL DEFAULT 0 AFTER `loan_date_terminated`,
	ADD COLUMN `created_by` INT NULL DEFAULT NULL AFTER `terminated`;
ALTER TABLE `pms_deductioninfo`
	ADD COLUMN `terminated` INT NULL DEFAULT 0 AFTER `deduct_date_terminated`,
	ADD COLUMN `created_by` INT NULL DEFAULT NULL AFTER `terminated`;
ALTER TABLE `pms_transactions`
	ADD COLUMN `over_remittance` DECIMAL(9,2) NULL DEFAULT NULL AFTER `gsis_loan`;