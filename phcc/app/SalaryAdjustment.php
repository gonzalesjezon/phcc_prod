<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryAdjustment extends Model
{
    protected $table = 'pms_salary_adjustments';
    protected $fillable = [

		'employee_id',
		'employee_number',
		'old_basic_pay_amount',
		'new_basic_pay_amount',
		'salary_adjustment_amount',
		'gsis_cont_amount',
		'philhealth_cont_amount',
		'provident_fund_amount',
		'wtax_amount',
		'first_deduction_amount',
		'second_deduction_amount',
		'third_deduction_amount',
		'fourth_deduction_amount',
		'date_from',
		'date_to',
		'year',
		'month',
		'actual_workdays',
		'adjustment_amount',
		'created_by',
		'updated_by',

    ];

    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }

    public function stepincrements(){
    	return $this->belongsTo('App\StepIncrement','employee_id','employee_id');
    }
}
