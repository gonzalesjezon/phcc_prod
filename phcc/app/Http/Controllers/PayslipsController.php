<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\LoanInfo;
use App\Transaction;
use App\EmployeeInfo;
use App\BenefitInfo;
use App\DeductionInfo;
use App\SalaryInfo;
use App\LoanInfoTransaction;
use App\BenefitInfoTransaction;
use App\DeductionInfoTransaction;
use App\SpecialPayrollTransaction;
use App\Company;
use App\Office;
use App\Division;
use App\Department;
use App\Position;
use App\AttendanceInfo;
use App\Benefit;
use App\PostedReport;
use App\Signatory;
use App\Rata;
use Input;
use Auth;

class PayslipsController extends Controller
{
    function __construct(){
    	$this->title = 'PAYSLIP';
    	$this->module = 'payslips';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employeeinfo = Employee::where('active','!=',0)->orderBy('lastname','asc')->get();

        $perPage = 20;
        $posted = PostedReport::where('report_type','payslip')->latest()->get();

    	$response = array(
                        'employeeinfo'  => $employeeinfo,
                        'posted'        => $posted,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    			);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $transaction = Transaction::where('year',$request->year)
        ->where('month',$request->month)
        ->get();


        foreach ($transaction as $key => $value) {

            $posted = PostedReport::where('year',$request->year)
            ->where('employee_id',$value->employee_id)
            ->where('month',$request->month)
            ->where('report_type','payslip')
            ->first();

            if(!$posted){
                $posted = new PostedReport;
                $posted->fill($request->all());
                $posted->employee_id    = $value->employee_id;
                $posted->report_type    = 'payslip';
                $posted->created_by     = Auth::id();

                if($posted->save()){
                    $transaction = Transaction::where('year',$request->year)
                    ->where('month',$request->month)
                    ->where('employee_id',$value->employee_id)
                    ->update(['posted' => 1]);
                }
            }
        }

        return json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
    }

    public function show(){

    }


    public function getPayslip(){
        $data = Input::all();

        // $filtered = $this->filter($data['emp_status'],$data['category'],$data['emp_type'],$data['searchby']);
        $transaction        = new Transaction;
        $loaninfo           = new LoanInfoTransaction;
        $benefitinfo        = new BenefitInfoTransaction;
        $employeeinfo       = new EmployeeInfo;
        $deductioninfo      = new DeductionInfoTransaction;
        $spayroll           = new SpecialPayrollTransaction;
        $salaryinfo         = new SalaryInfo;
        $benefit            = new Benefit;
        $rtransact          = new Rata;

        $year        = $data['year'];
        $month       = $data['month'];
        $employee_id = $data['id'];

        $benefit = $benefit->where('name','PERA')->first();


        if(isset($data['id'])){

            $query['transaction'] = $transaction
            ->with([
                'benefitTransactions' => function($qry) use($benefit,$year,$month){
                    $qry = $qry->where('month',$month)
                    ->where('year',$year)
                    ->where('benefit_id',$benefit->id);
                },
                'benefitinfo' => function($qry) use($benefit){
                    $qry->where('benefit_id',$benefit->id);
                },
                'employees',
                'positionitems',
                'positions',
                'offices'
            ])
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->first();

            $query['spayroll'] = $spayroll
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->get();

            $query['employeeinfo'] = $employeeinfo
            ->with('employees')
            ->where('employee_id',$employee_id)
            ->first();

            $query['loaninfo'] = $loaninfo
            ->with('loans')
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->get();

            $query['benefitinfo'] = $benefitinfo
            ->with('benefits')
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->get();

            $query['deductioninfo'] = $deductioninfo
            ->with('deductions')
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->get();

            $query['salaryinfo'] = $salaryinfo
            ->where('employee_id',$employee_id)
            ->orderBy('salary_effectivity_date','desc')
            ->first();

            $query['rtransact'] = $rtransact
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->first();

            date_default_timezone_set('Asia/Manila');

            $query['date_run'] = date('F d, Y h:i A',time());

            return json_encode($query);

        }
    }


}
