<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\EmployeeInfo;
use App\AttendanceInfo;
class GeneralPayrollReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL';
    	$this->module = 'generalpayroll';
        $this->module_prefix = 'payrolls/reports/plantillareports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new Transaction;
    	$employeeinfo = new EmployeeInfo;
    	$attendanceinfo = new AttendanceInfo;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $query['transaction'] = $transaction
        ->with(['salaryinfo',
        'positions',
        'employeeinfo',
        'employees',
        'benefitTransactions'=>function($qry) use($year,$month){
            $qry = $qry->where('year',$year)
            ->where('month',$month)
            ->where('status','pera');
        }])
		->where('year',$year)
        ->where('month',$month)
        ->get();

       return json_encode($query);
    }
}
