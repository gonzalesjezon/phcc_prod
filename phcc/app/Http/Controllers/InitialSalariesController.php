<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\Employee;
use App\EmployeeInfo;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\BenefitInfo;
use App\Benefit;
use App\SalaryInfo;
use App\InitialSalary;
use App\TaxTable;
use Auth;
class InitialSalariesController extends Controller
{
    function __construct(){
    	$this->title = 'INITIAL SALARY';
    	$this->module = 'initialsalaries';
        $this->module_prefix = 'payrolls/otherpayrolls';
    	$this->controller = $this;

    }

    public function index(){


        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname'];

        $status              = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;
        $employee            = new Employee;

        $status = $status
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $arr_employeeid = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();


        $query = $employee
                ->whereIn('id',$arr_employeeid)
                ->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });


        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function store(Request $request){


        $adjustment = new InitialSalary;
        $transaction_date   = $request->transaction_date;
        $employee_id        = $request->employee_id;
        $transaction_id     = $request->transaction_id;

        $basic_amount = (isset($request->basic_amount)) ? str_replace(',', '', $request->basic_amount) : 0;
        $pera_amount = (isset($request->pera_amount)) ? str_replace(',', '', $request->pera_amount) : 0;
        $gsis_cont_amount = (isset($request->gsis_cont_amount)) ? str_replace(',', '', $request->gsis_cont_amount) : 0;
        $philhealth_cont_amount = (isset($request->philhealth_cont_amount)) ? str_replace(',', '', $request->philhealth_cont_amount) : 0;
        $tax_amount = (isset($request->tax_amount)) ? str_replace(',', '', $request->tax_amount) : 0;
        $gross_amount = (isset($request->gross_amount)) ? str_replace(',', '', $request->gross_amount) : 0;
        $pagibig_amount = (isset($request->pagibig_amount)) ? str_replace(',', '', $request->pagibig_amount) : 0;
        $total_deduction_amount = (isset($request->total_deduction_amount)) ? str_replace(',', '', $request->total_deduction_amount) : 0;
         $net_amount = (isset($request->net_amount)) ? str_replace(',', '', $request->net_amount) : 0;


        if(isset($transaction_id)){

            $adjustment = $adjustment->find($transaction_id);

            $adjustment->employee_id                = $employee_id;
            $adjustment->basic_amount               = $basic_amount;
            $adjustment->pera_amount                = $pera_amount;
            $adjustment->gsis_cont_amount           = $gsis_cont_amount;
            $adjustment->philhealth_cont_amount     = $philhealth_cont_amount;
            $adjustment->pagibig_amount             = $pagibig_amount;
            $adjustment->total_deduction_amount     = $total_deduction_amount;
            $adjustment->net_amount                 = $net_amount;
            $adjustment->gross_amount               = $gross_amount;
            $adjustment->tax_amount                 = $tax_amount;
            $adjustment->actual_workdays            = $request->actual_workdays;
            $adjustment->date_from                  = $request->date_from;
            $adjustment->date_to                    = $request->date_to;
            $adjustment->year                       = $request->year;
            $adjustment->month                      = $request->month;
            $adjustment->updated_by                 = Auth::User()->id;

            $adjustment->save();

            $response = json_encode(['status'=>true,'response' => 'Update Successfully']);


        }else{

            // $this->validate($request,[
            // ]);

            $adjustment->employee_id                = $employee_id;
            $adjustment->basic_amount               = $basic_amount;
            $adjustment->gsis_cont_amount           = $gsis_cont_amount;
            $adjustment->pera_amount                = $pera_amount;
            $adjustment->philhealth_cont_amount     = $philhealth_cont_amount;
            $adjustment->pagibig_amount             = $pagibig_amount;
            $adjustment->total_deduction_amount     = $total_deduction_amount;
            $adjustment->net_amount                 = $net_amount;
            $adjustment->gross_amount               = $gross_amount;
            $adjustment->tax_amount                 = $tax_amount;
            $adjustment->actual_workdays            = $request->actual_workdays;
            $adjustment->date_from                  = $request->date_from;
            $adjustment->date_to                    = $request->date_to;
            $adjustment->year                       = $request->year;
            $adjustment->month                      = $request->month;
            $adjustment->created_by                 = Auth::User()->id;

            $adjustment->save();

            $response = json_encode(['status'=>true,'response' => 'Save Successfully']);

        }


        return $response;

    }

    public function showInitialSalary(){

         $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);


    }

    public function getInitialSalary(){

        $employee_id = Input::get('id');

        $salaryinfo            = new SalaryInfo;
        $employeeinfo          = new EmployeeInfo;
        $benefit               = new Benefit;
        $benefitinfo           = new BenefitInfo;
        $initialsalary         = new InitialSalary;

        $benefit = $benefit
        ->where('code','PERA')
        ->first();

        $pera = $benefitinfo
        ->where('benefit_id',$benefit->id)
        ->where('employee_id',@$employee_id)
        ->first();

        $initialsalary = $initialsalary
        ->where('employee_id',@$employee_id)
        ->get();

        $salaryinfo = $salaryinfo
                    ->with('positions')
                    ->where('employee_id',@$employee_id)
                    ->orderBy('salary_effectivity_date','desc')
                    ->first();

        $employeeinfo = $employeeinfo->where('employee_id',@$employee_id)->first();
        $basic_amount = @$salaryinfo->salary_new_rate;

        $query['new_rate_amount'] = $basic_amount;
        $query['philhealth_policy'] = $employeeinfo->philhealth_contribution;
        $query['pera_amount'] = @$pera->benefit_amount;
        $query['initialsalary'] = @$initialsalary;

        return $query;

    }

    public function getDaysInAMonth(){
        $data = Input::all();

        $year = @$data['year'];
        $month = @$data['month'];

        $y = date('Y', strtotime($year));
        $m = date('m', strtotime($month));

        $workdays = cal_days_in_month(CAL_GREGORIAN, $m, $y);

        return json_encode(['workdays'=>$workdays]);

    }

    public function getCountedDays(){
        $data = Input::all();

        $from         = @$data['date_from'];
        $to           = @$data['date_to'];

        if(isset($from) && isset($to)){
            $start_date   = date('Y-m-d', strtotime($from));
            $end_date     = date('Y-m-d', strtotime($to));
            $counted_days = $this->getWorkingDays($start_date,$end_date);

            return json_encode(['counted_days'=>$counted_days]);
        }

    }

    public function deleteInitialSalary(){
        $data = Input::all();

        $id             = $data['id'];
        $employee_id    = $data['employee_id'];

        $adjustments =  new InitialSalary;

        $adjustments->destroy($id);

        $query['adjustments'] = $adjustments
        ->where('employee_id',$employee_id)
        ->get();

        return $query;

    }

    public function getTax(){
        $data = Input::all();

        $taxtable = new TaxTable;
        $gross_salary = $data['gross_salary'];

        $prescribeTax        = 0;
        $prescribePercentage = 0;
        $clRange             = 0;
        $taxDue              = 0;

        $taxtable = $taxtable
        ->get()->toArray();

        $data['monthlyCL'] =  [
            0,
            $taxtable[1]['salary_bracket_level2'],
            $taxtable[1]['salary_bracket_level3'],
            $taxtable[1]['salary_bracket_level4'],
            $taxtable[1]['salary_bracket_level5'],
            $taxtable[1]['salary_bracket_level6']
        ];

        $data['percent'] = [0,.20,.25,.30,.32,.35];
        $data['below']  = [0,20833.33,33333.33,66666.66,166666.66,666666.66];
        $data['above'] = [20832,33332,66666,166666,666666,9999999] ;

        foreach ($data['monthlyCL'] as $key => $value) {

            if($gross_salary > $data['below'][$key] && $gross_salary < $data['above'][$key]){
                $prescribeTax = $value;
                $prescribePercentage = $data['percent'][$key];
                $clRange = $data['below'][$key];
            }
        }

        if($gross_salary < 20833){
            $clRange = 0;
            $prescribePercentage = 0;
        }

        $grossTaxable = $gross_salary - $clRange;
        $taxPercentage = $grossTaxable * $prescribePercentage;
        $taxDue = $taxPercentage + $prescribeTax;

        return $taxDue;
    }


}
