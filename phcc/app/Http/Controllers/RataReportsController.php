<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Rata;
use App\EmployeeStatus;
use App\Employee;
use App\EmployeeInformation;
use App\SpecialPayrollTransaction;
use App\PostedReport;
use App\Signatory;
use App\Exports\RataExport;

use Auth;
use Storage;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
class RataReportsController extends Controller
{
    function __construct(){
    	$this->title = 'REPRESENTATION AND TRANSPORTATION ALLOWANCE';
    	$this->module = 'rata';
        $this->module_prefix = 'payrolls/reports/othercompensations';
    	$this->controller = $this;

    }

    public function index(){


    	$employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $posted = PostedReport::where('report_type','rata')
        ->latest()
        ->paginate(20);

        $response = array(
                        'posted'        => $posted,
                        'employee'      => $query,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $query = Rata::leftJoin('pms_employees as e','e.id','pms_rata.employee_id')
            ->with([
            'employees',
            'positions',
            'departments',
            'offices',
            'responsibilities',
            'leave'
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('job_grade','desc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(count($query) > 0){

            foreach ($query as $key => $value) {
                $data[@$value->responsibilities->name][$key] = $value;
            }

        }else{

            $data = [];
        }

        return json_encode([
            'transaction'=>$data,
        ]);
    }

    /*
    * Store posted rata transaction
    */

    public function store(Request $request){

        $ifPosted = PostedReport::where('year',$request->year)
        ->where('month',$request->month)
        ->where('report_type','rata')
        ->first();

        if(isset($ifPosted)){
            $response = json_encode(['status'=>false,'response'=> 'Already posted.']);
        }else{

            $signatory = new Signatory;
            $signatory->fill($request->all());
            $signatory->created_by = Auth::id();

            if($signatory->save()){
                $posted = new PostedReport;
                $posted->fill($request->all());
                $posted->signatory_id   = $signatory->id;
                $posted->report_type    = 'rata';
                $posted->created_by     = Auth::id();

                if($posted->save()){
                    $transaction = SpecialPayrollTransaction::where('year',$request->year)
                    ->where('month',$request->month)
                    ->update(['posted' => 1]);
                }
            }

            $response = json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
        }

        return $response;
    }

    public function generateCSV(Request $request)
    {
        
        $year  = Input::get('_year');
        $month = Input::get('_month');

        $query = Rata::where('year',$year)
        ->where('month',$month)
        ->get();

        $status = false;
        if(count($query) > 0)
        {   
            $string = [];
            foreach ($query as $key => $value) {

                $raAmount = ($value->representation_amount) ? $value->representation_amount : 0;
                $taAmount = ($value->transportation_amount) ? $value->transportation_amount : 0;
                $raDiff   = ($value->ra_diff_amount) ? $value->ra_diff_amount : 0;
                $taDiff   = ($value->ta_diff_amount) ? $value->ta_diff_amount : 0;

                $totalRa = $raAmount + $raDiff;
                $totalTa = $taAmount + $taDiff;
                $forRelease = $totalRa + $totalTa;

                $string[$key] = array(
                    'account' =>  str_replace('-', '', $value->payrollinfo->atm_no),
                    'name'    => $value->employees->getFullName(),
                    'amount'  => str_replace(array('.',','), '', number_format($forRelease,2))
                );
            }

            return Excel::download(new RataExport($string), 'rata_'.date('YmdHis',strtotime(Carbon::now())).'.csv');

            $status = true;
        }


    }


}
