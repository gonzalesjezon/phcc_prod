<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaction;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\Benefit;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\PostedReport;
use App\Signatory;

use Input;
use DB;
use Auth;
use Storage;
use Carbon\Carbon;
class CommunicationExpenseReportController extends Controller
{
    function __construct(){
		$this->title = 'COMMUNICATION EXPENSE ALLOTMENT';
    	$this->module = 'communicationexpense';
        $this->module_prefix = 'payrolls/reports/othercompensations';
    	$this->controller = $this;
	}

	public function index(){


    	$employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $posted = PostedReport::where('report_type','cea')
        ->latest()
        ->paginate(20);

        $response = array(
                        'posted'        => $posted,
                        'employee'      => $query,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    /*
    * Store Eme posted transaction
    */

    public function store(Request $request){


        $ifPosted = PostedReport::where('year',$request->year)
        ->where('month',$request->month)
        ->where('report_type','cea')
        ->first();

        if(isset($ifPosted)){
            $response = json_encode(['status'=>false,'response'=> 'Already posted.']);
        }else{

            $signatory = new Signatory;
            $signatory->fill($request->all());
            $signatory->created_by = Auth::id();

            if($signatory->save()){
                $posted = new PostedReport;
                $posted->fill($request->all());
                $posted->signatory_id   = $signatory->id;
                $posted->report_type    = 'cea';
                $posted->created_by     = Auth::id();

                if($posted->save()){
                    $transaction = SpecialPayrollTransaction::where('year',$request->year)
                    ->where('month',$request->month)
                    ->update(['posted' => 1]);
                }
            }

            $response = json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
        }

        return $response;
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction =  new SpecialPayrollTransaction;


        $query = $transaction
        ->join('pms_employees as e','e.id','=','pms_specialpayroll_transactions.employee_id')
        ->with([
            'employees',
            'offices',
            'positions',
            'salaryinfo',
            'responsibilities'
        ])
        ->where('month',$month)
        ->where('year',$year)
        ->where('status','cea')
        ->orderBy('job_grade','desc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {

                if($value->responsibilities){
                 $data[$value->responsibilities->name][$key] = $value;
                }
            }

        }else{
            $data = [];
        }


        return json_encode([
            'transaction'=>$data,
        ]);
    }

}
