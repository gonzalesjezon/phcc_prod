<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\LeaveMonetizationTransaction;
class MonetizationTransmitalReportsReportsController extends Controller
{
    function __construct(){
    	$this->title = 'TRANSMITAL';
    	$this->module = 'monetizationtransmital';
        $this->module_prefix = 'payrolls/reports/monetizationreports';
    	$this->controller = $this;

    }

    public function index(){

    	$transaction = new LeaveMonetizationTransaction;

    	$transaction = $transaction->groupBy('remarks')->get();

    	$response = array(
    					'transaction' 	=> $transaction,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new LeaveMonetizationTransaction;

        $data = Input::all();

        $created_at = $data['created_at'];

        $query['transaction'] = $transaction
        ->where('created_at','like','%'.$created_at.'%')
        ->sum('net_amount');
       return json_encode($query);
    }
}
