<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\AttendanceInfo;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\NonPlantillaEmployeeInfo;
use App\NonPlantillaTransaction;
use App\EmployeeInfo;
use App\OvertimePay;
use App\Employee;
use App\Transaction;
class OvertimeReportsController extends Controller
{
     function __construct(){
		$this->title = 'OVERTIME REPORT';
    	$this->module = 'overtimereport';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

    	$employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $response = array(
                        'employee'      => $query,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction              = new Transaction;
        $employeeinfo             = new EmployeeInfo;
        $nonplantilla_transaction = new NonPlantillaTransaction;
        $nonplantilla_employeeinfo = new NonPlantillaEmployeeInfo;
        $attendanceinfo           = new AttendanceInfo;
        $overtime                 = new OvertimePay;

        $query['overtimeinfo'] = $overtime
        ->with('employees','employeeinfo')
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('created_at','desc')
        ->get();

        return json_encode($query);
    }

}
