<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Office;
use App\Transaction;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\PostedReport;
use App\Signatory;
use App\Benefit;
use Input;
use Auth;
use Carbon\Carbon;

use App\Exports\RataExport;
use Maatwebsite\Excel\Facades\Excel;
class PayrollRegisterReportController extends Controller
{
    function __construct(){
    	$this->title = 'PAYROLL REGISTER REPORT';
    	$this->module = 'payrollregister';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $posted = PostedReport::where('report_type','payroll_register')
        ->latest()
        ->get();

    	$response = array(
                        'employee'      => $query,
                        'posted'        => $posted,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $this->validate($request,[
            'pay_period' => 'required'
        ]);

        $ifPosted = PostedReport::where('year',$request->year)
        ->where('month',$request->month)
        ->where('report_type','payroll_register')
        ->where('pay_period',$request->pay_period)
        ->first();

        if(isset($ifPosted)){
            $response = json_encode(['status'=>false,'response'=> 'Already posted.']);
        }else{

            $signatory = new Signatory;
            $signatory->fill($request->all());
            $signatory->created_by = Auth::id();

            if($signatory->save()){
                $posted = new PostedReport;
                $posted->fill($request->all());
                $posted->signatory_id   = $signatory->id;
                $posted->report_type    = 'payroll_register';
                $posted->created_by     = Auth::id();

                if($posted->save()){
                    $transaction = Transaction::where('year',$request->year)
                    ->where('month',$request->month)
                    ->update(['posted' => 1]);
                }
            }

            $response = json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
        }

        return $response;
    }

    public function show(){

        $q = Input::all();

        $transaction = new Transaction;
        $office      = new Office;
        $benefit     = new Benefit;

        $year = $q['year'];
        $month = $q['month'];
        $pay_period = $q['pay_period'];

        $benefit = $benefit->where('name','PERA')->first();

        $query2 = $transaction
        ->where('year',$year)
        ->where('hold',0)
        ->orderBy('month','asc')
        ->groupBy('month')
        ->get();

        $holdCount = count($query2);

        $query = $transaction
        ->leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'offices',
            'employees',
            'employeeinformation',
            'employeeinfo',
            'salaryinfo',
            'benefitTransactions' => function($qry) use($benefit,$year,$month){
                $qry = $qry
                ->where('year',$year)
                ->where('month',$month)
                ->where('benefit_id',$benefit->id);
            },
            'transactions' => function($qry) use($year,$month,$pay_period){
                // $qry = $qry->where('year',$year)->where('month','<=',$month);
                            // if($pay_period == 'firsthalf'){
                            //     $qry->where('month','!=',$month);
                            // }
                $qry = $qry->whereNotNull('hold_status')
                            // ->where('hold',0)
                            ->where('for_period',$pay_period)
                            ->where('for_month',$month)
                            ->orderBy('month','desc');
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.sort_by','asc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(isset($query)){
            foreach ($query as $key => $value) {
                $data[strtoupper(@$value->offices->Name)][$key] = $value;
            }
        }else{

            $data = [];
        }

        return json_encode([
           'transaction' => $data,
           'hold_pay' => $query2,
           'hold_count' => $holdCount
        ]);
    }

    public function showWPage(){

        $q = Input::all();

        $transaction = new Transaction;
        $office      = new Office;

        $year = $q['year'];
        $month = $q['month'];
        $pay_period = $q['pay_period'];


        $query2 = $transaction
        ->where('year',$year)
        ->whereIn('hold_status',[$pay_period,'both'])
        ->where('hold',0)
        ->orderBy('month','asc')
        ->groupBy('month')
        ->get();

        $holdCount = count($query2);

        $query = $transaction
        ->leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'offices',
            'employees',
            'employeeinformation',
            'employeeinfo',
            'salaryinfo',
            'transactions' => function($qry) use($year, $pay_period){
                $qry = $qry->where('year',$year)
                            ->whereIn('hold_status',[$pay_period,'both'])
                            ->where('hold',0);
            }
        ])
        ->where('hold',0)
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.sort_by','asc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(isset($query)){
            foreach ($query as $key => $value) {
                $data[@$value->offices->Name][$key] = $value;
            }
        }else{

            $data = [];
        }

        $data2 = [];
        foreach ($data as $key => $value) {
            $data2[$key] = array_values($value);
        }


        $sameOffice = '';
        $data3 = [];
        $ctr = 1;
        $ctr2 = 1;
        foreach ($data2 as $key => $value) {
            foreach ($value as $k => $val) {
                if($ctr <= 30){
                    $data3[$ctr2][$key][$ctr] = $val;
                }else{
                    $ctr = 0;
                    $ctr2++;
                }
                $ctr++;
            }
            // $ctr  = 1;
            // $ctr2 = 1;
        }

        return json_encode([
           'transaction' => $data3,
           'hold_pay' => $query2,
           'hold_count' => $holdCount,
           'totalPage' => $ctr2
        ]);
    }


    public function generateCSV(Request $request)
    {
        
        $year  = Input::get('_year');
        $month = Input::get('_month');
        $payPeriod = Input::get('_payperiod');

        $query = Transaction::with([
            'transactions' => function($qry) use($payPeriod, $year, $month){
                $qry = $qry->where('year',$year)
                ->where('for_month',$month)
                ->where('for_period',$payPeriod)
                ->where('hold',1);
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->where('hold',0)
        ->get();

        $status = false;
        if(count($query) > 0)
        {   
            $string = [];
            foreach ($query as $key => $value) {

                $addAdj     = ($value->additional_adj) ? $value->additional_adj : 0;
                $lessAdj    = ($value->deduction_adj) ? $value->deduction_adj : 0;

                $salaryForRelease = 0;
                if(count($value->transactions) > 0)
                {
                    foreach ($value->transactions as $key2 => $value2) { 
                        switch ($value2->hold_status) {
                            case 'firsthalf':
                                $salaryForRelease += $value2->net_first_half; 
                                break;
                            
                            case 'secondhalf':
                                $salaryForRelease += $value2->net_second_half;
                                break;
                        }
                        
                    }
                }

                $totalPay = 0;
                switch ($payPeriod) {
                    case 'firsthalf':
                        $totalPay = $value->net_first_half; 
                        break;
                    
                    case 'secondhalf':
                        $totalPay = $value->net_second_half;
                        break;
                }

                $forRelease = $totalPay + $salaryForRelease + $addAdj - $lessAdj;

                $string[$key] = array(
                    'account' =>  str_replace('-', '', $value->payrollinfo->atm_no),
                    'name'    => $value->employees->getFullName(),
                    'amount'  => str_replace(array('.',','), '', number_format($forRelease,2))
                );
            }

            return Excel::download(new RataExport($string), 'payrollregister_'.date('YmdHis',strtotime(Carbon::now())).'.csv');

            $status = true;
        }


    }
}
