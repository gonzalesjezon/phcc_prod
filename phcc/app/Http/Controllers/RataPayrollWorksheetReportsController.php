<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Rata;
use App\Deduction;
use App\DeductionInfoTransaction;
class RataPayrollWorksheetReportsController extends Controller
{
    function __construct(){
    	$this->title = 'RATA - PAYROLL WORKSHEETS';
    	$this->module = 'ratapayrollworksheets';
        $this->module_prefix = 'payrolls/reports/ratareports';
    	$this->controller = $this;

    }

    public function index(){

    	$deductions = new Deduction;
    	$deductions = $deductions->where('payroll_group','rata')->get();
    	$response = array(
    					'deductions' 	=> $deductions,
    					'cols' 			=> count($deductions),
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new Rata;

        $transaction = $transaction
        ->with([
        	'employees',
        	'positions',
        	'salaryinfo',
        	'deductions' => function($qry) use($year,$month){
        		$qry = $qry->where('year',$year)
        		->where('month',$month);
        	}
    	])
    	->where('year',$year)
    	->where('month',$month)
    	->get();

    	return json_encode(['transaction' => $transaction]);
    }
}
