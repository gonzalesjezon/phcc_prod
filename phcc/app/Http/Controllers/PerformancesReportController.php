<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
use App\BenefitInfo;
use App\Benefit;
class PerformancesReportController extends Controller
{
    function __construct(){
		$this->title = 'PERFORMANCE ENHANCEMENT INCENTIVE';
    	$this->module = 'performancesreport';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new SpecialPayrollTransaction;

        $query = $transaction
        ->with('offices','employees','positions','employeeinformation')
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','pei')
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                $data[@$value->offices->name][$key] = $value;
            }

        }else{
            $data = [];
        }


        return json_encode($data);
    }
}
