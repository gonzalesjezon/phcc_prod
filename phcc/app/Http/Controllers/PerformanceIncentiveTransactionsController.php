<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
use App\SalaryInfo;
use App\Rate;
use DateTime;
class PerformanceIncentiveTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'PERFORMANCE ENHANCEMENT INCENTIVE';
    	$this->controller = $this;
    	$this->module = 'peitransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	        	=> $this->title,
           'controller'         => $this->controller,
           'module'	        	=> $this->module,
           'module_prefix'      => $this->module_prefix,
           'months'             => config('params.months'),
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'current_month'      => (int)date('m')

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $_year      = Input::get('_year');
        $_month     = Input::get('_month');
        $check_pei 	= Input::get('check_pei');
        $checkpei 	= Input::get('checkpei');
        $data = "";

        $data = $this->searchName($q,$check_pei,$_year,$_month);

        if(isset($year) || isset($month) || isset($checkpei)){
            $data = $this->filter($year,$month,$checkpei);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkpei,$year,$month){

        $employee_status            = new EmployeeStatus;
        $employeeinfo               = new EmployeeInformation;
        $employee                   = new Employee;
        $transaction                = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname'];

        $empstatus_id = $employee_status
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinfo
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

        // $salary_info_employee_id = $salaryinfo->select('employee_id')->get()->toArray();

        $query = [];
        switch ($checkpei) {
            case 'wpei':
               $employee_id = $transaction->whereIn('employee_id',$employeeinfo)
               ->where('year',$year)
               ->where('month',$month)
               ->where('status','pei')
               ->select('employee_id')
               ->get()
               ->toArray();

               $query = $employee->whereIn('id',$employee_id);

                break;

            case 'wopei':
               $employee_id = $transaction->whereIn('employee_id',$employeeinfo)
               ->where('year',$year)
               ->where('month',$month)
               ->where('status','pei')
               ->select('employee_id')
               ->get()
               ->toArray();

               $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$employeeinfo);
                break;

            default:

                $query = $employee
                ->whereIn('id',$employeeinfo);

                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


     public function filter($year,$month,$checkpei){

        $employee_status        = new EmployeeStatus;
        $transaction            = new SpecialPayrollTransaction;
        $employee               = new Employee;
        $employeeinfo           = new EmployeeInformation;

        $empstatus_id = $employee_status
        ->where('category',1)->select('RefId')
        ->get()->toArray();

        $employeeinfo  = $employeeinfo
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

        // $salary_info_employee_id = $employeeinfo->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkpei) {
            case 'wpei':

                $query =  $transaction->select('employee_id')->where('status','pei');

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();


                break;

            case 'wopei':

                 $query =  $transaction->select('employee_id')->where('status','pei');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employeeinfo)
                                        // ->whereIn('id',$salary_info_employee_id)
                                        ->whereNotIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }


    public function processPei(Request $request){
    	$data = Input::all();

    	$employeeinformation = new EmployeeInformation;
        $employee            = new Employee;

        $rate                = new Rate;
        $pei_amount = 5000;
        $cna_amount = 25000;

        $rate = $rate->where('status','pei')->get();

    	foreach ($data['list_id'] as $key => $value) {

            if(isset($value)){

                $transaction     = new SpecialPayrollTransaction;
                $employeeinfo    = $employeeinformation->where('employee_id',$value)->first();
                $employee       = $employee->where('id',$value)->first();
                $assumption_date = ($employeeinfo->assumption_date) ? $employeeinfo->assumption_date : '';

                $first_date = new DateTime($assumption_date);
                $nov = date('Y').'-11-30 00:00:00';

                $second_date = DateTime::createFromFormat('Y-m-d H:i:s', $nov);
                $interval = $first_date->diff($second_date);

                // $x = $interval->format('%Y years %M months and %D days.');
                $number_of_month = (int)$interval->format('%M');
                $number_of_year = (int)$interval->format('%Y');

                if($number_of_year !== 0){

                    $transaction->employee_id  = $value;
                    $transaction->percentage   = 100;
                    $transaction->position_id  = @$employeeinfo->position_id;
                    $transaction->office_id    = @$employeeinfo->office_id;
                    $transaction->employee_number  = @$employee->employee_number;
                    $transaction->cna_amount   = $cna_amount;
                    $transaction->amount       = $pei_amount;
                    $transaction->year         = $data['year'];
                    $transaction->month        = $data['month'];
                    $transaction->status       = 'pei';

                    $transaction->save();

                }else{

                    if($number_of_month >= 3 && $number_of_month <= 4){

                        $new_amount     =  ((float)$pei_amount*.5);
                        $cna_new_amount =  ((float)$cna_amount*.5);

                        $transaction->percentage   = 50;
                        $transaction->position_id  = @$employeeinfo->position_id;
                        $transaction->office_id    = @$employeeinfo->office_id;
                        $transaction->employee_number  = @$employee->employee_number;
                        $transaction->employee_id  = $value;
                        $transaction->cna_amount   = $cna_new_amount;
                        $transaction->amount       = $new_amount;
                        $transaction->year         = $data['year'];
                        $transaction->month        = $data['month'];
                        $transaction->status        = 'pei';

                        $transaction->save();

                    }elseif($number_of_month >= 32&& $number_of_month <= 3){

                        $new_amount =  ((float)$pei_amount*.4);
                        $cna_new_amount =  ((float)$cna_amount*.4);

                        $transaction->percentage    = 40;
                        $transaction->position_id   = @$employeeinfo->position_id;
                        $transaction->office_id     = @$employeeinfo->office_id;
                        $transaction->employee_number  = @$employee->employee_number;
                        $transaction->employee_id   = $value;
                        $transaction->cna_amount    = $cna_new_amount;
                        $transaction->amount        = $new_amount;
                        $transaction->year          = $data['year'];
                        $transaction->month         = $data['month'];
                        $transaction->status        = 'pei';

                        $transaction->save();

                    }elseif($number_of_month >= 1 && $number_of_month <= 2){

                        $new_amount =  ((float)$pei_amount*.3);
                        $cna_new_amount =  ((float)$cna_amount*.3);

                        $transaction->percentage    = 30;
                        $transaction->position_id   = @$employeeinfo->position_id;
                        $transaction->office_id     = @$employeeinfo->office_id;
                        $transaction->employee_number  = @$employee->employee_number;
                        $transaction->employee_id   = $value;
                        $transaction->cna_amount    = $cna_new_amount;
                        $transaction->amount        = $new_amount;
                        $transaction->year          = $data['year'];
                        $transaction->month         = $data['month'];
                        $transaction->status        = 'pei';

                        $transaction->save();

                    }else{

                        $new_amount =  ((float)$pei_amount*.2);
                        $cna_new_amount =  ((float)$cna_amount*.2);

                        $transaction->percentage    = 20;
                        $transaction->employee_id   = $value;
                        $transaction->position_id   = @$employeeinfo->position_id;
                        $transaction->office_id     = @$employeeinfo->office_id;
                        $transaction->employee_number  = @$employee->employee_number;
                        $transaction->cna_amount    = $cna_new_amount;
                        $transaction->amount        = $new_amount;
                        $transaction->year          = $data['year'];
                        $transaction->month         = $data['month'];
                        $transaction->status        = 'pei';

                        $transaction->save();

                    }
                }

	    	}
    	}

    	$response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

    	return $response;
    }

    public function showPeiDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getPeiInfo(){
    	$data = Input::all();

    	$pei =  new SpecialPayrollTransaction;

    	$query = $pei->with('positions')
                    ->where('year',$data['year'])
			    	->where('month',$data['month'])
                    ->where('status','pei')
			    	->where('employee_id',@$data['employee_id'])->get();

    	return json_encode($query);
    }

    public function deletePei(){
        $data = Input::all();

        $rata = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $rata->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->where('status','pei')
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
