<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\EmployeeInformation;
use App\EmployeeStatus;
use App\Employee;
use App\PostedReport;
use App\Signatory;

use Input;
use Auth;
class GSISLoanReportsController extends Controller
{
    function __construct(){
		$this->title = 'GSIS REMITTANCE REPORT';
    	$this->module = 'gsisloanreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){

		$employee = new Employee;
		$employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;


		$status = $employeestatus
        ->where('category',1)
        ->select('id')
        ->get()->toArray();

        $employee_id = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $employee = $employee
        ->whereIn('id',$employee_id)
        ->where('active',1)
        ->where('with_setup',1)
        ->orderBy('lastname','asc')
        ->get();

        $posted = PostedReport::where('report_type','gsis_loans')
        ->latest()
        ->paginate(20);

    	$response = array(
                        'posted'        => $posted,
    					'employee' 		=> $employee,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

     /*
    * Store GSIS posted transaction
    */

    public function store(Request $request){


        $ifPosted = PostedReport::where('year',$request->year)
        ->where('month',$request->month)
        ->where('report_type','gsis_loans')
        ->first();

        if(isset($ifPosted)){
            $response = json_encode(['status'=>false,'response'=> 'Already posted.']);
        }else{

            $signatory = new Signatory;
            $signatory->fill($request->all());
            $signatory->created_by = Auth::id();

            if($signatory->save()){
                $posted = new PostedReport;
                $posted->fill($request->all());
                $posted->signatory_id   = $signatory->id;
                $posted->report_type    = 'gsis_loans';
                $posted->created_by     = Auth::id();

                if($posted->save()){
                    $transaction = Transaction::where('year',$request->year)
                    ->where('month',$request->month)
                    ->update(['posted' => 1]);
                }
            }

            $response = json_encode(['status'=>true,'response'=> 'Report posted successfully.']);
        }

        return $response;
    }


    public function show(){

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction         = new EmployeeInfo;
        $employeeinformation = new EmployeeInformation;
        $employeestatus      = new EmployeeStatus;

        $status = $employeestatus
        ->where('category',1)
        ->select('id')
        ->get()->toArray();

        $employee_id = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $transaction
        ->with([
        		'loanstransaction' => function($qry){
        			$qry = $qry->with([
        				'loans' => function($qry){
        					$qry->where('loan_type','GSIS');
        				}
        			]);
        		},
        		'salaryinfo',
        		'employees',
            ])
        ->whereIn('employee_id',$employee_id)
		->get();


        return json_encode($query);
    }
}
