<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Office;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Employee;
use App\SpecialPayrollTransaction;
use App\EmployeeInfo;

use DateTime;
use Input;
use Auth;
class ClothingTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'CLOTHING TRANSACTIONS';
    	$this->controller = $this;
    	$this->module = 'clothing_transactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	            => $this->title,
           'controller'         => $this->controller,
           'module'	            => $this->module,
           'module_prefix'      => $this->module_prefix,
           'months'             => config('params.months'),
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'current_month'      => (int)date('m')

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $_year      = Input::get('_year');
        $_month     = Input::get('_month');
        $check_eme 	= Input::get('check_eme');
        $checkeme 	= Input::get('checkeme');
        $data = "";

        $data = $this->searchName($q,$check_eme,$_year,$_month);

        if(isset($year) || isset($month) || isset($checkeme)){
            $data = $this->filter($year,$month,$checkeme);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

      public function searchName($q,$checkeme,$year,$month){

        $employee         = new Employee;
        $transaction      = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname'];

        $arr_employee_id = $transaction
        ->where('year',$year)
        ->where('status','ua')
        ->pluck('employee_id')
        ->toArray();

        $query = [];
        switch ($checkeme) {
            case 'with':
            	$query = $employee->whereIn('id',$arr_employee_id);
                break;
            case 'wout':
            	$query = $employee->whereNotIn('id',$arr_employee_id);
                break;

            default:
            	$query = $employee;
                break;
        }

	    $query = $query->where(function($qry) use($q, $cols){
	        foreach ($cols as $key => $value) {
	            $qry->orWhere($value,'like','%'.$q.'%');
	        }
	    });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;
    }


    public function filter($year,$month,$checkeme){

        $employee           = new Employee;
        $transaction        = new SpecialPayrollTransaction;

        $arr_applicable_employee = $this->applicable_employee();

        $arr_employee_id = $transaction
        ->where('year',$year)
        ->where('status','ua')
        ->pluck('employee_id')
        ->toArray();

        $query = [];
        switch ($checkeme) {
            case 'with':
            	$query = $employee->whereIn('id',$arr_employee_id);
                break;
            case 'wout':
            	$query = $employee->whereNotIn('id',$arr_employee_id);
                break;

            default:
            	$query = $employee->whereIn('id',$arr_applicable_employee);
                break;
        }

        $response = $query
        ->where('active',1)
        ->orderBy('lastname','asc')->get();

        return $response;
    }

    public function store(Request $request){

        $this->validate($request,[
            'remarks' => 'required'
        ]);

        $transaction                    = SpecialPayrollTransaction::find($request->id);
        $transaction->special_remarks   = $request->remarks;
        $transaction->updated_by        = Auth::id();
        if($transaction->save()){
            return json_encode(['status' => true, 'response' => 'Save successfully.']);
        }

    }


    public function process(Request $request){
    	$data = Input::all();

    	$year 				= $data['year'];
    	$month 				= $data['month'];
    	$clothing_amount 	= 6000;

    	foreach ($data['list_id'] as $key => $id) {
    		$transaction 				= new SpecialPayrollTransaction;
    		$employeeinfo        		= new EmployeeInformation;
            $payrollinfo         		= new EmployeeInfo;
            $employee            		= new Employee;

            $employee 		= $employee->where('id',$id)->first();
            $employeeinfo   = $employeeinfo->where('employee_id',$id)->first();
            $payrollinfo   	= $payrollinfo->where('employee_id',$id)->first();

            $transaction->employee_number   = @$employee->employee_number;
            $transaction->office_id         = @$employeeinfo->office_id;
            $transaction->position_id       = @$employeeinfo->position_id;
            $transaction->division_id       = @$employeeinfo->division_id;
            $transaction->responsibility_id = @$payrollinfo->responsibility_id;

    		$transaction->amount 		= $clothing_amount;
    		$transaction->employee_id 	= $id;
    		$transaction->year 			= $year;
    		$transaction->month 		= $month;
    		$transaction->status 		= 'ua';
    		$transaction->created_by 	= Auth::id();
    		$transaction->save();
    	}

    	$response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

    	return $response;
    }

    public function showClothing(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getClothing(){
    	$data = Input::all();

    	$transaction =  new SpecialPayrollTransaction;

    	$query = $transaction
        ->with([
            'positions',
            'offices',
    	])
        ->where('year',$data['year'])
		->where('month',$data['month'])
        ->where('status','ua')
		->where('employee_id',@$data['employee_id'])
        ->first();

    	return json_encode($query);
    }

    public function deleteClothing(){
        $data = Input::all();

        $transaction = new SpecialPayrollTransaction;
        foreach ($data['empid'] as $key => $value) {

            $transaction->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->where('status','ua')
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

    public function applicable_employee(){

    	$employeeinfo = EmployeeInformation::getModels();
    	$arr_id = [];
    	foreach ($employeeinfo as $key => $value) {

	        $assumption_date = ($value->assumption_date) ? $value->assumption_date : '';

	        $first_date = new DateTime($assumption_date);

	        $second_date = DateTime::createFromFormat('Y-m-d H:i:s', now());
	        $interval = $first_date->diff($second_date);

	        // $x = $interval->format('%Y years %M months and %D days.');
	        $number_of_month = (int)$interval->format('%M');
	        $number_of_year = (int)$interval->format('%Y');
    		# code...

    		if($number_of_year || $number_of_month >= 6){
    			$arr_id[$key] = $value->employee_id;
    		}
    	}

    	return $arr_id;

    }

}
