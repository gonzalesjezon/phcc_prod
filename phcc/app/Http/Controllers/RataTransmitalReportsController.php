<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Rata;
use App\EmployeeInfo;
use App\AttendanceInfo;
class RataTransmitalReportsController extends Controller
{
    function __construct(){
    	$this->title = 'RATA TRANSMITAL';
    	$this->module = 'ratatransmital';
        $this->module_prefix = 'payrolls/reports/ratareports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new Rata;
    	$employeeinfo = new EmployeeInfo;
    	$attendanceinfo = new AttendanceInfo;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $query['transaction'] = $transaction->where('year',$year)
								        ->where('month',$month)
								        ->get();
       return json_encode($query);
    }
}
