<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Crypt;
use App\OvertimePay;
use App\Transaction;
use App\EmployeeStatus;
use App\Employee;
use App\EmployeeInfo;
use App\EmployeeInformation;
use App\BenefitInfo;
use App\SalaryInfo;
use App\Benefit;
use App\LongevityPay;
use App\SpecialPayrollTransaction;
use App\AnnualTaxPolicy;
use App\AnnualTaxRate;
use App\SpecialAnnualTax;
use App\BeginningBalance;
use App\PreviousEmployer;
use Carbon\Carbon;
use Session;
use Auth;
class AnnualTaxSetupController extends Controller
{
    function __construct(){
    	$this->title = 'ANNUAL TAXATION SETUP';
    	$this->controller = $this;
    	$this->module = 'annualtaxsetup';
        $this->module_prefix = 'payrolls/admin';
    }

     public function index(){

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q              = Input::get('q');
        $year           = Input::get('year');
        $month          = Input::get('month');

        $data = $this->searchName($q);


        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }
    public function searchName($q){

        $employee_status            = new EmployeeStatus;
        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $transaction                = new Transaction;
        $employeeinfo                = new EmployeeInfo;

        $cols = ['lastname','firstname',];

        $employeeinfo = $employeeinfo->select('employee_id')->get()->toArray();

        $empstatus_id = $employee_status->where('category',1)->select('RefId')->get()->toArray();

        $employee_info_id = $employee_info
        ->whereIn('employee_status_id',$empstatus_id)
        ->whereIn('employee_id',$employeeinfo)
        ->select('employee_id')->get()->toArray();

        $query = $employee->whereIn('id',$employee_info_id)
            ->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function getEmployeesinfo(){

        $data = Input::all();

        $year        = $data['year'];
        $employee_id = $data['id'];

        $transaction        = new Transaction;
        $overtimepay        = new OvertimePay;
        $salaryinfo         = new SalaryInfo;
        $benefit            = new Benefit;
        $benefitTransaction = new BenefitInfo;
        $employeeinfo       = new EmployeeInfo;
        $longevitypay       = new LongevityPay;
        $specialpayroll     = new SpecialPayrollTransaction;
        $taxrates           = new AnnualTaxRate;
        $specialtax         = new SpecialAnnualTax;
        $beginningbalance   = new BeginningBalance;
        $previousemployer   = new PreviousEmployer;

        $rataCode = ['RATA1','RATA2'];

        $peraId = $benefit
        ->where('code','PERA')
        ->select('id')
        ->first();

        $rataId = $benefit
        ->whereIn('code',$rataCode)
        ->select('id')
        ->get()
        ->toArray();

        $hpId = $benefit
        ->where('code','HP')
        ->select('id')
        ->first();

        $query['salaryinfo'] = $salaryinfo
        ->where('employee_id',$employee_id)
        ->orderBy('salary_effectivity_date','desc')
        ->first();

        $query['employeeinfo'] = $employeeinfo
        ->with('employees')
        ->where('employee_id',$employee_id)
        ->orderBy('created_at','desc')
        ->first();

        $query['transaction'] = $transaction
        ->where('year',$year)
        ->where('employee_id',$employee_id)
        ->get();

        $query['overtimepay'] = $overtimepay
        ->where('year',$year)
        ->where('employee_id',$employee_id)
        ->where('used_amount','!=',null)
        ->orderBy('created_at','asc')
        ->get();

        $query['pera'] = $benefitTransaction
        ->where('benefit_id',$peraId->id)
        ->where('employee_id',$employee_id)
        ->first();

        // $query['hp'] = $benefitTransaction
        // ->where('benefit_id',@$hpId->id)
        // ->where('employee_id',$employee_id)
        // ->first();

        // $query['lp'] = $longevitypay
        // ->where('employee_id',$employee_id)
        // ->orderBy('longevity_date','desc')
        // ->first();

        $query['rata'] = $benefitTransaction
        ->with('benefits')
        ->whereIn('benefit_id',$rataId)
        ->where('employee_id',$employee_id)
        ->get();

        $query['yearendbonus'] = $specialpayroll
        ->where('employee_id',$employee_id)
        ->where('year',$year)
        ->where('status','yearendbonus')
        ->sum('amount');

        $query['cash_gift_amount'] = $specialpayroll
        ->where('employee_id',$employee_id)
        ->where('year',$year)
        ->where('status','yearendbonus')
        ->sum('cash_gift_amount');

        $query['midyearbonus'] = $specialpayroll
        ->where('employee_id',$employee_id)
        ->where('year',$year)
        ->where('status','midyearbonus')
        ->sum('amount');

        $query['pbb'] = $specialpayroll
        ->where('employee_id',$employee_id)
        ->where('year',$year)
        ->where('status','pbb')
        ->sum('amount');

        $query['pei'] = $specialpayroll
        ->where('employee_id',$employee_id)
        ->where('year',$year)
        ->where('status','pei')
        ->sum('amount');

        $query['taxrates'] = $taxrates
        ->where('employee_id',$employee_id)
        ->where('for_year',$year)
        ->get();

        $query['specialtax'] = $specialtax
        ->where('employee_id',$employee_id)
        ->where('for_year',$year)
        ->first();

        $query['previousemployer'] = $previousemployer
        ->where('employee_id',$employee_id)
        ->first();

        $query['beginningbalance'] = $beginningbalance
        ->where('employee_id',$employee_id)
        ->first();

        return json_encode($query);
    }

    public function getAnnualTaxPolicy(){
        $data = Input::all();

        $year        = $data['year'];
        $tax_income  = $data['tax_income'];
        $employee_id = $data['employee_id'];
        // $employee_number = $data['employee_number'];
        $count       = @$data['limit'];
        $count = $count - 1;
        $annualtax = new AnnualTaxPolicy;
        $taxrates  = new AnnualTaxRate;
        $specialtax = new SpecialAnnualTax;
        $takeAll = $taxrates->count();

        if($takeAll !== 0){
            $take = $takeAll - (int)$count;
        }else{
            $take = $count;
        }

        $annualtax = $annualtax
        ->orWhere('from_year',$year)
        ->orWhere('to_year',$year)
        ->orderBy('below_amount','asc')
        ->get();

        $query = [];

        $query['taxrates'] = $taxrates
        ->where('employee_id',$employee_id)
        // ->where('employee_number',$employee_number)
        ->where('for_year',$year)
        // ->take($count)
        ->get();

        $query['specialtax'] = $specialtax
        ->where('employee_id',$employee_id)
        // ->where('employee_number',$employee_number)
        ->where('for_year',$year)
        ->first();

        $query['rate_id'] = $taxrates
        ->where('employee_id',$employee_id)
        // ->where('employee_number',$employee_number)
        ->where('for_year',$year)
        ->orderBy('for_year','desc')
        ->skip($count)
        ->take($take)
        ->get();

        $query['recordsCount'] = count($query['taxrates']);

        foreach ($annualtax as $key => $value) {
            $below_amount = $value->below_amount;
            $above_amount = $value->above_amount;

            if($tax_income > $below_amount && $tax_income < $above_amount){
                $query['rates'] = $value;
                return $query;
            }
        }

        return $query;

    }

    public function store(Request $request){

        $dataTax    = array_values(array_filter(explode(',', $request->array_tax)));
        $dataPera   = array_values(array_filter(explode(',', $request->array_pera)));
        $dataCont   = array_values(array_filter(explode(',', $request->array_cont)));
        $dataRata   = array_values(array_filter(explode(',', $request->array_rata)));
        $dataBasic  = array_values(array_filter(explode(',', $request->array_basic)));
        $dataLP     = array_values(array_filter(explode(',', $request->array_lp)));
        $dataHP     = array_values(array_filter(explode(',', $request->array_hp)));
        $dataOT     = array_values(array_filter(explode(',', $request->array_ot)));
        $dataLWOP   = array_values(array_filter(explode(',', $request->array_lwop)));
        $dataMonth   = array_values(array_filter(explode(',', $request->array_month)));
        $dataId     = explode(',', $request->array_id);

        $year = $request->for_year;
        $loyalty_amount = ($request->loyalty_amount !== null) ? str_replace(',', '', $request->loyalty_amount) : 0;
        $pb_amount = ($request->pb_amount !== null) ? str_replace(',', '', $request->pb_amount) : 0;
        $cn_amount = ($request->cn_amount !== null) ? str_replace(',', '', $request->cn_amount) : 0;
        $pei_amount = ($request->pei_amount !== null) ? str_replace(',', '', $request->pei_amount) : 0;
        $myb_amount = ($request->myb_amount !== null) ? str_replace(',', '', $request->myb_amount) : 0;
        $yeb_amount = ($request->yeb_amount !== null) ? str_replace(',', '', $request->yeb_amount) : 0;
        $clothing_amount = ($request->total_clothing_amount !== null) ? str_replace(',', '', $request->total_clothing_amount) : 0;
        $cash_gift_amount = ($request->total_cg_amount !== null) ? str_replace(',', '', $request->total_cg_amount) : 0;

        $thirteen_month_amount = ($request->thirteen_month !== null) ? str_replace(',', '', $request->thirteen_month) : 0;
        $mandatory_amount = ($request->mandatory_deductions !== null) ? str_replace(',', '', $request->mandatory_deductions) : 0;
        $gross_taxable_income_amount = ($request->gross_taxable_income_amount !== null) ? str_replace(',', '', $request->gross_taxable_income_amount) : 0;
        $tax_withheld = ($request->tax_withheld !== null) ? str_replace(',', '', $request->tax_withheld) : 0;

        $thirteen_month_prev_employer = ($request->thirteen_month_prev_employer !== null) ? str_replace(',', '', $request->thirteen_month_prev_employer) : 0;
        $mandatory_deductions_prev_employer = ($request->mandatory_deductions_prev_employer !== null) ? str_replace(',', '', $request->mandatory_deductions_prev_employer) : 0;
        $gross_taxable_prev_employer_amount = ($request->gross_taxable_prev_employer_amount !== null) ? str_replace(',', '', $request->gross_taxable_prev_employer_amount) : 0;
        $tax_withheld_prev_employer = ($request->tax_withheld_prev_employer !== null) ? str_replace(',', '', $request->tax_withheld_prev_employer) : 0;

        if(count($dataTax) !== 1){
            if(count($dataId) > 0 && count($dataId) !== 1){
                $taxrates =  new AnnualTaxRate;

                foreach ($dataTax as $key => $value) {
                    if(!empty($value)){
                        $taxrates = $taxrates->find($dataId[$key]);
                        $taxrates->employee_id          = $request->employee_id;
                        $taxrates->tax_amount           = $dataTax[$key];
                        $taxrates->pera_amount          = $dataPera[$key];
                        $taxrates->contribution_amount  = $dataCont[$key];
                        $taxrates->rata_amount          = $dataRata[$key];
                        $taxrates->basic_amount         = $dataBasic[$key];
                        $taxrates->longevity_amount     = $dataLP[$key];
                        $taxrates->hp_amount            = $dataHP[$key];
                        $taxrates->lwop_amount          = $dataLWOP[$key];
                        $taxrates->overtime_amount      = $dataOT[$key];
                        $taxrates->for_month            = $dataMonth[$key];
                        $taxrates->for_year             = $year;
                        $taxrates->updated_by           = Auth::User()->id;
                        $taxrates->save();
                    }
                }

                if(isset($request->specialtax_id)){
                    $specialtax =  new SpecialAnnualTax;
                    $specialtax = $specialtax->find($request->specialtax_id);
                    $specialtax->employee_id                    = $request->employee_id;
                    $specialtax->clothing_allowance_amount      = $clothing_amount;
                    $specialtax->cash_gift_amount               = $cash_gift_amount;
                    $specialtax->loyalty_amount                 = $loyalty_amount;
                    $specialtax->performance_base_amount        = $pb_amount;
                    $specialtax->collective_negotiation_amount  = $cn_amount;
                    $specialtax->mid_year_amount                = $myb_amount;
                    $specialtax->year_end_amount                = $yeb_amount;
                    $specialtax->pei_amount                     = $pei_amount;
                    $specialtax->for_year                       = $year;

                    $specialtax->save();
                }


                $beginningbalance = new BeginningBalance;

                if(isset($request->begbalance_id)){
                    $beginningbalance = $beginningbalance->find($request->begbalance_id);
                    $beginningbalance->employee_id = $request->employee_id;
                    $beginningbalance->gross_taxable_income = $gross_taxable_income_amount;
                    $beginningbalance->thirteen_month_pay = $thirteen_month_amount;
                    $beginningbalance->tax_withheld = $tax_withheld;
                    $beginningbalance->mandatory_deduction = $mandatory_amount;
                    $beginningbalance->as_of_date = $request->as_of_date;

                    $beginningbalance->save();

                }else{

                    $beginningbalance->employee_id = $request->employee_id;
                    $beginningbalance->gross_taxable_income = $gross_taxable_income_amount;
                    $beginningbalance->thirteen_month_pay = $thirteen_month_amount;
                    $beginningbalance->tax_withheld = $tax_withheld;
                    $beginningbalance->mandatory_deduction = $mandatory_amount;
                    $beginningbalance->as_of_date = $request->as_of_date;

                    $beginningbalance->save();

                }


                $previousemployer = new PreviousEmployer;
                if(isset($request->prevemployer_id)){

                    $previousemployer = $previousemployer->find($request->prevemployer_id);
                    $previousemployer->employee_id = $request->employee_id;
                    $previousemployer->gross_taxable_income = $gross_taxable_prev_employer_amount;
                    $previousemployer->thirteen_month_pay = $thirteen_month_prev_employer;
                    $previousemployer->tax_withheld = $tax_withheld_prev_employer;
                    $previousemployer->mandatory_deduction = $mandatory_deductions_prev_employer;
                    $previousemployer->as_of_date = $request->as_of_date_prev_employer;
                    $previousemployer->save();

                }else{

                    $previousemployer->employee_id = $request->employee_id;
                    $previousemployer->gross_taxable_income = $gross_taxable_prev_employer_amount;
                    $previousemployer->thirteen_month_pay = $thirteen_month_prev_employer;
                    $previousemployer->tax_withheld = $tax_withheld_prev_employer;
                    $previousemployer->mandatory_deduction = $mandatory_deductions_prev_employer;
                    $previousemployer->as_of_date = $request->as_of_date_prev_employer;
                    $previousemployer->save();

                }

                $response = json_encode(['status'=>true,'response'=>'Update Successfully']);

            }else{

                $ctr = 1;
                foreach ($dataTax as $key => $value) {
                    if(isset($value) && $value !== ""){
                        $taxrates =  new AnnualTaxRate;
                        $taxrates->employee_id          = $request->employee_id;
                        $taxrates->tax_amount           = $dataTax[$key];
                        $taxrates->pera_amount          = $dataPera[$key];
                        $taxrates->contribution_amount  = $dataCont[$key];
                        $taxrates->rata_amount          = (!empty($dataRata)) ? $dataRata[$key] : 0;
                        $taxrates->basic_amount         = $dataBasic[$key];
                        $taxrates->longevity_amount     = (!empty($dataLP)) ? $dataLP[$key] : 0;
                        $taxrates->hp_amount            = (!empty($dataHP)) ? $dataHP[$key] : 0;
                        $taxrates->lwop_amount          = (!empty($dataLWOP)) ? $dataLWOP[$key] : 0;
                        $taxrates->overtime_amount      = (!empty($dataOT)) ? $dataOT[$key] : 0;
                        $taxrates->for_month            = $ctr;
                        $taxrates->for_year             = $year;
                        $taxrates->created_by           = Auth::User()->id;
                        $taxrates->save();
                    }
                    $ctr++;

                }

                $specialtax =  new SpecialAnnualTax;
                $specialtax->employee_id                    = $request->employee_id;
                $specialtax->clothing_allowance_amount      = $clothing_amount;
                $specialtax->cash_gift_amount               = $cash_gift_amount;
                $specialtax->loyalty_amount                 = $loyalty_amount;
                $specialtax->performance_base_amount        = $pb_amount;
                $specialtax->collective_negotiation_amount  = $cn_amount;
                $specialtax->mid_year_amount                = $myb_amount;
                $specialtax->year_end_amount                = $yeb_amount;
                $specialtax->pei_amount                     = $pei_amount;
                $specialtax->for_year                       = $year;
                $specialtax->save();

                if(isset($request->as_of_date)){
                    $beginningbalance = new BeginningBalance;
                    $beginningbalance->employee_id = $request->employee_id;
                    $beginningbalance->gross_taxable_income = $gross_taxable_income_amount;
                    $beginningbalance->thirteen_month_pay = $thirteen_month_amount;
                    $beginningbalance->tax_withheld = $tax_withheld;
                    $beginningbalance->mandatory_deduction = $mandatory_amount;
                    $beginningbalance->as_of_date = $request->as_of_date;

                    $beginningbalance->save();
                }


                if(isset($request->as_of_date_prev_employer)){
                    $previousemployer = new PreviousEmployer;
                    $previousemployer->employee_id = $request->employee_id;
                    $previousemployer->gross_taxable_income = $gross_taxable_prev_employer_amount;
                    $previousemployer->thirteen_month_pay = $thirteen_month_prev_employer;
                    $previousemployer->tax_withheld = $tax_withheld_prev_employer;
                    $previousemployer->mandatory_deduction = $mandatory_deductions_prev_employer;
                    $previousemployer->as_of_date = $request->as_of_date_prev_employer;
                    $previousemployer->save();
                }



                $response = json_encode(['status'=>true,'response'=>'Save Successfully']);
            }
        }else{
            $response = json_encode(['status'=>false,'response'=>'Pleas click compute button']);
        }

        return $response;
    }


}
