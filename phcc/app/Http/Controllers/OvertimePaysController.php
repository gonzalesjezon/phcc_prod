<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\OvertimePay;
use App\Employee;
use App\Transaction;
use App\EmployeeInfo;
use App\EmployeeInformation;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\EmployeeStatus;
use App\AttendanceInfo;
use Auth;
use Datetime;
class OvertimePaysController extends Controller
{
    function __construct(){
    	$this->title = 'OVERTIME PAY';
    	$this->module = 'overtimepay';
        $this->module_prefix = 'payrolls';
    	$this->controller = $this;

    }

   public function index(){

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title,
                        'latest_year'   => $this->latestYear(),
                        'earliest_year' => $this->earliestYear(),
                        'months'        => $this->getMonths(),
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q              = Input::get('q');
        $year           = Input::get('year');
        $month          = Input::get('month');
        $_year           = Input::get('_year');
        $_month          = Input::get('_month');
        $chkovertime     = Input::get('check_overtime');
        $chk_overtime    = Input::get('chk_overtime');
        $employee_status = Input::get('status');


        $data = $this->searchName($q,$chk_overtime,$_year,$_month);

        if(isset($year) || isset($month) || isset($chkovertime) || isset($employee_status)){
            $data = $this->filter($year,$month,$chkovertime,$employee_status);
        }


        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    public function searchName($q,$chk_overtime,$year,$month){

        $cols = ['lastname','firstname'];

        $employee_info              = new EmployeeInformation;
        $employee                   = new Employee;
        $transaction                = new OvertimePay;

        $employee_info_id = $employee->select('id')->get()->toArray();

         $query = [];
        switch ($chk_overtime) {
            case 'wovertime':
               $employee_id = $transaction
               ->where('year',$year)
               ->where('month',$month)
               ->whereIn('employee_id',$employee_info_id)
               ->select('employee_id')->get()->toArray();

               $query = $employee->whereIn('id',$employee_id);
                break;
            case 'woovertime':
                $employee_id = $transaction
                ->where('year',$year)
                ->where('month',$month)
                ->whereIn('employee_id',$employee_info_id)
                ->select('employee_id')
                ->get()->toArray();
                $query = $employee->whereNotIn('id',$employee_id);
                break;

            default:
                $employee_id = $transaction->whereIn('employee_id',$employee_info_id)->select('employee_id')->get()->toArray();
                $query = $employee->whereNotIn('id',$employee_id);
                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($year,$month,$chkovertime,$employee_status){


        $empstatus_id = [];
        switch($employee_status){
            case 'plantilla':
                $empstatus_id = EmployeeStatus::where('category',1)->select('RefId')->get()->toArray();
            break;

            case 'nonplantilla':
                $empstatus_id = EmployeeStatus::where('category',0)->select('RefId')->get()->toArray();
            break;
        }

        $employeeinformation = new EmployeeInformation;

        $employee_id  = $employeeinformation->select('employee_id')->whereIn('employee_status_id',$empstatus_id)->get()->toArray();

        $query = [];
        $response = "";
        switch ($chkovertime) {
            case 'wovertime':
                $query = OvertimePay::select('employee_id');
                    if(isset($year)){
                        $query = $query->where('year',$year);
                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }
                    $query = $query->get()->toArray();

                    $response = Employee::whereIn('id',$query)->where('active',1)->orderBy('lastname','asc')->get();
                break;
            default:
                $query = OvertimePay::select('employee_id');
                    if(isset($year)){
                        $query = $query->where('year',$year);
                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }
                    $query = $query->get()->toArray();

                    $response = Employee::whereIn('id',$employee_id)
                                        ->whereNotIn('id',$query)
                                        ->where('active',1)
                                        ->orderBy('lastname','asc')
                                        ->get();

                break;
        }
        return $response;
    }

    public function store(Request $request){

        $overtimepay = OvertimePay::find($request->overtime_id);

        if(empty($overtimepay)){
            $overtimepay = new OvertimePay;
        }
        $overtimepay->fill($request->all());
        $overtimepay->actual_regular_amount = str_replace(',', '', $request->actual_regular_amount);
        $overtimepay->actual_holiday_amount = str_replace(',', '', $request->actual_holiday_amount);
        $overtimepay->actual_other_amount = str_replace(',', '', $request->actual_other_amount);
        $overtimepay->actual_regular_rate = str_replace(',', '', $request->actual_regular_rate);
        $overtimepay->actual_holiday_rate = str_replace(',', '', $request->actual_holiday_rate);
        $overtimepay->actual_other_rate = str_replace(',', '', $request->actual_other_rate);
        $overtimepay->other_rate = str_replace(',', '', $request->other_rate);
        $overtimepay->max_amount_to_avail = str_replace(',', '', $request->max_amount_to_avail);
        $overtimepay->overtime_pay_for_period = str_replace(',', '', $request->overtime_pay_for_period);
        $overtimepay->regular_rate = str_replace(',', '', $request->regular_rate);
        $overtimepay->holiday_rate = str_replace(',', '', $request->holiday_rate);
        $balance_forwarded = (isset($request->balance_forwarded)) ? str_replace(',', '', $request->balance_forwarded) : 0;

        $previous_balance = $this->previousBalance($overtimepay->year,$overtimepay->month,$overtimepay->employee_id);

        $overtimepay->balance_forwarded = $balance_forwarded + $previous_balance;
        if($previous_balance == 0){
            $balance_forwarded = 0;
        }
        $overtimepay->balance_previous_month = $previous_balance;
        if($overtimepay->exists()){
            $overtimepay->updated_by = Auth::id();
        }else{
            $overtimepay->created_by = Auth::id();
        }
        $overtimepay->save();

        return json_encode(['status'=>true,'response'=>'Update Successfully!']);
    }

    public function previousBalance($year,$month,$id){
        $month = date('m',strtotime($month));
        $prev_month = date('m',strtotime($month.'-1 month'));
        $dateObj = Datetime::createFromFormat('!m',$prev_month);
        $month_name = $dateObj->format('F');
        $overtimepay = new OvertimePay;
        $overtimepay = $overtimepay->where('employee_id',$id)
                                ->where('year',$year)
                                ->where('month',$month_name)->first();

        $prev_balance = 0;
        if(isset($overtimepay)){
            $prev_balance = $overtimepay->balance_forwarded;
        }

        return $prev_balance;

    }

    public function getOvertimeInfo(){
        $data = Input::all();

        $year  = $data['year'];
        $month = $data['month'];

        $employeeinfo = new EmployeeInfo;
        $nonplantilla = new NonPlantillaEmployeeInfo;
        $overtimepay  = new OvertimePay;

        $query['plantilla'] = $employeeinfo
        ->where('employee_id',$data['employee_id'])
        ->first();

        $query['nonplantilla'] = $nonplantilla
        ->where('employee_id',$data['employee_id'])
        ->first();

        $query['overtimepay'] = $overtimepay
        ->where('employee_id',@$data['employee_id'])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('created_at','desc')
        ->first();

        return json_encode($query);
    }

    public function storeOvertimeInfo(Request $request){

        foreach ($request['employee_id'] as $key => $value) {

            if(isset($value)){
                $overtimepay      = new OvertimePay;
                // if($request->status == 'plantilla' ){
                //     $employeeinfo = EmployeeInfo::where('employee_id',$value)->first();
                // }else{
                //     $employeeinfo = NonPlantillaEmployeeInfo::where('employee_id',$value)->first();
                // }
                $overtimepay->employee_id                     = $value;
                $overtimepay->year                            = $request->year;
                $overtimepay->month                           = $request->month;
                $overtimepay->save();
            }


        }

        return json_encode(['status'=>true,'response'=>'Save Successfully!']);

    }

    public function deleteOvertime(){
        $data = Input::all();

        $overtimepay = new OvertimePay;

        foreach ($data['empid'] as $key => $value) {

            $overtimepay->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
    public function showDatatable(){
        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.show',$response);
    }

}
