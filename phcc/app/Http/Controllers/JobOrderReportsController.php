<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Employee;
use App\AttendanceInfo;
use App\OvertimePay;
class JobOrderReportsController extends Controller
{
    function __construct(){
		$this->title = 'JOB ORDER';
    	$this->module = 'joborder';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


		$employeestatus   = new EmployeeStatus;
		$employeeinfo     = new EmployeeInformation;
        $employee         = new Employee;

        $empstatus_id = $employeestatus
        ->where('category',0)
        ->select('RefId')
        ->get()->toArray();

        $employee_id  = $employeeinfo
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_number')
        ->get()->toArray();

    	$employeeinfo = $employee->whereIn('employee_number',$employee_id)->where('active',0)->orderBy('lastname','asc')->get();

    	$response = array(
                        'employeeinfo'  => $employeeinfo,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getJobOrder(){

        $data = Input::all();

        $transaction    =  new NonPlantillaTransaction;
        $employeeinfo   = new NonPlantillaEmployeeInfo;
        $attendanceinfo = new AttendanceInfo;
        $overtime       = new OvertimePay;

        $query['transaction'] = $transaction->with(['employees','positionitems'=>function($qry){ $qry->with('positions'); },'offices'])
        									->where('year',$data['year'])
        									->where('month',$data['month'])
                                            ->where('sub_pay_period',@$data['sub_pay_period'])
        									->where('employee_id',$data['id'])
                                            ->orderBy('created_at','desc')
        									->first();

        $query['employeeinfo'] = $employeeinfo->with('taxpolicyOne','taxpolicyTwo')->where('employee_id',$data['id'])
        									->first();

        $query['attendanceinfo'] =  $attendanceinfo->where('employee_id',$data['id'])
                                                ->where('month',$data['month'])
                                                ->where('year',$data['year'])
                                                ->first();

         $query['overtime'] =  $overtime->where('employee_id',$data['id'])
                                                ->where('month',$data['month'])
                                                ->where('year',$data['year'])
                                                ->where('pay_period',$data['pay_period'])
                                                ->where('sub_pay_period',@$data['sub_pay_period'])
                                                ->orderBy('created_at','desc')
                                                ->first();

        return json_encode($query);
    }
}
