<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\SpecialPayrollTransaction;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
class CAReportsController extends Controller
{

    function __construct(){
        $this->title = 'CLOTHING ALLOWANCE';
        $this->module = 'clothing_reports';
        $this->module_prefix = 'payrolls/reports';
        $this->controller = $this;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $response = array(
                'employee'      => $query,
                'module'        => $this->module,
                'controller'    => $this->controller,
                'module_prefix' => $this->module_prefix,
                'title'         => $this->title,
                'months'             => config('params.months'),
                'latest_year'        => $this->latestYear(),
                'earliest_year'      => $this->earliestYear(),
                'current_month'      => (int)date('m')
                );

        return view($this->module_prefix.'.'.$this->module,$response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction =  new SpecialPayrollTransaction;


        $query = $transaction
        ->join('pms_employees as e','e.id','=','pms_specialpayroll_transactions.employee_id')
        ->leftJoin('office as o','o.RefId','=','pms_specialpayroll_transactions.office_id')
        ->with([
            'employees',
            'offices',
            'positions',
            'salaryinfo',
            'responsibilities',
            'employeeinformation'
        ])
        ->where('month',$month)
        ->where('year',$year)
        ->where('status','ua')
        ->orderBy('o.sort_by','asc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(isset($query)){
            foreach ($query as $key => $value) {
                $data[strtoupper(@$value->offices->Name)][$key] = $value;
            }
        }else{

            $data = [];
        }

        return json_encode([
            'transaction'=>$data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
