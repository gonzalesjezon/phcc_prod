<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Auth;
use App\Office;
class OfficesController extends Controller
{
    function __construct(){
    	$this->title 		 = 'OFFICES SETUP';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module 		 = 'offices';
    	$this->controller 	 = $this;
    }

    public function index(){

    	$response = array(
						'module'        => $this->module,
						'controller'    => $this->controller,
		                'module_prefix' => $this->module_prefix,
						'title'		    => $this->title
					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    private function get_records($q){

        $cols = ['code','name'];

        $query = Office::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->orderBy('name','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	$offices = new Office;

    	if(isset($request->office_id)){

    		$offices = Office::find($request->office_id);

	    	$offices->code 	      = $request->code;
	    	$offices->name 	      = $request->name;
            $offices->sequence_no = $request->sequence_no;
	    	$offices->updated_by  = Auth::User()->id;

	    	$offices->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
    	}else{

    		$this->validate($request,[
	    		'code' 	=> 'required|unique:pms_offices',
	    		'name' 	=> 'required',
                'sequence_no' => 'required'
    		]);

    		$offices->code 	      = $request->code;
	    	$offices->name 	      = $request->name;
            $offices->sequence_no = $request->sequence_no;
	    	$offices->created_by  = Auth::User()->id;

	    	$offices->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}


    	return $response;
    }
}
