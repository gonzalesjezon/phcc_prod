<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\Benefit;
use App\BenefitInfo;
use App\EmployeeInfo;
use App\Bank;
use App\SalaryInfo;
class PayrollConfigurationsController extends Controller
{
    function __construct(){
    	$this->title = 'Payroll Configuration';
    	$this->module = 'payrollconfigurations';
        $this->module_prefix = 'payrolls/admin';
    	$this->controller = $this;

    }

    public function index(){

    	$gsis_policy 	   = GsisPolicy::get();
    	$philhealth_policy = PhilhealthPolicy::get();
    	$pagibig_policy    = PagibigPolicy::get();
    	$banks 			   = Bank::get();
    	$benefits 		   = Benefit::whereNotIn('code',['EYB','ME'])->get();

    	$response = array(
    					'benefits' 			=> $benefits,
    					'gsis_policy'		=> $gsis_policy,
    					'philhealth_policy'	=> $philhealth_policy,
    					'pagibig_policy'	=> $pagibig_policy,
    					'banks'				=> $banks,
    					'module'        	=> $this->module,
    					'controller'   	 	=> $this->controller,
                        'module_prefix' 	=> $this->module_prefix,
    					'title'		    	=> $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

    	$this->validate(request(),[
    		'number_of_days_in_a_year' 	=> 'required',
    		'number_of_days_in_a_year' 	=> 'required',
    		'bank_id' 					=> 'required',
    		'gsis_policy_id' 			=> 'required',
    		'philhealth_policy_id' 		=> 'required',
    		'pagibig_policy_id' 		=> 'required',
    	]);

    	$ee_percent = $request->ee_percentage;
    	$er_percent = $request->er_percentage;
    	$above 		= $request->above;
    	$below 		= $request->below;
    	$employeeinfo = new EmployeeInfo;
    	$benefitinfo  = new BenefitInfo;

    	$query = SalaryInfo::get();

    	foreach ($query as $key => $value) {

    		foreach ($request->benefit_id as $k => $val) {
    			$benefitinfo->employee_id = $value->employee_id;
    			$benefitinfo->benefit_amount = @$val;
    			$benefitinfo->benefit_id = @$k;

    			$benefitinfo->save();
    		}

    		$basic_salary = $value->salary_new_rate;

    		$gsis_employee_share 		= $this->compute_gsis_employee_share($basic_salary,$ee_percent);
    		$gsis_employer_share 		= $this->compute_gsis_employer_share($basic_salary,$er_percent);
    		$philhealth_share 			= $this->compute_philhealth_share($above,$below,$basic_salary);


			$employeeinfo->philhealth_contribution 	= $philhealth_share['ee_share'];
			$employeeinfo->er_philhealth_share 	    = $philhealth_share['er_share'];
			$employeeinfo->gsis_contribution 		= $gsis_employee_share;
			$employeeinfo->er_gsis_share 	 		= $gsis_employer_share;
			$employeeinfo->pagibig_contribution 	= 100;
			$employeeinfo->er_pagibig_share 		= 100;
    		$employeeinfo->employee_id 				= $value->employee_id;
    		$employeeinfo->gsispolicy_id 			= $request->gsis_policy_id;
    		$employeeinfo->philhealthpolicy_id 		= $request->philhealth_policy_id;
    		$employeeinfo->pagibigpolicy_id 		= $request->pagibig_policy_id;
    		$employeeinfo->bank_id 					= $request->bank_id;
    		$employeeinfo->mid_year_bonus 			= ($request->chk_mid_year) ? 1 : NULL;
    		$employeeinfo->year_end_bonus 			= ($request->chk_year_end) ? 1 : NULL;

    		$employeeinfo->save();
    	}

    	return json_encode(['status'=>true,'response'=>'Saving Successfully!']);
    }

    public function compute_gsis_employee_share($basic_salary,$ee_percent){
    	return ((int)$basic_salary * (float)$ee_percent);
    }
    public function compute_gsis_employer_share($basic_salary,$er_percent){
    	return ((int)$basic_salary * (float)$er_percent);
    }
    public function compute_philhealth_share($above,$below,$basic_salary){

    	if((int)$basic_salary >= 41999 ){
    		$share['ee_share'] = $above/2;
    		$share['er_share'] = $share['ee_share'];
    	}else{
    		$share['ee_share'] = ((int)$basic_salary*$below)/2;
    		$share['er_share'] = $share['ee_share'];
    	}

    	return $share;
    }
}
