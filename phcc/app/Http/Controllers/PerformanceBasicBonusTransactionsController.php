<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\EmployeeInfo;
use App\PositionItem;
use App\Office;
use DateTime;
class PerformanceBasicBonusTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'PERFORMANCE BASE BONUS TRANSACTIONS';
    	$this->controller = $this;
    	$this->module = 'pbbtransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	        	=> $this->title,
           'controller'         => $this->controller,
           'module'	        	=> $this->module,
           'module_prefix'      => $this->module_prefix,
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'offices'            => Office::get()

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $_year      = Input::get('_year');
        $_month     = Input::get('_month');
        $officeId   = Input::get('officeId');
        $_officeId  = Input::get('_officeId');
        $check_pbb 	= Input::get('check_pbb');
        $checkpbb 	= Input::get('checkpbb');
        $data = "";

        $data = $this->searchName($q,$check_pbb,$_year,$_month,$_officeId);

        if(isset($year) || isset($month) || isset($checkpbb) || isset($officeId)){
            $data = $this->filter($year,$month,$checkpbb,$officeId);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkpbb,$year,$month,$office_id){

        $employee_status            = new EmployeeStatus;
        $employeeinfo               = new EmployeeInformation;
        $employee                   = new Employee;
        $transaction                = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname'];

        $empstatus_id = $employee_status
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinfo
        ->orWhere('office_id',$office_id)
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

        $employee_id = $transaction->whereIn('employee_id',$employeeinfo)
               ->where('year',$year)
               ->where('status','pbb')
               ->select('employee_id')
               ->get()
               ->toArray();

        $query = [];
        switch ($checkpbb) {
            case 'wpbb':

               $query = $employee->whereIn('id',$employee_id);

                break;

            case 'wopbb':

               $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$employeeinfo);

                break;

            default:

                $query = $employee
                ->whereIn('id',$employeeinfo);

                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }

    public function filter($year,$month,$checkpbb,$office_id){

        $employee_status        = new EmployeeStatus;
        $transaction            = new SpecialPayrollTransaction;
        $employee               = new Employee;
        $employeeinfo           = new EmployeeInformation;

        $empstatus_id = $employee_status
        ->where('category',1)->select('RefId')
        ->get()->toArray();

        $employeeinfo  = $employeeinfo
        ->orWhere('office_id',$office_id)
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()->toArray();

        // $salary_info_employee_id = $employeeinfo->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";
        switch ($checkpbb) {
            case 'wpbb':

                $query =  $transaction->select('employee_id')->where('status','pbb');
                    if(isset($year)){
                        $query = $query->where('year',$year);
                    }
                    // if(isset($month)){
                    //     $query = $query->where('month',$month);
                    // }
                    $query = $query->get()->toArray();
                    $response = $employee->whereIn('id',$query)->where('active',1)
                                        ->orderBy('lastname','asc')->get();
                break;

            case 'wopbb':

                 $query =  $transaction->select('employee_id')->where('status','pbb');
                    if(isset($year)){
                        $query = $query->where('year',$year);
                    }
                    // if(isset($month)){
                    //     $query = $query->where('month',$month);
                    // }
                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employeeinfo)
                                ->whereNotIn('id',$query)->where('active',1)
                                ->orderBy('lastname','asc')->get();

                break;
            default:
                $response = $employee->whereIn('id',$employeeinfo)
                                ->where('active',1)
                                ->orderBy('lastname','asc')->get();
                break;
        }

        return $response;
    }


    public function processPbb(Request $request){
        $data = Input::all();
        $year = $data['year'];
        $rate = $data['rate'];
        $office_id = $data['office_id'];

        $rate = ($rate / 100);

        $employeeinformation = new EmployeeInformation;
        $employee            = new Employee;
        $payrollinfo         = new EmployeeInfo;

        foreach ($data['list_id'] as $key => $value) {

            if(isset($value)){

                $employeeinfo   = $employeeinformation->where('employee_id',$value)->first();
                $employee       = $employee->where('id',$value)->first();
                $payrollinfo    = $payrollinfo->where('id',$value)->first();

                $assumption_date = ($employeeinfo->assumption_date) ? $employeeinfo->assumption_date : '';
                $basic_amount = (float)$payrollinfo->monthly_rate_amount;

                $first_date = new DateTime($assumption_date);
                $nov = date('Y').'-10-31 00:00:00';

                $second_date = DateTime::createFromFormat('Y-m-d H:i:s', $nov);
                $interval = $first_date->diff($second_date);

                // $x = $interval->format('%Y years %M months and %D days.');
                $number_of_month = (int)$interval->format('%M');
                $number_of_year = (int)$interval->format('%Y');

                $pbbAmount = $basic_amount * $rate;

                $transaction = new SpecialPayrollTransaction;

                if($number_of_year !== 0){
                    $transaction->percentage = 100;
                    $transaction->amount = $pbbAmount;
                }else{
                    if($number_of_month >= 8 && $number_of_month < 9){
                        $transaction->percentage = 90;
                        $transaction->amount = $pbbAmount * .90;
                    }else if($number_of_month >= 7 && $number_of_month < 8){
                        $transaction->percentage = 80;
                        $transaction->amount = $pbbAmount * .80;
                    }else if($number_of_month >= 6 && $number_of_month < 7){
                        $transaction->percentage = 70;
                        $transaction->amount = $pbbAmount * .70;
                    }else if($number_of_month >= 5 && $number_of_month < 6){
                        $transaction->percentage = 60;
                        $transaction->amount = $pbbAmount * .60;
                    }else if($number_of_month >= 4 && $number_of_month < 5){
                        $transaction->percentage = 50;
                        $transaction->amount = $pbbAmount * .50;
                    }else if($number_of_month >= 3 && $number_of_month < 4){
                        $transaction->percentage = 40;
                        $transaction->amount = $pbbAmount * .40;
                    }
                }

                $transaction->employee_id       = $value;
                $transaction->office_id         = $office_id;
                $transaction->position_id       = @$employeeinfo->position_id;
                $transaction->division_id       = @$employeeinfo->division_id;
                $transaction->employee_number   = @$employee->employee_number;
                $transaction->rate              = @$rate;
                $transaction->year              = $year;
                $transaction->status            = 'pbb';
                $transaction->created_by        = Auth::User()->id;
                $transaction->save();

            }
        }

        $response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

        return $response;
    }

    public function showPbbDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getPbbInfo(){
        $data = Input::all();

        $year = @$data['year'];
        $month = @$data['month'];
        $employee_id = $data['employee_id'];

        $transaction =  new SpecialPayrollTransaction;

        $query = $transaction->with('positions','offices')
                    ->where('year',$year)
                    ->where('status','pbb')
                    ->where('employee_id',@$employee_id)
                    ->orderBy('created_at','desc')
                    ->first();

        return json_encode($query);
    }

    public function deletePbb(){
        $data = Input::all();

        $transaction = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $transaction->where('employee_id',$data['empid'][$key])
            ->where('year',$data['year'])
            ->where('status','pbb')
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
