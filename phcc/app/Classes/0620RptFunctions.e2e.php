<?php
   function chkdate($date) {
      /*yyyy-mm-dd*/
      $date_arr = explode("-",$date);
      if (
         isset($date_arr[0]) &&
         isset($date_arr[1]) &&
         isset($date_arr[2])
      ) {
         return checkdate($date_arr[1],$date_arr[2],$date_arr[0]);
      } else {
         return false;
      }
      /*checkdate(month,day,year);*/
   }
   function chkDateRange($dtFrom,$dtTo) {
      if ($dtFrom != "" && !chkdate($dtFrom)) $GLOBALS['errmsg'] .= "<li>Invalid Date <i class='errmsg'>$dtFrom</i></li>";
      if ($dtTo != "" && !chkdate($dtTo)) $GLOBALS['errmsg'] .= "<li>Invalid Date <i class='errmsg'>$dtTo</i></li>";
      if ($dtFrom != "" && $dtTo != "") {
         if ($dtFrom > $dtTo) {
            $GLOBALS['errmsg'] .= "<li>Invalid Date Range (lower to higher): <i class='errmsg'>$dtFrom to $dtTo</i></li>";
         }
      } else if ($dtFrom == "" && $dtTo != "") {
         $GLOBALS['errmsg'] .= "<li>Invalid Date Range (make it pair): <i class='errmsg'>null to $dtTo</i></li>";
      } else if ($dtFrom != "" && $dtTo == "") {
         $GLOBALS['errmsg'] .= "<li>Invalid Date Range (make it pair): <i class='errmsg'>$dtFrom to null</i></li>";
      }
   }
   function chk_get_row($empRefid,$table,$dtFrom,$dtTo,$fld) {
      if ($dtFrom != "" && $dtTo != "") {
         $rsTable = f_Find($table,"WHERE EmployeesRefId = $empRefid AND $fld BETWEEN '$dtFrom' AND '$dtTo' ORDER BY $fld DESC LIMIT 1");
         if ($rsTable) {
            $rs_assoc = mysqli_fetch_assoc($rsTable);
            return $rs_assoc;
         }
         else return false;
      } else {
         $rsTable = f_Find($table,"WHERE EmployeesRefId = $empRefid ORDER BY $fld DESC LIMIT 1");
         if ($rsTable) {
            $rs_assoc = mysqli_fetch_assoc($rsTable);
            return $rs_assoc;
         }
         else return true;
      }
   }

   function getRptName($drpvalue) {
      $rptName = [
      "rptSummaryAttendance"=>"Monthly Summary Report For Late, Undertime And Absences",
      "rptSummaryDailyAttendance"=>"Attendance Monitoring Report",
      "rptAbsentList"=>"Monthly Summary Report For Absences",
      "rptLateList"=>"Monthly Summary Report For Late",
      "rptUndertimeList"=>"Monthly Summary Report For Undertime",
      "rptHabitualTUT"=>"Habitual Late And Undertime Report",
      "rptHabitualMemo"=>"Memorandum",
      "rptLeave_Card"=>"Employee Leave Card",  // PIDS
      "rptSummaryLeave"=>"Summary of Leave Balances",
      "rptSummaryLeaveDetail"=>"Detailed of Leave Balances",
      "rptLeaveCertificate"=>"Certificate of Leave Balances",
      "rptLeaveWOP"=>"Summary of Leave W/O Pay",
      "rptOfficialBusiness"=>"(Office Authority) Official Business",
      "rptTravelOrder"=>"(Office Authority) Travel Order",
      "rptOfficeOrder"=>"(Office Authority) Office Order",
      "rptSummaryCTO"=>"Summary of CTO",
      "rptCTODetail"=>"Detail of CTO",
      "rptCOCCertificate"=>"Certificate of COC Earned",
      "rptLoanSummary"=>"Loan Report Summary",
      "rptConsoleLoan"=>"Loan Report Detail - Console Loan",
      "rptPolicyLoan"=>"Loan Report Detail - Policy Loan",
      "rptPagibigLoan"=>"Loan Report Detail - Pagibig Loan",
      "rptEmergencyLoan"=>"Loan Report Detail - Emergency Loan",
      "rptLandbankLoan"=>"Loan Report Detail - Landbank Loan",
      "rptLoanBalance"=>"Loan Report Balance",
      "rptPayrollRegister"=>"Payroll Register",
      "rptPayslip"=>"Payslip",
      "rptGSIS_RemittanceExcel"=>"GSIS-Remittance Excel",
      "rptGSIS_LoanSummary"=>"GSIS-Loan Summary",
      "rptGSIS_PremiumCertificate"=>"GSIS-Premium Certificate",
      "rptGSIS_LoanCertificate"=>"GSIS-Loan Certificate",
      "rptGSIS_ConsolidatedPremiumRemittances"=>"GSIS-Consolidated Premium Remittances",
      "rptGSIS_ConsolidatedLoanRemittances"=>"GSIS-Consolidated Loan Remittances",
      "rptPHILHEALTH_ContributionList"=>"PHILHEALTH-Contribution List",
      "rptPHILHEALTH_Certificate"=>"PHILHEALTH-Certificate",
      "rptPHILHEALTH_MonthlyDiskette"=>"PHILHEALTH-Monthly Diskette",
      "rptPHILHEALTH_QuarterlyDiskette"=>"PHILHEALTH-Quarterly Diskette",
      "rptPHILHEALTH_EmployersMonthlyReport"=>"PHILHEALTH-RF-1 Employer's Monthly Report",
      "rptPHILHEALTH_EmployersQuarterlyReport"=>"PHILHEALTH-RF-1 Employer's Quarterly Report",
      "rptPAGIBIG_ContributionList"=>"PAGIBIG-Contribution List",
      "rptPAGIBIG_Diskette"=>"PAGIBIG-Diskette",
      "rptPAGIBIG_Certificate"=>"PAGIBIG-Certificate",
      "rptPAGIBIG_LoanCollectionList"=>"PAGIBIG-Loan Collection List",
      "rptPAGIBIG_LoanDiskette"=>"PAGIBIG-Loan Diskette",
      "rptPAGIBIG_LoanCertificate"=>"PAGIBIG-Loan Certificate",
      "rptPAGIBIG_MembershipRemittanceForm"=>"PAGIBIG-M1-1 Membership Remittance Form",
      "rptPAGIBIG_MonthlyRemittanceSchedule"=>"PAGIBIG-P2-4 Monthly Remittance Schedule (Loan)",
      "rptCertLeaveBalances"=>"Certificate Of Leave Balances",
      "rptAuthorizationToField"=>"Authorization To Go On Field",
      "rptCESLeave"=>"CES Official On Leave",
      "rptMontlyOvertimePay"=>"Monthly Report On Overtime Pay",
      "rptAuthorityRenderOvertimeWork"=>"Request For Authority to Render Overtime Work",
      "rptSummaryLWOP"=>"Summary of Leave Without Pay/Late(s)/Undertime(s)",
      "rptSummaryLWOP"=>"Summary of Leave Without Pay/Late(s)/Undertime(s)",
      "rptEntitlementRata"=>"Entitlement To Rata",
      "rptEmployeesMovement"=>"CES Executive Movement Report",
      "rptNonCESPosition"=>"Update On CESOS and CES Eligibles Occupying Non-CES Positions",
      "rptOccupancy"=>"Career Executive Service (CES) Occupancy Reports",
      "rptOfficialPendingCases"=>"Officials with pending cases",
      "rptVacantCESPosition"=>"Vacant CES Position",
      "rptTax"=>"TAX",
      "rptEmp_Rata" => "REPRESENTATION AND TRANSPORTATION ALLOWANCE",
      "rptEME" => "GENERAL PAYROLL FOR EXTRAORDINARY AND MISCELLANEOUS EXPENSES",
      "rptEmp_COMMExpAllot" => "COMMUNICATIONS EXPENSE ALLOTMENT",
      "rpt_COE-COMPEN-PLANTILLA" => "CERTIFICATE OF EMPLOYMENT AND COMPENSATION",
      "rpt_CertOfLeaveCredits" => "CERTIFICATE OF LEAVE CREDITS",
      "rpt_COE-JOBBERS-COMPEN" => "CERTIFICATE",
      "rpt_HalfYearEndAndCashGifts" => "PAYROLL- 1/2 YEAR-END BONUS AND 1/2 CASH GIFT",
      "rpt_payrollTransfer" => "PAYROLL TRANSFER",
      "rpt_InitialSalary" => "INITIAL SALARY",
      "rpt_LastPaymentofSalary" => "LAST PAYMENT OF SALARY",
      "rpt_YearEndAndCashGifts" => "PAYROLL TRANSFER - YEAR-END BONUS AND 1/2 CASH GIFT",
      "rpt_ClothingAllowance" => "PAYROLL SHEET FOR CLOTHING ALLOWANCE",
      "rpt_Monetization" => "REGULAR / 50% MONETIZATION OF EARNED VACATION/SICK LEAVE CREDITS",
      "rpt_PEI" => "PERFORMANCE ENHANCEMENT INCENTIVE (PEI)",
      "rpt_Payslip" => "PAYSLIP",
      "PCC_rpt_LeaveCard" => "LEAVE CARD",
      "PCC_rptCertLeaveCredits" => "CERTIFICATE OF LEAVE CREDITS",
      "rpt_Appt_Transmittal_And_Action_Form" => "Appointment Transmittal And Action Form",
      "rpt_RAI" => "Report on Appointments Issued",
      "rpt_DIBAR" => "REPORT ON  DATABASE OF INDIVIDUALS BARRED FROM ENTERING SERVICE <br> AND TAKING CIVIL SERVICE EXAMINATIONS (DIBAR)",
      "rpt_Req_For_Publication_Of_Vacant_Position" => "Request for Publication of Vacant Positions",
      "Form_Medical_Certificate" => "Medical Certificate",
      "Form_OB" => "Official Business Pass Slip <br> (for Outside Official Business during Office Hours)",
      "Form_COC_Certificate" => "Certificate Of COC Earne",
      "Form_EmpMovement" => "HR ACTION NOTICE",
      "Form_Pos_Description" => "POSITION DESCRIPTION FORM <br> DBM-CSC Form No. 1"
      ];
      return $rptName[$drpvalue];
   }

?>