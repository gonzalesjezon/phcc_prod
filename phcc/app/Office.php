<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{

	protected $primaryKey = 'RefId';
    protected $table = 'office';
    protected $fillable = [
    	'Code',
    	'Name',
    	'sort_by',
    	'sequence_no',
        'created_by',
        'updated_by'

    ];

}
