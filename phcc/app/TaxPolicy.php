<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxPolicy extends Model
{
    protected $table = 'pms_taxespolicy';
    protected $fillable = [
    	'policy_name',
    	'pay_period',
    	'deduction_period',
    	'policy_type',
    	'based_on',
    	'computation',
    	'is_withholding',
        'job_grade_rate',
    	'remarks',
    ];
}
