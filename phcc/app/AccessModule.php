<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessModule extends Model
{
    protected $table = 'access_modules';
    protected $fillable = [
        'modules',
        'description',
        'crud'
    ];
}
