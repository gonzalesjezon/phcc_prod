<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RataExport implements FromView
{
	public function __construct($data){
		$this->data = $data;
	}
	
    public function view(): View
    {
        return view('payrolls.exports.rata', [
            'data' => $this->data
        ]);

    }
}