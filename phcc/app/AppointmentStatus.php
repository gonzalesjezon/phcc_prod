<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentStatus extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'apptstatus';

    protected $fillable = [
    	'Code',
    	'Name'
    ];
}
