<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponsibilityCenter extends Model
{
    protected $table = 'pms_responsibility_center';
    protected $fillable = [
    	'code',
    	'name',
    	'created_by',
    	'updated_by'
    ];
}
