<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostedReport extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pms_posted_reports';
    protected $fillable = [
		'employee_id',
		'signatory_id',
		'report_type',
        'pay_period',
		'code_1',
		'code_2',
		'year',
		'month'
    ];

    public function signatory(){
    	return $this->belongsTo('App\Signatory');
    }

    public function employee(){
        return $this->belongsTo('App\Employee');
    }

    public function transaction(){
        return $this->belongsTo('App\Transaction','employee_id','employee_id');
    }
}
