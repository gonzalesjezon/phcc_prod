<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessType extends Model
{
    protected $table = 'access_types';
    protected $fillable = [
        'name'
    ];

    public function accessRights(){
        return $this->hasMany('App\AccessRight');
    }
}
