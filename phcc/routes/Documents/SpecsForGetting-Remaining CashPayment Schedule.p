DEFINE TEMP-TABLE CashPaySchedule NO-UNDO
  FIELD SchoolKey                 AS INTEGER
  FIELD StudentRecKey             AS INTEGER
  FIELD EnrollmentKey             AS INTEGER
  FIELD CashPayScheduleKey        AS INTEGER
  FIELD DueDate                   AS DATE  
  FIELD AmountDue                 AS DECIMAL
  FIELD AmountPaid                AS DECIMAL
  FIELD AmountPerPayment          AS DECIMAL
  FIELD LastUpdateDate            AS DATE  
    INDEX idx_CashPaySchedule StudentRecKey EnrollmentKey DueDate.

EMPTY TEMP-TABLE CashPaySchedule NO-ERROR.
FOR EACH rgmsms.StudentCashPaySchedules NO-LOCK 
	WHERE rgmsms.StudentCashPaySchedules.SchoolKey     EQ RGMSMS.StudentEnrollments.SchoolKey   
	  AND rgmsms.StudentCashPaySchedules.StudentRecKey EQ RGMSMS.StudentEnrollments.StudentRecKey
	  AND rgmsms.StudentCashPaySchedules.EnrollmentKey EQ RGMSMS.StudentEnrollments.EnrollmentKey:

	dtPaymentSchedule = rgmsms.StudentCashPaySchedules.FirstDate.
	IF rgmsms.StudentCashPaySchedules.DayOfMonthDue LT DAY(dtPaymentSchedule)
		AND rgmsms.StudentCashPaySchedules.DayOfMonthDue > 0 
	THEN 
		ASSIGN
			dtPaymentSchedule = DATE(STRING(MONTH(rgmsms.StudentCashPaySchedules.FirstDate)) + "/" + STRING(rgmsms.StudentCashPaySchedules.DayOfMonthDue) + "/" + STRING(YEAR(rgmsms.StudentCashPaySchedules.FirstDate)))
			dtPaymentSchedule = ADD-INTERVAL(dtPaymentSchedule,1,"MONTHS").

	ASSIGN
		inPaymentScheduleYear  = YEAR(dtPaymentSchedule)
		inPaymentScheduleMonth = MONTH(dtPaymentSchedule).

	IF rgmsms.StudentCashPaySchedules.NumberOfPayments GT 0 THEN 
	DO:

		cntEntry = 0.
		DO cntEntry = 1 TO rgmsms.StudentCashPaySchedules.NumberOfPayments:

			CREATE CashPaySchedule.
			BUFFER-COPY rgmsms.StudentCashPaySchedules TO CashPaySchedule.
			ASSIGN
				CashPaySchedule.CashPayScheduleKey = rgmsms.StudentCashPaySchedules.StudentPayScheduleKey
				CashPaySchedule.AmountPerPayment   = rgmsms.StudentCashPaySchedules.AmountPerPayment
				CashPaySchedule.AmountDue          = rgmsms.StudentCashPaySchedules.AmountPerPayment
				CashPaySchedule.DueDate            = dtPaymentSchedule.

			CASE rgmsms.StudentCashPaySchedules.PaymentInterval:
				WHEN 1 THEN 
					dtPaymentSchedule = dtPaymentSchedule + 7.
				WHEN 2 THEN 
					dtPaymentSchedule = dtPaymentSchedule + 14.
				WHEN 3 THEN 
					ASSIGN
						dtPaymentSchedule = dtPaymentSchedule + 14
						inDayInMonth = f_GETDAYSINMONTH(INPUT inPaymentScheduleMonth,
														INPUT inPaymentScheduleYear)
						dtPaymentSchedule = dtPaymentSchedule + INT(inDayInMonth / 2).                                
				WHEN 4 THEN 
					dtPaymentSchedule = ADD-INTERVAL(dtPaymentSchedule,1,"MONTHS").
				WHEN 5 THEN 
					dtPaymentSchedule = ADD-INTERVAL(dtPaymentSchedule,3,"MONTHS").
			END CASE.                                                       
		END.
		
		/* Get How Much was already Paid */
		deTotalPaidForSched = 0.
		FOR EACH rgmsms.StudentCashPayments NO-LOCK 
			WHERE rgmsms.StudentCashPayments.SchoolKey      EQ rgmsms.StudentCashPaySchedules.SchoolKey    
			  AND rgmsms.StudentCashPayments.StudentRecKey  EQ rgmsms.StudentCashPaySchedules.StudentRecKey
			  AND rgmsms.StudentCashPayments.EnrollmentKey  EQ rgmsms.StudentCashPaySchedules.EnrollmentKey
			  AND rgmsms.StudentCashPayments.PartOfSchedule BEGINS "Y"
			  AND rgmsms.StudentCashPayments.StudentPayScheduleKey EQ rgmsms.StudentCashPaySchedules.StudentPayScheduleKey
			  BY rgmsms.StudentCashPayments.Date:
			  deTotalPaidForSched = deTotalPaidForSched + rgmsms.StudentCashPayments.Amount.
		END.
		
		/* Update CashPaySchedule if they were already Paid */
		PAYSCHEDULE:
		FOR EACH CashPaySchedule 
			WHERE CashPaySchedule.SchoolKey          EQ rgmsms.StudentCashPayments.SchoolKey
			  AND CashPaySchedule.StudentRecKey      EQ rgmsms.StudentCashPayments.StudentRecKey
			  AND CashPaySchedule.EnrollmentKey      EQ rgmsms.StudentCashPayments.EnrollmentKey
			  AND CashPaySchedule.CashPayScheduleKey EQ rgmsms.StudentCashPayments.StudentPayScheduleKey
			  AND CashPaySchedule.AmountDue          GT 0  
			BY CashPaySchedule.DueDate:        

			IF deTotalPaidForSched GE CashPaySchedule.AmountPerPayment THEN
				ASSIGN
					CashPaySchedule.AmountPaid = CashPaySchedule.AmountPerPayment
					CashPaySchedule.AmountDue  = 0.
			ELSE 
				ASSIGN
					CashPaySchedule.AmountPaid = deTotalPaidForSched
					CashPaySchedule.AmountDue  = CashPaySchedule.AmountDue - deTotalPaidForSched.

			deTotalPaidForSched = deTotalPaidForSched - CashPaySchedule.AmountPaid.
		END.
	END.
	
	/* Get the Payment Plan */
	ASSIGN
		RemainingPayments 	= 0
		FirstDateToPay 		= ?
		AmtPerPay			= 0.
	FOR EACH CashPaySchedule 
		WHERE CashPaySchedule.SchoolKey          EQ rgmsms.StudentCashPayments.SchoolKey
		  AND CashPaySchedule.StudentRecKey      EQ rgmsms.StudentCashPayments.StudentRecKey
		  AND CashPaySchedule.EnrollmentKey      EQ rgmsms.StudentCashPayments.EnrollmentKey
		  AND CashPaySchedule.CashPayScheduleKey EQ rgmsms.StudentCashPayments.StudentPayScheduleKey
		  AND CashPaySchedule.AmountDue	         GT 0
		  BY CashPaySchedule.DueDate:       
		  
		ASSIGN
			RemainingPayments 	= RemainingPayments + 1
			AmtPerPay 			= CashPaySchedule.AmountPerPayment.
		IF FirstDateToPay EQ ? THEN
			FirstDateToPay = CashPaySchedule.DueDate.
	END.
END.








FUNCTION f_GETDAYSINMONTH RETURNS INTEGER(INPUT ipMONTH AS INTEGER,
                                          INPUT ipYEAR AS INTEGER):

  DEFINE VARIABLE iDAYINMONTH AS INTEGER NO-UNDO.

    iDAYINMONTH = 0.
    IF ipMONTH = 1 
        OR ipMONTH = 3 
        OR ipMONTH = 5 
        OR ipMONTH = 7 
        OR ipMONTH = 8 
        OR ipMONTH = 10 
        OR ipMONTH = 12 
    THEN 
        iDAYINMONTH = 31.
    ELSE 
        IF ipMONTH = 4 
            OR ipMONTH = 6 
            OR ipMONTH = 9 
            OR ipMONTH = 11 
        THEN 
            iDAYINMONTH = 30.
    ELSE 
    DO:
        IF ipYEAR MOD 4 GT 0 THEN 
            iDAYINMONTH = 28.
        ELSE 
            iDAYINMONTH = 29.
    END.

    RETURN 
        iDAYINMONTH.
END FUNCTION.