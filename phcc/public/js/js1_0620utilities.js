var bgcolor       = getCookie("bgcolor");
var fontcolor     = getCookie("fontcolor");
var bordercolor   = getCookie("bordercolor");
var divcolor      = getCookie("divcolor");
var titlebarcolor = getCookie("titlebarcolor");
var font          = getCookie("font");
var hlightcolor   = getCookie("hlightcolor");

function getCookie(c_name) {
   if (document.cookie.length>0) {
     c_start=document.cookie.indexOf(c_name + "=");
     if (c_start!=-1) {
      c_start=c_start + c_name.length+1;
      c_end=document.cookie.indexOf(";",c_start);
      if (c_end==-1) c_end=document.cookie.length;
      return unescape(document.cookie.substring(c_start,c_end));
     }
   }
   return "";
}

function setCookie(c_name,value,expiredays) {
   var exdate=new Date();
   exdate.setDate(exdate.getDate()+expiredays);
   document.cookie=c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}

function delCookie(c_name) {
  var cookie_date = new Date ();  // current date & time
  cookie_date.setTime ( cookie_date.getTime() - 1 );
  document.cookie = c_name += "=; expires=" + cookie_date.toGMTString();
}

function SelectAddItems(obj,objVALUE,objTEXT,j){
   var oOption=document.createElement("option");
   oOption.value=objVALUE;
   oOption.text=objTEXT;
   if (j<=0) {
      oOption.style.color="#f00";
      oOption.style.fontWeight="600";
   }
   try {obj.add(oOption,null);}
   catch(err) {obj.add(oOption);}
}
      /*function insertItem(val,obj) {
        var elemPos = CourseKey.indexOf(val);
        SelectAddItems(obj,val,CourseLbl[elemPos]);
      }*/

function setBHV(bhvType,attrValue,objectList) {
   var d = document.xForm;
   var oArr = objectList.split(",");
   for (h = 0; h < oArr.length; h++) {
      if (d[oArr[h]]!=null) {
         var obj = d[oArr[h]];
      }
      else if (document.getElementById(oArr[h])!=null) {
         var obj = document.getElementById(oArr[h]);
      }
      else window.status = "OBJECT NOT FOUND " + oArr[h];
      //d[oArr[h]].disabled = attrValue;
      if (obj!=null) {
         switch (bhvType) {
            case "check":
               obj.checked = attrValue;
            break;
            case "disable":
               obj.disabled = attrValue;
            break;
            case "readonly":
               obj.readOnly = attrValue;
            break;
            case "value":
               obj.value = attrValue;
            break;
            case "styleDisplay":
               obj.style.display = attrValue;
            break;
            case "innerHTML":
               obj.innerHTML = attrValue;
            break;
            default:
            alert("No assigned");
         }
      }
   }
}

function getvalue(obj) {
   var d = document.xForm;
   if (d[obj]!=null) {
      var objx = d[obj];
   } else if (document.getElementById(obj)!=null) {
      var objx = document.getElementById(obj);
   } else {
      alert("UNDEFINED OBJECT " + obj + "!!!");
      return false;
   }
   return objx.value;
}

function isAvail(obj) {
   if (document.xForm[obj]!=null) return true;
   else {
      if (document.getElementById(obj)!=null) return true;
      else return false;
   }
}

function getElement_ObjnValue() {
   var d = document.xForm;
   var objvalue='';
   var u='';
   var lgInclude = false;
   with (d) {
      // Get All Form Objects Value & Name
      for (var i=0; i < elements.length; i++) {
         var ObjName = elements[i].name;
         var ObjClassName = elements[i].className;

         lgInclude = false;
         if (
               (
                  (
                     ObjClassName.indexOf("form-input") >= 0 ||
                     ObjClassName.indexOf("flds") >= 0 ||
                     ObjClassName.indexOf("HiddenEntry") >= 0
                  )
               )
               && elements[i].type != "button"
               && elements[i].value != ""

            )
         {
            lgInclude = true;
         }
         if (elements[i].value == "" &&
               ObjClassName.indexOf("form-input") >= 0)
         {
            lgInclude = true;
         }

         // get object Name & object Value
         if (lgInclude)
         {
            var Elem_Value = elements[i].value;

            if (ObjClassName.indexOf("date--") >=0 && Elem_Value=="") {
               Elem_Value = '1900-01-01';
            }

            if (elements[i].type == "text" ||
                elements[i].type == "password" ||
                elements[i].type == "select-one")
            {
               if (elements[i].name != "char_Password1" &&
                   elements[i].name != "char_Password2")
               {
                  objvalue += elements[i].type + "|" +
                              elements[i].name + "|" +
                              Elem_Value + '!';
               }
            }
            else if (elements[i].type == "radio")
            {
               objvalue += elements[i].type + "|" +
                           elements[i].name + "|" +
                           elements[i].checked + '!';
            }
            else if (elements[i].type == "checkbox") {
               if (elements[i].checked) var val = "1";
                                   else var val = "0";
               objvalue += elements[i].type + "|" +
                           elements[i].name + "|" +
                           val + '!';
            }
            else if (elements[i].type == "hidden" && ObjClassName.indexOf("HiddenEntry") >= 0)
            {
               objvalue += elements[i].type + "|" +
                           elements[i].name + "|" +
                           Elem_Value + '!';
            }
            else if (elements[i].type == "textarea" && Elem_Value != "")
            {
               objvalue += elements[i].type + "|" +
                           elements[i].name + "|" +
                           Elem_Value + '!';
            }
         }
      }
   }
   return objvalue;
}

function parseURLhParam() {
   var d = document.xForm;
   var parseURL = "";
   with (d) {
      for (var i=0; i < elements.length; i++) {
         if (elements[i].name != "" && elements[i].type == "hidden" && elements[i].value != "") {
            if (elements[i].className == "HiddenParam") {
               parseURL += "&" + elements[i].name + "=" + escape(elements[i].value);
            }
         }
      }
   }
   return parseURL;
}

function setObjToAddEdit(val) {
   var d = document.xForm;
   var obj = d.hObjEntry;
   var attr = obj.value;
   var objArr = attr.split("|");
   var Err_ObjName = "";
   for (j=0;j<objArr.length;j++) {

      var eachLIST = objArr[j];
      var eachATTR = eachLIST.split(",");
      var eachOBJ = eachATTR[0];
      if  (eachOBJ!="") {
         if (document.xForm[eachOBJ] != null) {
            if (document.xForm[eachOBJ].type == "text") {
               if (val == "ADD") {
                  document.xForm[eachOBJ].value = eachATTR[2]; //default value
               }
               document.xForm[eachOBJ].readOnly = eval(eachATTR[1]); // true or false;
            }
            else if (document.xForm[eachOBJ].type == "select-one") {
               if (val == "ADD") {
                  document.xForm[eachOBJ].value = eachATTR[2]; //default value
               }
               document.xForm[eachOBJ].disabled = eval(eachATTR[1]); // true or false;
            }
            else if (document.xForm[eachOBJ].type == "button") {
               document.xForm[eachOBJ].disabled = eval(eachATTR[1]); // true or false;
            }
         }
         else {
            Err_ObjName += eachOBJ+"\n";
         }
      }
   }
   /*
   if (Err_ObjName != "") {
      alert("oops WARNING!!! Some object did not found in this Form\n" + Err_ObjName);
   }*/
}

function SetCursorToEnd(obj) {
   if (obj.createTextRange) {
      //IE
      var FieldRange = obj.createTextRange();
      FieldRange.moveStart('character', obj.value.length);
      FieldRange.collapse();
      FieldRange.select();
   }
   else {
      //Firefox and Opera
      obj.focus();
      var length = obj.value.length;
      obj.setSelectionRange(length, length);
   }
}

function setObjToSaveCancel() {
   var d = document.xForm;
   var obj = d.hObjEntry;
   var attr = obj.value;
   var objArr = attr.split("|");

   for (j=0;j<objArr.length;j++) {
      var eachLIST = objArr[j];
      var eachATTR = eachLIST.split(",");
      var eachOBJ = eachATTR[0];
      if (d[eachOBJ] != null) {
         if (d[eachOBJ].type == "text") {
            d[eachOBJ].readOnly = true; // true or false;
         }
      }
   }
}

function setGlobalURL() {
   var url = "";
   /*url += "hSess=" + $("#hSess").val();
   url += "&hUser=" + $("#hUser").val();
   url += "&hUserLvl=" + $("#hUserLvl").val();
   url += "&hCompCode=" + $("#hCompCode").val();
   url += "&hCompanyID=" + $("#hCompanyID").val();
   url += "&hBranchID=" + $("#hBranchID").val();
   url += "&hUserRefID=" + $("#hUserRefID").val();
   url += "&hmemof=" + $("#hmemof").val();
   url += "&hRefId=" + $("#hRefId").val();
   url += "&hucode=" + $("#hucode").val();
   url += "&hEmpRefId=" + $("#hEmpRefId").val();
   */



   return url;
}
function setAddURL() {
   var addParam = "";
   addParam += "&paramTitle=" + $("#hScrnTitle").val();
   addParam += "&hTable=" + $("#hTable").val();
   addParam += "&hGridTblHdr=" + $("#hHdr").val();
   addParam += "&hGridTblFld=" + $("#hFld").val();
   addParam += "&id=" + getMenuClick();
   return addParam;
}

/**
 * isValidDate(str)
 * @param string str value yyyy-mm-dd
 * @return boolean true or false
 * IF date is valid return true
 */
function isValidDate(str){
   // STRING FORMAT yyyy-mm-dd
   if(str=="" || str==null){$.notify("Invalid Date!!!","warn"); return false;}                        
   
   // m[1] is year 'YYYY' * m[2] is month 'MM' * m[3] is day 'DD'             
   var m = str.match(/(\d{4})-(\d{2})-(\d{2})/);
   
   // STR IS NOT FIT m IS NOT OBJECT
   if( m === null || typeof m !== 'object') {$.notify("Invalid Date!!!","warn"); return false;}           
   
   // CHECK m TYPE
   if (typeof m !== 'object' && m !== null && m.size!==3){$.notify("Invalid Date!!!","warn"); return false;}
            
   var ret = true; //RETURN VALUE                  
   var thisYear = new Date().getFullYear(); //YEAR NOW
   var minYear = 1901; //MIN YEAR
   
   // YEAR CHECK
   if( (m[1].length < 4) || m[1] < minYear || m[1] > thisYear) {$.notify("Invalid Year!!!","warn"); ret = false;}
   // MONTH CHECK       
   if( (m[2].length < 2) || m[2] < 1 || m[2] > 12){$.notify("Invalid Month!!!","warn"); ret = false;}
   // DAY CHECK
   if( (m[3].length < 2) || m[3] < 1 || m[3] > 31){$.notify("Invalid Day!!!","warn"); ret = false;}
   
   return ret;       
}

function showBtnUpd() {
  if ($(".enabler--:checked").length > 0) {
    $("#btnUPDATE").show();
  } else {
    $("#btnUPDATE").hide();
  }
}

