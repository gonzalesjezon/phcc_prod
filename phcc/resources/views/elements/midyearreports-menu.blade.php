
<?php

(strtolower($module) == 'midyeartransmital') ? $midyeartransmital = 'active' : $midyeartransmital = '';
(strtolower($module) == 'midyeargeneralpayroll') ? $midyeargeneralpayroll = 'active' : $midyeargeneralpayroll = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $midyeargeneralpayroll }}">
            <a href="{{ url('payrolls/reports/midyearreports/midyeargeneralpayroll') }}"  >
                <span>GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $midyeartransmital }}">
            <a href="{{ url('payrolls/reports/midyearreports/midyeartransmital') }}"  >
                <span>TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




