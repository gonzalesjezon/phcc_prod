
<?php

(strtolower($module) == 'communicationexpense') ? $communicationexpense = 'active' : $communicationexpense = '';
(strtolower($module) == 'eme') ? $eme = 'active' : $eme = '';
(strtolower($module) == 'cashmidyearbonus') ? $cashmidyearbonus = 'active' : $cashmidyearbonus = '';
(strtolower($module) == 'rata') ? $rata = 'active' : $rata = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $rata }}">
            <a href="{{ url('payrolls/reports/othercompensations/rata') }}"  >
            RATA</a>
        </li>
        <li class="{{ $communicationexpense }}">
            <a href="{{ url('payrolls/reports/othercompensations/communicationexpense') }}"  >
            COMMUNICATION EXPENSE ALLOTMENT</a>
        </li>
        <li class="{{ $eme }}">
            <a href="{{ url('payrolls/reports/othercompensations/eme') }}"  >
            EXTRAORDINARY AND MISCELLANEOUS EXPENSES</a>
        </li>
        <!-- <li class="{{ $cashmidyearbonus }}">
            <a href="{{ url('payrolls/reports/othercompensations/cashmidyearbonus') }}"  >
            CASH GIFT AND MID YEAR BONUS</a>
        </li> -->
        <!-- <li class="nav-divider"></li> -->

    </ul>
</nav>




