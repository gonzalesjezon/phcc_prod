
<?php

(strtolower($module) == 'cnapeitransmital') ? $cnapeitransmital = 'active' : $cnapeitransmital = '';
(strtolower($module) == 'cnapeigeneralpayroll') ? $cnapeigeneralpayroll = 'active' : $cnapeigeneralpayroll = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $cnapeigeneralpayroll }}">
            <a href="{{ url('payrolls/reports/cnapeireports/cnapeigeneralpayroll') }}"  >
                <span>GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $cnapeitransmital }}">
            <a href="{{ url('payrolls/reports/cnapeireports/cnapeitransmital') }}"  >
                <span>TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




