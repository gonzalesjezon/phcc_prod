
	<div class="panel" style="padding: 10px;margin-top: 15px;">
		<label style="font-weight: 600;font-size: 15px;padding-left: 5px;position: absolute;top: 10px;" id="d-name"></label>
		<table class="table table-striped table-responsive datatable" id="tbl_pbb" style="border: none;">
			<thead>
				<tr >
					<th>Position</th>
					<th>Office</th>
					<th>PBB Amount</th>
					<th>Tax Amount</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody class="text-right">
			</tbody>
		</table>
	</div>

<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_pbb').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_pbb tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );


})
</script>
