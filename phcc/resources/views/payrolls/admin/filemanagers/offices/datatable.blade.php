<br><br>
<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_offices">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Sequence No</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $value)
			<tr data-id="{{ $value->id }}" data-name="{{ $value->name }}" data-sequence_no="{{ $value->sequence_no }}" data-code="{{ $value->code }}" data-btnnew="newOffices" data-btnedit="editOffices" data-btnsave="saveOffices" data-btncancel="cancelOffices">
				<td>{{ $value->code }}</td>
				<td>{{ $value->name }}</td>
				<td>{{ $value->sequence_no }}</td>
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_offices').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_offices tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	       	office_id 	= $(this).data('id');
			code 			= $(this).data('code');
			name 			= $(this).data('name');
			sequence_no 	= $(this).data('sequence_no');

			$('#office_id').val(office_id);
			$('#code').val(code);
			$('#name').val(name);
			$('#sequence_no').val(sequence_no);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});


})
</script>
