<div class="col-md-12">
	<table class="table datatable" id="taxpolicy">
		<thead>
			<tr>
				<th>Policy Name</th>
				<th>Pay Period</th>
				<th>Deduction Period</th>
				<th>Type</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->policy_name }}</td>
					<td>{{ $value->pay_period }}</td>
					<td>{{ $value->deduction_period }}</td>
					<td>{{ $value->policy_type }}</td>
					<td><a class="btn btn-xs btn-danger delete_item" data-function_name="delete" data-id="{{ $value->id }}"><i class="fa fa-trash"></i> Delete</a></td>
				</tr>
				@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#taxpolicy').DataTable({
		'dom':'<lf<t>pi>',
		"paging": false,
	 	// "scrollY":"250px",
        // "scrollCollapse": true,
	});

	$('#taxpolicy tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

	$('#taxpolicy tr').on('click',function(){
		var id = 0;
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getTaxpolicy',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#policy_name').val(data.policy_name);
				$('#pay_period').val(data.pay_period);
				$('#deduction_period').val(data.deduction_period);
				$('#policy_type').val(data.policy_type);
				$('#based_on').val(data.based_on);
				$('#computation').val(data.computation);
				$('#job_grade_rate').val(data.job_grade_rate);

				$('#chk_monthly').attr('checked',false);
				$('#chk_annual').attr('checked',false);
				if(data.is_withholding == 'Monthly'){
					$('#chk_monthly').attr('checked',true);
				}else{
					$('#chk_annual').attr('checked',true);
				}
				$('#update_taxpolicy').val(data.id);
			}
		})
	});

	// ======================================================= //
   // ============ DELETE  FUNCTION ==================== //
  // ===================================================== //

$(document).on('click','.delete_item',function(){
	id 	= $(this).data('id');
	function_name = $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						}).then(function(isConfirm){
							if(isConfirm.value == true){
								window.location.href = base_url+module_prefix+module;
							}else{
								return false;
							}
						})

					}

				})
			}else{
				return false;
			}
		});
	}
})

})
</script>
