@extends('app-filemanager')

@section('filemanager-content')
<div id="resp_center" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>{{ $title }}</label>
		</div>

	</div>

	<div class="sub-panel">
		{!! $controller->show() !!}
	</div>

	<div class="formcontent">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="for_update" id="for_update">
			<div class="col-md-12">
				<div class="box-1 button-style-wrapper">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savercenter"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
					<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>
			<div class="benefits-content style-box2"">
				<div class="col-md-12">
					<div class="col-md-5">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Code</label></td>
								<td>
									@if ($errors->has('code'))
									    <span class="text-danger">{{ $errors->first('code') }}</span>
									@endif
									<input type="text" name="code" id="code" class="form-control font-style2">
								</td>
							</tr>
							<tr>
								@if ($errors->has('name'))
									    <span class="text-danger">{{ $errors->first('name') }}</span>
									@endif
								<td><label>Name</label></td>
								<td>
									<input type="text" name="name" id="name" class="form-control font-style2">
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-7">
						<div class="box-1">

						</div>
					</div>
				</div>
			</div>

		</form>

	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});

		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('.error-msg').remove();
			$('#for_update').val('');
		});

	// ======================================================= //
  // ============ DELETE  FUNCTION ==================== //
// ===================================================== //

$(document).on('click','.delete_item',function(){
	id 			= $(this).data('id');
	function_name = $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						}).then(function(isConfirm){
							window.location.href = base_url+module_prefix+module;
						})
					}

				})
			}else{
				return false;
			}
		});
	}
})
	})
</script>
@endsection
