@extends('app')

@section('sidemenu')
@endsection

@section('content')
<div class="container body">
    <div class="main_container">
        @include('elements/nav')
        @include('elements/sidemenu')
        <!-- page content -->
        <div class="right_col" role="main">

            <div class="row">
                <div class="col-md-10 col-sm-12 col-xs-12">
                    <div class="page-title">
                        <h3>{{ $title }}</h3>
                    </div>
                    <div class='x_panel'>
                        <div class='x_title'>
                            <div class="row">
                                <a href="{{ url($module) }}" class="btn btn-app"><i class="fa fa-long-arrow-left"></i> Return</a>
                                <a class="btn btn-app submit"><i class="glyphicon glyphicon-floppy-disk"></i> Save </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class='x_content'>
                            <form method="POST" action="{{ url('/').'/'.$module.'/update'}}" onsubmit="return false" id="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{ Crypt::encrypt($result->id) }}">
                                <div class="col-xs-12 col-md-6">
                                    <div class="inner-panel">
                                        <div class="ip-title">Permission Properties</div>
                                        <div class="ip-body">
                                            <div class="form-group">
                                                <label>Access Name</label>
                                                <input type="text" name="name" value="{{ $result->name }}" class="form-control">
                                            </div>
                                            <table class="table">
                                                <thead>
                                                    <th style="width:50%">Modules</th>
                                                    <th>Create</th>
                                                    <th>Read</th>
                                                    <th>Update</th>
                                                    <th>Delete</th>
                                                </thead>

                                                <tbody>
                                                <?php
                                                // dd($result);
                                                $access_rights = [];
                                                    foreach ($result->accessRights as $key => $value) {
                                                        $access_rights[$value->access_module_id] = $value;
                                                    }
                                                    // dd($access_rights);

                                                ?>
                                                <?php foreach ($access_modules as $key => $value): ?>
                                                    <?php
                                                        if(isset($access_rights[$value->id])){
                                                            ($access_rights[$value->id]->to_add == 1) ? $create ='checked' : $create = '';
                                                            ($access_rights[$value->id]->to_view == 1) ? $read ='checked' : $read = '';
                                                            ($access_rights[$value->id]->to_edit == 1) ? $update ='checked' : $update = '';
                                                            ($access_rights[$value->id]->to_delete == 1) ? $delete ='checked' : $delete = '';
                                                        }else{

                                                            $create ='';
                                                            $read ='';
                                                            $update ='';
                                                            $delete ='';

                                                        }
                                                        $crud = array_flip(explode('/',$value->crud));
                                                    ?>

                                                    <tr>
                                                        <td>{{ $value->description }}</td>
                                                        <td>
                                                            <?php if (isset($crud['c'])): ?>
                                                                <input type="checkbox" name="crud[{{$value->id}}][]" value="1" class="flat" {{ $create }}>
                                                            <?php endif ?>
                                                        </td>

                                                        <td>
                                                            <?php if (isset($crud['r'])): ?>
                                                                <input type="checkbox" name="crud[{{$value->id}}][]" value="2" class="flat " {{ $read }}>
                                                            <?php endif ?>
                                                        </td>

                                                        <td>
                                                            <?php if (isset($crud['u'])): ?>
                                                                <input type="checkbox" name="crud[{{$value->id}}][]" value="3" class="flat " {{ $update }}>
                                                            <?php endif ?>
                                                        </td>
                                                        <td>
                                                            <?php if (isset($crud['d'])): ?>
                                                                <input type="checkbox" name="crud[{{$value->id}}][]" value="4" class="flat " {{ $delete }}>
                                                            <?php endif ?>
                                                        </td>

                                                    </tr>
                                                <?php endforeach ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
            <br />
        </div>
        <!-- /page content -->

        @include('elements/footer')
    </div>
</div>
@endsection




