@extends('app-front')

@section('content')
<div class="row" style="margin-top: 80px;">
	<div class="col-md-5">
		<div class="form-group">
		    <a href="{{ url($module_prefix.'/'.$module.'/create') }}" class="btn btn-info" style="float:left"> <i class="fa fa-plus"></i> New</a>
		    <div class="input-group search-wrapper">
		        <span class="input-group-btn">
		            <button class="btn btn-app  search-btn" type="button"><i class="fa fa-search"></i> Search</button>
		        </span>
		        <input type="text" style="" class="form-control search" placeholder="Enter keyword">
		    </div>
		</div>
	</div>
</div>
<div class="row" style="margin-top: 50px;">
	<div class="col-md-12">
	    <div class="sub-panel">
	        {!! $controller->show() !!}
	    </div>
	</div>
</div>



@endsection

@section('js-logic1')
<!-- <script src="{{ asset('js/payroll-vue/benefits.js') }} "></script> -->
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);

		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});


		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('#for_update').val('');
			$('.error-msg').remove();
		});

		$('#tbl_benefits tr').on('click',function(){

			id = $(this).data('id');
			name = $(this).data('name');
			username = $(this).data('username');
			password = $(this).data('password');

			$('#id').val(id)
			$('#username').val(username)
			$('#name').val(name)
		});

	    $('.onlyNumber').keypress(function (event) {
	    	return isNumber(event, this)
		});

		$(".onlyNumber").keyup(function(){
			amount  = $(this).val();
			if(amount == 0){
				$(this).val('');
			}else{
				plainAmount = amount.replace(/\,/g,'')
				$(this).val(commaSeparateNumber(plainAmount));
			}
		})

	// ======================================================= //
  // ============ DELETE  FUNCTION ==================== //
// ===================================================== //

$(document).on('click','.delete_item',function(){
	id 			= $(this).data('id');
	function_name = $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						}).then(function(isConfirm){
							window.location.href = base_url+module_prefix+module;
						})
					}

				})
			}else{
				return false;
			}
		});
	}
})

	});

</script>
@endsection

