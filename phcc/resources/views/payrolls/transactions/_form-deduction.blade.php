<div class="button-wrapper" style="margin-left: 10px;">
	<a class="btn btn-xs btn-info btn-savebg btn_new" id="newDeductionInfo" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"><i class="fa fa-save"></i> New</a>

	<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editDeductionInfo" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"><i class="fa fa-save"></i> Edit</a>

	<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="formDeduction" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo" id="saveDeductionInfo"><i class="fa fa-save"></i> Save</a>
	<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-form="myform" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"id="cancelDeductionInfo"> Cancel</a>
</div>
<div class="border-style2">
	<form method="post" action="{{ url($module_prefix.'/'.$module.'/storeDeductionInfoTransaction')}}" id="formDeduction" onsubmit="return false" class="myformDeduction">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="employee_id" class="employee_id">
		<input type="hidden" name="year" class="year">
		<input type="hidden" name="month" class="month">
		<div class="col-md-3">
			<div class="form-group newDeductionInfo">
				<label>Deduction</label>
				<select class="form-control" id="deduction_id" name="deduction_id">
					<option value=""></option>
					@foreach($deductions as $key => $value)
					<option value="{{ $value->id }}">{{ $value->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group newDeductionInfo">
				<label>Amount</label>
				<input type="text" name="deduction_amount" id="deduction_amount" class="form-control onlyNumber">
			</div>
		</div>
	</form>
</div>