<div class="col-md-12">
		<div class="border-style2">
			<table class="table borderless">
				<tr class="text-center">
					<td colspan="2"><label style="margin-left: 80px;">Employee Share</label></td>
					<td colspan="1"><label style="margin-right: 32px;">Employer Share</label></td>
					<td colspan="3"><label style="margin-right: 24px;">ECC</label></td>
				<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr>
					<td>GSIS</td>
					<td>
						<input type="text" name="gsis_ee_share" id="gsis_ee_share" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmpShareGsis"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmpShareGsis"></a>
						</div>
					</td> -->
					<td>
						<input type="text" name="gsis_er_share" id="gsis_er_share" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmprShareGsis"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmprShareGsis"></a>
						</div>
					</td> -->
					<td>
						<input type="text" name="ecc_amount" id="ecc_amount" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEccGsis"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEccGsis"></a>
						</div>
					</td> -->
					<td colspan="2"></td>
				</tr>
				<tr>
					<td>Philhealth</td>
					<td>
						<input type="text" name="philhealth_ee_share" id="philhealth_ee_share" class="form-control font-style2 onlyNumber" readonly>
					</td>
				<!-- 	<td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmpSharePhhealth"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmpSharePhhealth"></a>
						</div>
					</td> -->
					<td>
						<input type="text" name="philhealth_er_share" id="philhealth_er_share" class="form-control font-style2 onlyNumber" readonly>
					</td>
				<!-- 	<td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmprSharePhhealth"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmprSharePhhealth"></a>
						</div>
					</td> -->
					<td colspan="3"></td>
				</tr>
				<tr>
					<tr>
					<td>Pag-ibig</td>
					<td>
						<input type="text" name="pagibig_ee_share" id="pagibig_ee_share" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmpSharePagibig"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmpSharePagibig"></a>
						</div>
					</td> -->
					<td>
						<input type="text" name="pagibig_er_share" id="pagibig_er_share" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmprSharePagibig"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmprSharePagibig"></a>
						</div>
					</td> -->
					<td colspan="3"></td>
				</tr>
				<tr>
					<td>Withholding Tax</td>
					<td>
						<input type="text" name="witholding_tax" id="witholding_tax" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmpShareWtax"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmpShareWtax"></a>
						</div>
					</td> -->
					<td colspan="4"></td>
				</tr>
				<tr>
					<td>Add Withholding Tax</td>
					<td>
						<input type="text" name="additional_tax" id="additional_tax" class="form-control font-style2 ">
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmpShareWtax"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmpShareWtax"></a>
						</div>
					</td> -->
					<td colspan="4"></td>
				</tr>
				<tr>
					<td>Provident Fund</td>
					<td>
						<input type="text" name="input_contribEmpSharePfund" id="input_contribEmpSharePfund" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmpSharePfund"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmpSharePfund"></a>
						</div>
					</td> -->
					<td>
						<input type="text" name="input_contribEmprSharePfund" id="input_contribEmprSharePfund" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmprSharePfund"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmprSharePfund"></a>
						</div>
					</td> -->
					<td colspan="3"></td>
				</tr>
				<tr>
					<td>Association Dues</td>
					<td>
						<input type="text" name="input_contribEmpShareAdues" id="input_contribEmpShareAdues" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmpShareAdues"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmpShareAdues"></a>
						</div>
					</td> -->
					<td colspan="4"></td>
				</tr>
				<tr>
					<td>Pag-ibig Fund II</td>
					<td>
						<input type="text" name="pagibig2" id="pagibig2" class="form-control font-style2 onlyNumber" readonly>
					</td>
					<!-- <td>
						<div style="margin-top: 5px;">
							<a href="#" class="fa fa-plus" id="add_contribEmpSharePfund2"></a>
							<a href="#" class="fa fa-minus" id="diff_contribEmpSharePfund2"></a>
						</div>
					</td> -->
					<td colspan="4"></td>
				</tr>
				<tr>
					<td></td>
					<td class="text-center">Add</td>
					<td class="text-center">Less</td>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td>Adjustment</td>
					<td>
						<input type="text" name="additional_adj" id="additional_adj" class="form-control font-style2 isNumber" >
					</td>
					<td>
						<input type="text" name="deduction_adj" id="deduction_adj" class="form-control font-style2 isNumber">
					</td>
					<td>
						<select class="form-control font-style2" id="pay_period">
							<option value="0">Select period</option>
							<option value="firsthalf">1st Half</option>
							<option value="secondhalf">2nd Half</option>
						</select>
					</td>
					<td colspan="2"></td>
				</tr>

				<tr>
					<td>Remarks</td>
					<td>
						<input type="text" name="additional_remarks" id="additional_remarks" class="form-control font-style2" placeholder="Additional Remarks">
					</td>
					<td>
						<input type="text" name="less_remarks" id="less_remarks" class="form-control font-style2" placeholder="Less Remarks">
					</td>
					<td colspan="3"></td>
				</tr>
			</table>
		</div>
		<br>
	</div>