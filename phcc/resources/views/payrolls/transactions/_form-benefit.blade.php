<div class="button-wrapper" style="margin-left: 10px;">
	<a class="btn btn-xs btn-info btn-savebg btn_new" id="newBenefitInfo" data-btnnew="newBenefitInfo" data-btncancel="cancelBenefitInfo" data-btnedit="editBenefitInfo" data-btnsave="saveBenefitInfo"><i class="fa fa-save"></i> New</a>

	<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editBenefitInfo" data-btnnew="newBenefitInfo" data-btncancel="cancelBenefitInfo" data-btnedit="editBenefitInfo" data-btnsave="saveBenefitInfo"><i class="fa fa-save"></i> Edit</a>

	<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newBenefitInfo" data-btncancel="cancelBenefitInfo" data-btnedit="editBenefitInfo" data-btnsave="saveBenefitInfo" id="saveBenefitInfo"><i class="fa fa-save"></i> Save</a>
	<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newBenefitInfo" data-btncancel="cancelBenefitInfo" data-form="myform" data-btnedit="editBenefitInfo" data-btnsave="saveBenefitInfo"id="cancelBenefitInfo"> Cancel</a>
</div>
<div class="border-style2">
	<form method="post" action="{{ url($module_prefix.'/'.$module.'/storeBenefitInfoTransaction')}}" id="form" onsubmit="return false" class="myform">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="employee_id" class="employee_id">
		<input type="hidden" name="year" class="year">
		<input type="hidden" name="month" class="month">
		<div class="col-md-3">
			<div class="form-group newBenefitInfo">
				<label>Allowance</label>
				<select class="form-control" id="benefit_id" name="benefit_id">
					<option value=""></option>
					@foreach($benefit as $key => $value)
					<option value="{{ $value->id }}">{{ $value->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group newBenefitInfo">
				<label>Amount</label>
				<input type="text" name="bt_amount" id="bt_amount" class="form-control onlyNumber">
			</div>
		</div>
	</form>
</div>