<div class="col-md-12">
	<div class="border-style2">
		<input type="hidden" name="attendance_id" id="attendance_id">
		<input type="hidden" name="employee_id" id="employee_id">
		<table class="table borderless">
			<tr>
				<td><label>Salary Rate</label></td>
				<td class="text-left"><label id="_salaryrate"></label></td>
				<td></td>
				<td></td>

			</tr>
			<tr>
				<td></td>
				<td class="text-center"><span>Actual</span></td>
				<td class="text-center"><span>Adjustment</span></td>
				<td class="text-center"><span>Total</span></td>
			</tr>
			<tr>
				<td>Work Days</td>
				<td>
					<input type="text" name="actual_workdays" id="input_actualworkdays" class="form-control font-style2 input attendance workdays" maxlength="2" readonly>
				</td>
				<td class="newAttendance">
					<input type="text" name="adjust_workdays" id="input_adjustworkdays" class="form-control font-style2 input attendance  workdays isNumber" maxlength="2">
				</td>
			<!-- 	<td>
					<div style="margin-top: 5px;">
						<a href="#" class="fa fa-plus" id="add_adjworkdays"></a>
						<a href="#" class="fa fa-minus" id="diff_adjworkdays"></a>
					</div>
				</td> -->
				<td>
					<input type="text" name="total_workdays" id="input_totalworkdays" class="form-control font-style2 input attendance" placeholder="0.00" readonly>
				</td>
			</tr>
			<tr>
				<td>LWOP</td>
				<td class="newAttendance">
					<input type="text" name="actual_absences" id="input_actualabsence" class="form-control font-style2 input attendance absences isNumber" maxlength="5">
				</td>
				<td class="newAttendance">
					<input type="text" name="adjust_absences" id="input_adjustabsence" class="form-control font-style2 input attendance absences isNumber" maxlength="2">
				</td>
			<!-- 	<td>
					<div style="margin-top: 5px;">
						<a href="#" class="fa fa-plus" id="add_adjabsence"></a>
						<a href="#" class="fa fa-minus" id="diff_adjabsence"></a>
					</div>
				</td> -->
				<td>
					<input type="text" name="total_absences" id="input_totalabsence" class="form-control font-style2 input attendance" placeholder="0.00" readonly>
				</td>
			</tr>
			<tr>
				<td>Tardiness</td>
				<td class="newAttendance">
					<input type="text" name="actual_tardines" id="input_actualwtardiness" class="form-control font-style2 attendance input computetardines isNumber">
				</td>
				<td class="newAttendance">
					<input type="text" name="adjust_tardines" id="input_adjusttardiness" class="form-control font-style2 attendance input computetardines isNumber">
				</td>
			<!-- 	<td>
					<div style="margin-top: 5px;">
						<a href="#" class="fa fa-plus" id="add_adjtardiness"></a>
						<a href="#" class="fa fa-minus" id="diff_adjtardiness"></a>
					</div>
				</td> -->
				<td>
					<input type="text" name="total_tardines" id="input_totaltardiness" class="form-control font-style2 input attendance " readonly placeholder="0.00">
				</td>
			</tr>
			<tr>
				<td>Undertime</td>
				<td class="newAttendance">
					<input type="text" name="actual_attendance_undertime" id="actual_attendance_undertime" class="form-control attendance font-style2 input  computeundertime isNumber">
				</td>
				<td class="newAttendance">
					<input type="text" name="adjust_attendance_undertime" id="adjust_attendance_undertime" class="form-control attendance font-style2 input computeundertime isNumber">
				</td>
				<!-- <td>
					<div style="margin-top: 5px;">
						<a href="#" class="fa fa-plus" id="add_adjundertime"></a>
						<a href="#" class="fa fa-minus" id="diff_adjundertime"></a>
					</div>
				</td> -->
				<td>
					<input type="text" name="total_undertime" id="input_totalundertime" class="form-control font-style2 input attendance " readonly placeholder="0.00">
				</td>
			</tr>
			<tr>
				<td colspan="3" class="text-right">
					<label>Basic Net Pay</label>
				</td>
				<td><label id="totalamount">0.00</label></td>

			</tr>
		</table>
	</div>
	<br>
</div>