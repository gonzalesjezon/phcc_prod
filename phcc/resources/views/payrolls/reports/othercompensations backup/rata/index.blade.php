@extends('app-othercompensations')

@section('othercompensations-content')

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 1180px;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
	       				<h5><b>{{ $title }}</b></h5>
	       				<h6><b>Covered Period:</b> <span id="month_year"></span></h6>

	       			</div>
					<table class="table" style="width: 1473px;">
					 <thead class="text-center" style="font-weight: bold;">
						 <tr>
						 	<td rowspan="2">#</td>
						 	<td rowspan="2">EMPLOYEE NAME</td>
						 	<td rowspan="2">DESIGNATION</td>
						 	<td rowspan="2"  >REPRESENTATION</td>
						 	<td rowspan="2"  >TRANSPORTATION</td>
						 	<td rowspan="2"  >LEAVE FILED</td>
						 	<td rowspan="2"  >NO. OF WORKDAYS OF ACTUAL WORK PERFORMANCE THE PREVIOUS MONTH</td>
						 	<td rowspan="2"  >PERCENTAGE OF ACTUAL RATA</td>
						 	<td colspan="3" >NET</td>
						 </tr>
						 <tr>
						 	<td>REPRESENTATION</td>
						 	<td>TRANSPORTATION</td>
						 	<td>TOTAL</td>
						 </tr>
					 </thead>
					 <tfoot >
					 	<tr style="border:1px solid #c0c0c0;font-weight: bold">
					 		<td style="border:none;"></td>
					 		<td colspan="1" style="border:none;" class="text-left"><b>Grand Total</b></td>
					 		<td style="border:none;"></td>
					 		<td style="border:none;" class="text-center"><span class="net_representation_amount" ></span></td>
					 		<td style="border:none;" class="text-center"><span class="net_transportation_amount" ></span></td>
					 		<td style="border:none;"></td>
					 		<td style="border:none;"></td>
					 		<td style="border:none;"></td>
					 		<td style="border:none;" class="text-center"><span class="net_representation_amount" ></span></td>
					 		<td style="border:none;" class="text-center"><span class="net_transportation_amount" ></span></td>
					 		<td style="border:none;" class="text-center"><span id="net_rata" ></span></td>
					 	</tr>
					 	<tr >
					 		<td style="border:none;" colspan="6" >
					 			<br>
					 			<b><i>CERTIFICATION:  This is to certify that the above-mentioned officials/personnel are entitled to the grant of RATA	per NBC No. 546 dated January 17, 2013 re: Rules and Regulations on the grant of RATA. </i></b>
					 		</td>
					 		<td  style="border:none;text-align: left;" colspan="6">
					 			<span>*RATA Schedule follows:</span>
					 			<table class="table" style="font-size: 10px;">

					 				<thead class="text-center">
					 					<tr>
					 						<td><b>Number of Workdays of Actual Work Performance in a Month</b></td>
					 						<td><b>Actual RATA for a Month</b></td>
					 					</tr>
					 				</thead>
					 				<tbody class="text-center">
					 					<tr>
					 						<td>1 to 5</td>
					 						<td>25% of the monthly RATA</td>
					 					</tr>
					 					<tr>
					 						<td>6 to 11</td>
					 						<td>50% of the monthly RATA</td>
					 					</tr>
					 					<tr>
					 						<td>12 to 16</td>
					 						<td>75% of the monthly RATA</td>
					 					</tr>
					 					<tr>
					 						<td>17 and more</td>
					 						<td>100% of the monthly RATA</td>
					 					</tr>
					 				</tbody>
					 			</table>
					 		</td>
					 	</tr>
					 	<tr class="text-left">
					 			<td style="border:none;" colspan="3">
							 		Certified Correct
							 		<br><br>
							 		<b>WYNONA R. ALMIN</b>
							 		<br>
							 		<span>HRMO II, HRDD</span>
						 		</td>
						 		<td style="border:none;text-align: center;" colspan="2">
						 			<br><br>
							 		<b>ANTONIO LYNNEL L. BAUTISTA</b>
							 		<br>
							 		<span>Chief Admin Officer, HRDD</span>
						 		</td>
						 		<td  style="border:none;" colspan="2" class="text-center">
							 		Approved for Payment
							 		<br>
							 		<br>
							 		<b>KENNETH V. TANATE</b>
							 		<br>
							 		<span>Executive Director</span>
						 		</td>
						 		<td style="border:none;" colspan="2">
						 			<span><b>Certified:</b> Supporting documents complete and </span>
							 		<span>proper, and cash available in the amount of</span><br>
							 		<span>Php _________________________.</span><br><br><br>
							 		<span><b>CAROLYN V. AQUINO</b></span><br>
							 		<span>Accountant IV, FPMO</span>
						 		</td>
						 		<td style="border:none;" colspan="3">
						 			<span><b>Certified:</b> Each employee whose name appears on the payroll has </span>
							 		<span>been paid the amount as indicated opposite his/her name.</span><br><br><br><br>
							 		<span><b>MAC VINCENT E. JAVIER</b></span><br>
							 		<span>Cashier II, ALO</span>
						 		</td>
					 	</tr>
				<!-- 	 	<tr class="text-left">
					 		<td style="border:none;"></td>
					 		<td  style="border:none;" colspan="4">
					 			<br><br>
					 			<b>Certified:</b>Supporting documents complete and<br>
					 			proper, and cash available in the amount of<br>
					 			Php _________________________.
					 			<br>
					 			<br><br><br>
					 			<b>CAROLYN V. AQUINO</b><br>
					 			Accountant III, FPMO
					 		</td>
					 	</tr> -->
					 </tfoot>
					 <tbody id="tbl_body">
					 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);

					if(data.length !== 0){
						arr = [];
						ctr = 0;
						net_transportation_amount = 0;
						net_representation_amount = 0;
						net_rata = 0;

							$.each(data.report,function(key,val){
								sub_total_representation_amount = 0;
								sub_total_transportation_amount = 0;
								sub_total_rata = 0;

								arr += '<tr style="border:1px solid #c0c0c0;">';
								arr += '<td style="border-right:none;"></td>';
								arr += '<td style="font-weight:bold;border-left: none;" colspan="10">'+key+'</td>';
								arr += '</tr>';

								$.each(val,function(k,v){

								arr += '<tr class="border-top-black">';
									arr += '<td class="border-black text-center">'+(ctr+1)+'</td>';

									// EMPLOYEE
									arr += '<td class="border-black text-left">&nbsp;'+v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename+'</td>';
									salary = (v.net_pay) ? (v.net_pay)/2 : 0;

									if(v.positionitems.position_id !== null){
										arr += '<td class="border-black">&nbsp;'+v.positions.Name+'</td>';

									}else{
										arr += '<td class="border-black"></td>';
									}


									representation_amount = (v.representation_amount) ? parseFloat(v.representation_amount).toFixed(2) : 0;
									sub_total_representation_amount += parseFloat(representation_amount);
									arr += '<td class="text-center">'+commaSeparateNumber(representation_amount) +'</td>';

									transportation_amount = (v.transportation_amount) ? parseFloat(v.transportation_amount).toFixed(2) : 0;
									sub_total_transportation_amount += parseFloat(transportation_amount);
									arr += '<td class="text-center">'+commaSeparateNumber(transportation_amount) +'</td>';

									leave_list = Array();
									if(data['leave'] !== undefined){

										$.each(data['leave'][v.employees.id],function(k1,v1){
											leave_list +=  v1+' ('+k1+')';
										});
										arr += '<td class="text-center">'+leave_list+'</td>';
									}else{
										arr += '<td class="text-center"></td>';
									}

									arr += '<td class="text-center">'+v.number_of_actual_work+'</td>';
									percentage = (v.percentage_of_rata_value * 100);
									arr += '<td class="text-center">'+percentage+'% </td>';

									total_rata = (parseFloat(representation_amount) + parseFloat(transportation_amount));
									arr += '<td class="text-center">'+commaSeparateNumber(representation_amount) +'</td>';
									arr += '<td class="text-center">'+commaSeparateNumber(transportation_amount) +'</td>';

									sub_total_rata += parseFloat(total_rata);
									total_rata = (total_rata) ? parseFloat(total_rata).toFixed(2) : '-'
									arr += '<td class="text-center">'+commaSeparateNumber(total_rata) +'</td>';
								arr += '</tr>';
								ctr++;
								});
								ctr = 0;
								arr += '<tr style="border:1px solid #c0c0c0;">';
								arr += '<td style="border:none"></td>';
								arr += '<td style="border:none" colspan="2"><b>Sub Total</b></td>';

								net_representation_amount += parseFloat(sub_total_representation_amount);
								sub_total_representation_amount = (sub_total_representation_amount) ? parseFloat(sub_total_representation_amount).toFixed(2) : 0;
								sub_total_representation_amount = (sub_total_representation_amount !== 0) ? parseFloat(sub_total_representation_amount).toFixed(2) : '-';
								arr += '<td style="border:none" class="text-center"><b>'+commaSeparateNumber(sub_total_representation_amount) +'</b></td>';

								net_transportation_amount += parseFloat(sub_total_transportation_amount);
								sub_total_transportation_amount = (sub_total_transportation_amount) ? parseFloat(sub_total_transportation_amount).toFixed(2) : 0;
								sub_total_transportation_amount = (sub_total_transportation_amount) ? parseFloat(sub_total_transportation_amount).toFixed(2) : '-';
								arr += '<td style="border:none" class="text-center"><b>'+commaSeparateNumber(sub_total_transportation_amount) +'</b></td>';

								arr += '<td style="border:none"></td>';
								arr += '<td style="border:none"></td>';
								arr += '<td style="border:none"></td>';

								arr += '<td style="border:none" class="text-center"><b>'+commaSeparateNumber(sub_total_representation_amount) +'</b></td>';
								arr += '<td style="border:none" class="text-center"><b>'+commaSeparateNumber(sub_total_transportation_amount) +'</b></td>';


								net_rata += parseFloat(sub_total_rata);
								sub_total_rata = (sub_total_rata) ? parseFloat(sub_total_rata).toFixed(2) : '-';
								arr += '<td style="border:none"><b>'+commaSeparateNumber(sub_total_rata) +'</b></td>';
								arr += '</tr>';
							});

						net_representation_amount = (net_representation_amount == 0) ? '-' : parseFloat(net_representation_amount).toFixed(2);
						$('.net_representation_amount').text(commaSeparateNumber(net_representation_amount) );

						net_transportation_amount = (net_transportation_amount == 0) ? '-' : parseFloat(net_transportation_amount).toFixed(2);
						$('.net_transportation_amount').text(commaSeparateNumber(net_transportation_amount) );

						net_rata = (net_rata == 0) ? '-' : parseFloat(net_rata).toFixed(2);
						$('#net_rata').text(commaSeparateNumber(net_rata) );

						$('#tbl_body').html(arr);

						$('#month_year').text(_Month+' '+_Year);

						$('#btnModal').trigger('click');
					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}


	});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})
})
</script>
@endsection