@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printpayslip.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<div class="panel panel-default">
		<div class="panel-body">
			@include('payrolls.reports.includes._table-posted-payslip')
			<form action="{{ url($module_prefix.'/'.$module) }}" method="post" id="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-12">
						<span>Employee Name</span>
						<div class="form-group">
							<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
								<option value=""></option>
								@foreach($employeeinfo as $value)
								<option value="{{ $value->id }}">{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<span>Transaction Date</span>
						<div class="form-group">
							<select class="employee-type form-control font-style2 select2" id="select_month" name="month" placeholder="Month">
								@foreach($months as $key => $month)
								<option value="{{$key}}" {{ ($key == $current_month) ? 'selected' : '' }}>{{ $month }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group" style="margin-top: 15px;">
							<select class="employee-type form-control font-style2 select2" id="select_year" name="year">
								@foreach( range($latest_year,$earliest_year) as $i)
								<option value="{{$i}}">{{$i}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6" style="padding-top: 10px;">
						<div class="form-group">
							<span>Initial</span>
							<input type="text" name="code_2" id="code_2" class="form-control font-style2">
						</div>
					</div>
				</div>
				<br>
				@include('payrolls.reports.includes._post-button')
			</form>
		</div>
	</div>

</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div style="width: 760px;margin: auto;" id="reports">
		       <div class="row" style="margin-left: -5px;">
   					<div class="col-md-7">
   						<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
   					</div>
   					<div class="col-md-5 text-right">
   						<span style="font-size: 12px;">
   							<i class="fas fa-map-marker"></i> 25/F Vertis North Corporate Center I <br>
							North Avenue, Quezon City 1105 <br>
							<i class="fas fa-envelope"></i> queries@phcc.gov.ph <br>
							<i class="fas fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
   						</span>
   					</div>
		       </div>
		       <hr>
		       <div class="row" style="margin-left: -5px;">
		       		<div class="col-md-7 text-right">
		       			<span  style="font-size: 14px;font-weight: bold;">PAYSLIP</span> <br>
		       			<span id="for_month" style="font-size: 12px;"></span>
		       		</div>
		       		<div class="col-md-5 text-right">
		       			<!-- <span style="font-weight: bold;color: red;" id="hold_payslip"></span> -->
		       		</div>
		       </div>
		       <div style="height: 30px;"></div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-2">
		       			<span style="padding-left: 20px;">Name</span>
		       		</div>
		       		<div class="col-md-9">
		       			<span id="fullname"></span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-2">
		       			<span style="padding-left: 20px;">Employee No.</span>
		       		</div>
		       		<div class="col-md-9">
		       			<span id="empno"></span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-2">
		       			<span style="padding-left: 20px;">Position</span>
		       		</div>
		       		<div class="col-md-9">
		       			<span id="position"></span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-2">
		       			<span style="padding-left: 20px;">Office</span>
		       		</div>
		       		<div class="col-md-9">
		       			<span id="office"></span>
		       		</div>
		       </div>
		       <hr>
		       <div class="row" style="margin-left: -5px;font-weight: bold;">
		       		<div class="col-md-6 text-right">
		       			<span>Monthly</span>
		       		</div>
		       		<div class="col-md-4 text-right">
		       			<span>Total</span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-6">
		       			<span style="padding-left: 10px;font-weight: bold;">*EARNINGS*</span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 15px;">Basic Salary</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="basic_salary"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 15px;">PERA</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="pera"></span>
					</div>
					<div class="col-md-4 text-right">
						<span id="totalearnings"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-6">
		       			<span style="padding-left: 10px;font-weight: bold;">Mandatory Deductions</span>
		       		</div>
		        </div>
		        <div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;">BIR Withholding Tax</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="taxamount"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;">GSIS Contribution</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="gsiscontribution"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;">HDMF Contribution</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="pagibigcontribution"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;">PHIC Contribution</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="philhealtcontribution"></span>
					</div>
					<div class="col-md-2 text-right">
						<span id="totalcontribution"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;padding-top: 5px; font-size: 12px;">
		       		<div class="col-md-6">
		       			<span style="padding-left: 10px;font-weight: bold;">Other Deductions</span>
		       		</div>
		        </div>
		        <div id="loanlists" style="font-size: 12px;"></div>
		        <div id="deduction_list" style="font-size: 12px;"></div>
		        <div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;" class="lwop" id="lwop">LWOP</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="lwop_amount"></span>
					</div>
				</div>
		        <div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;" class="hdmf2" id="hdmf2">HDMF MP2</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="pagibig_two_amount" class="hdmf2"></span>
					</div>
					<div class="col-md-2 text-right">
						<span class="other_deduct_amount"></span>
					</div>
					<div class="col-md-2 text-right">
						<span id="total_deduction">0.00</span>
					</div>
				</div>

				<!-- Net Pay -->
				<div class="row" style="margin-left: -5px;margin-top: 20px;font-size: 12px;">
					<div class="col-md-8 text-right">
						<span style="font-weight: bold;" id="span_net_pay"></span>
					</div>
					<div class="col-md-2">
						<span id="netpay" style="position: absolute;right: -5px;font-weight: bold;"> </span>
					</div>
				</div>

				<!-- Additional Adjustment -->
				<div class="row  row_adjustment" style="margin-left: -5px;margin-top: 5px;font-size: 12px;">
					<div class="col-md-6">
		       			<span style="padding-left: 10px;font-weight: bold;">Adjustment</span>
		       		</div>
				</div>
				<div class="row row_add_adjustment"  style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-6">
						<span style="padding-left: 30px;">Add  <span class="adj_remarks"></span> </span>
					</div>
					<div class="col-md-4">
						<span id="add_adj" style="position: absolute;right: -5px;"> </span>
					</div>
				</div>
				<div class="row  row_less_adjustment"  style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-6">
						<span style="padding-left: 30px;">Less  <span class="less_remarks"></span> </span>
					</div>
					<div class="col-md-4">
						<span id="less_adj" style="position: absolute;right: -5px;"> </span>
					</div>
				</div>

				<!-- Net Pay after adjustment -->
				<div class="row row_adjustment" style="margin-left: -5px;margin-top: 5px;font-size: 12px;">
					<div class="col-md-8 text-right">
						<span style="font-weight: bold;">NET PAY AFTER ADJUSTMENT</span>
					</div>
					<div class="col-md-2">
						<span id="net_pay_after_adj" style="position: absolute;right: -5px;font-weight: bold;"> </span>
					</div>
				</div>

				<div class="row" style="margin-left: -5px;margin-top: 10px;font-size: 12px;">
					<div class="col-md-6 text-right">
						<span  id="firsthalf">First Half</span>
					</div>
					<div class="col-md-4">
						<span id="firsthalf_amount" style="position: absolute;right: -5px;font-weight: bold;">0.00</span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-6 text-right">
						<span  id="firsthalf">Second Half</span>
					</div>
					<div class="col-md-4">
						<span id="secondhalf_amount" style="position: absolute;right: -5px;font-weight: bold;">0.00</span>
					</div>
				</div>

				<!-- Other Compensation / Special Payroll -->
				<div class="row hidden" id="compensation" style="margin-left: -5px;padding-top: 5px; font-size: 12px;">
		       		<div class="col-md-6">
		       			<span style="padding-left: 10px;font-weight: bold;">Other Compensations</span>
		       		</div>
		        </div>
		        <div class="other_compensation_list" style="font-size: 12px;"></div>

				<div style="height: 5em;"></div>
				<div class="row form-group" style="margin-left: -5px;font-size: 10px;">
					<div class="col-md-12">
						<i >This is a computer generated document and does</i> <br>
						<i>not require any signature if without alterations</i>
					</div>
				</div>
				<div class="row form-group" style="margin-left: -5px; font-size: 10px;" >
					<div class="col-sm-6">
						<!-- <i id="ao"></i> <br> -->
						<i id="hrd"></i>
					</div>
					<div class="col-sm-6 text-right" >
						Run Date: <span id="run_date"></span>
					</div>
				</div>
	   	   </div>
		</div>
	</div>
</div>

@endsection

@section('js-logic2')
@include('payrolls.reports.includes._post-button-script')
<script type="text/javascript">
$(document).ready(function(){

// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$('#select_year').trigger('change');
	$('#select_month').trigger('change');

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})


var codeOne;
var codeTwo;
$(document).on('keyup','#code_1',function(){
	codeOne = "";
	codeOne = $(this).val();

});

$(document).on('keyup','#code_2',function(){
	codeTwo = "";
	codeTwo = $(this).val();

});

// View posted report
$(document).on('click','.view',function(){
	_Year 	= $(this).data('year');
	_Month 	= $(this).data('month');
	_empid 	= $(this).data('employee_id');
	codeOne = $(this).data('code_1');
	codeTwo = $(this).data('code_2');
	$('#preview').trigger('click');
});

var months ={
	1:'January',
	2:'February',
	3:'March',
	4:'April',
	5:'May',
	6:'June',
	7:'July',
	8:'August',
	9:'September',
	10:'October',
	11:'November',
	12:'December',
}


$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
	}else{

		$.ajax({
			url:base_url+module_prefix+module+'/getPayslip',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('.row_adjustment').addClass('hidden');
				$('.row_add_adjustment').addClass('hidden');
				$('.row_less_adjustment').addClass('hidden');

				if(data.transaction !== null){

					firstname = data.transaction.employees.firstname;
					lastname = data.transaction.employees.lastname;
					middlename = data.transaction.employees.middlename;
					employee_number = data.transaction.employees.employee_number;
					office = (data.transaction.offices) ? data.transaction.offices.Name : '';
					position = (data.transaction.positions !== null) ? data.transaction.positions.Name : '';

					middlename = (middlename) ? middlename+'. ' : '';
					fullname = firstname+' '+middlename+' '+lastname;

					basicSalary = (data.transaction.actual_basicpay_amount) ? data.transaction.actual_basicpay_amount : 0;
					defaultPera = (data.transaction.benefitinfo) ? data.transaction.benefitinfo.benefit_amount : 0;
					totalEarnings = (parseFloat(basicSalary) + parseFloat(defaultPera));
					taxAmount = (data.transaction.tax_amount) ? data.transaction.tax_amount : 0;
					taxAmountTwo = (data.transaction.additional_tax_amount) ? data.transaction.additional_tax_amount : 0;
					gsisContAmount = (data.transaction.gsis_ee_share) ? data.transaction.gsis_ee_share : 0;
					pagibigContAmount = (data.transaction.pagibig_share) ? data.transaction.pagibig_share : 0;
					pagibigPersonal = (data.transaction.mp1_amount) ? data.transaction.mp1_amount : 0;
					philhealthContAmount = (data.transaction.phic_share) ? data.transaction.phic_share : 0;
					pagibigTwoAmount = (data.transaction.mp2_amount) ? data.transaction.mp2_amount : 0;
					hasAbsent = (data.transaction.actual_absences) ? data.transaction.actual_absences : 0;

					lessGrossBasicAmount = 0;
					lessGrossPeraAmount  = 0;
					totalLWOP 			 = 0;
					lwopAmount 			 = 0;
					if(hasAbsent){
						lwopPeraAmount = (data.transaction.benefit_transaction) ? data.transaction.benefit_transaction.amount : 0;
						lwopAmount = (data.transaction.total_absences_amount) ? data.transaction.total_absences_amount : 0;
						lessGrossPeraAmount  = parseFloat(defaultPera) - parseFloat(lwopPeraAmount);
					}

					totalLWOP = Number(lwopAmount) + Number(lessGrossPeraAmount);

					// Adjustment
					addAdj 	= (data.transaction.additional_adj) ? data.transaction.additional_adj : 0;
					lessAdj = (data.transaction.deduction_adj) ? data.transaction.deduction_adj : 0;

					if(addAdj !== 0 && addAdj !== '0.00' || lessAdj !== 0 && lessAdj !== '0.00'){
						$('.row_adjustment').removeClass('hidden');
						if(addAdj !== 0 && addAdj !== '0.00'){
							$('.row_add_adjustment').removeClass('hidden');
						}
						if(lessAdj !== 0 && lessAdj !== '0.00'){
							$('.row_less_adjustment').removeClass('hidden');
						}
						$('#span_net_pay').text('NET PAY BEFORE ADJUSTMENT');
					}else{
						$('#span_net_pay').text('NET PAY ');
					}
					addRemarks = (data.transaction.additional_remarks) ? data.transaction.additional_remarks : '';
					lessRemarks = (data.transaction.less_remarks) ? data.transaction.less_remarks : '';
					adjAmount = Number(addAdj) - Number(lessAdj);

					pagibigContAmount = parseFloat(pagibigContAmount) + parseFloat(pagibigPersonal);
					totalTaxAmount = parseFloat(taxAmount) + parseFloat(taxAmountTwo);
					totalContAmount = (parseFloat(totalTaxAmount) + parseFloat(gsisContAmount) + parseFloat(pagibigContAmount) + parseFloat(philhealthContAmount));


					basic_salary = (basicSalary !== 0) ? commaSeparateNumber(parseFloat(basicSalary).toFixed(2)) : '';
					pera_amount = (defaultPera !== 0) ? commaSeparateNumber(parseFloat(defaultPera).toFixed(2)) : '';
					total_earnings = (totalEarnings !== 0) ? commaSeparateNumber(parseFloat(totalEarnings).toFixed(2)) : '';
					tax_amount = (totalTaxAmount !== 0) ? commaSeparateNumber(parseFloat(totalTaxAmount).toFixed(2)) : '';
					gsis_cont_amount = (gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(gsisContAmount).toFixed(2)) : '';
					pagibig_cont_amount = (pagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(pagibigContAmount).toFixed(2)) : '';
					philhealth_cont_amount = (philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(philhealthContAmount).toFixed(2)) : '';
					total_cont_amount = (totalContAmount !== 0) ? commaSeparateNumber(parseFloat(totalContAmount).toFixed(2)) : '';
					pagibig_two_amount = (pagibigTwoAmount !== 0) ? commaSeparateNumber(parseFloat(pagibigTwoAmount).toFixed(2)) : '';
					add_adj = (addAdj !== 0) ? commaSeparateNumber(parseFloat(addAdj).toFixed(2)) : '';
					less_adj = (lessAdj !== 0) ? commaSeparateNumber(parseFloat(lessAdj).toFixed(2)) : '';
					lwop_amount = (totalLWOP !== 0) ? commaSeparateNumber(parseFloat(totalLWOP).toFixed(2)) : '';

					$('#basic_salary').text(basic_salary);
					$('#pera').text(pera_amount);
					$('#totalearnings').text(total_earnings);
					$('#taxamount').text(tax_amount);
					$('#gsiscontribution').text(gsis_cont_amount);
					$('#pagibigcontribution').text(pagibig_cont_amount);
					$('#philhealtcontribution').text(philhealth_cont_amount);
					$('#totalcontribution').text(total_cont_amount);
					$('#pagibig_two_amount').text(pagibig_two_amount);
					$('#lwop_amount').text(lwop_amount);

					$('#less_adj').text(less_adj);
					$('#add_adj').text(add_adj);
					$('.adj_remarks').text(addRemarks);
					$('.less_remarks').text(lessRemarks);

					if(pagibigTwoAmount == 0){
						$('.hdmf2').hide();
					}else{
						$('#hdmf2').show('HDMF 2');
					}

					if(totalLWOP == 0){
						$('.lwop').hide();
					}else{
						$('#lwop').show('LWOP');
					}

					codeOne = (codeOne) ? codeOne : '';
					codeTwo = (codeTwo) ? codeTwo : '';

					$('#for_month').text(months[_Month]+' '+_Year);
					$('#fullname').text(fullname);
					$('#empno').text(employee_number);
					$('#office').text(office);
					$('#position').text(position);
					$('#ao').text('AO-'+_Year+_Month+'-'+codeOne);
					$('#hrd').text('hrdd/'+codeTwo);
					$('#run_date').text(data.date_run);

					// If salary is on hold
					// if(data.transaction.hold == 1){
					// 	$('#hold_payslip').text('"PAYROLL ON HOLD"');
					// }else{
					// 	$('#hold_payslip').text('');
					// }

					row = [];
					totalLoanAmount = 0;
					$('#loanlists').html('');
					if(data.loaninfo.length !== 0){
						$.each(data.loaninfo,function(k,v){

							loanAmount = (v.amount) ? v.amount : 0;

							loan_amount = (loanAmount !== 0) ? commaSeparateNumber(parseFloat(loanAmount).toFixed(2)) : '';

							row += '<div class="row" style="margin-left: -5px;">';
							row +=	'<div class="col-md-4">';
							row +=		'<span style="padding-left: 30px;">'+v.loans.name+'</span>';
							row +=	'</div>';
							row +=	'<div class="col-md-2 text-right">';
							row +=		'<span>'+loan_amount+'</span>';
							row +=	'</div>';
							// row +=	'<div class="col-md-6 text-right">';
							// row +=		'<span>₱0.00</span>';
							// row +=	'</div>';
							row +=  '</div>';

							totalLoanAmount += parseFloat(loanAmount);
						});

						$('#loanlists').html(row);
					}

					totalOtherDeduction = 0;
					row2 = [];
					$('#deduction_list').html('');
					if(data.deductioninfo.length !== 0){
						$.each(data.deductioninfo,function(k,v){

							deductiontAmount = (v.amount) ? v.amount : 0;

							deduction_amount = (deductiontAmount !== 0) ? commaSeparateNumber(parseFloat(deductiontAmount).toFixed(2)) : '';

							row2 += '<div class="row" style="margin-left: -5px;">';
							row2 +=	'<div class="col-md-4">';
							row2 +=		'<span style="padding-left: 30px;">'+v.deductions.name+'</span>';
							row2 +=	'</div>';
							row2 +=	'<div class="col-md-2 text-right">';
							row2 +=		'<span>'+deduction_amount+'</span>';
							row2 +=	'</div>';
							// row2 +=	'<div class="col-md-6 text-right">';
							// row2 +=		'<span>₱0.00</span>';
							// row2 +=	'</div>';
							row2 +=  '</div>';

							totalOtherDeduction += parseFloat(deductiontAmount);
						});

						$('#deduction_list').html(row2);
					}



					total_loan_amount = (totalLoanAmount) ? commaSeparateNumber(parseFloat(totalLoanAmount).toFixed(2)) : '';

					$('#totalloans').text(total_loan_amount);

					otherDeductAmount = parseFloat(totalLoanAmount) + parseFloat(pagibigTwoAmount) + Number(totalLWOP) + Number(totalOtherDeduction);
					other_deduct_amount = (otherDeductAmount !== 0) ? commaSeparateNumber(parseFloat(otherDeductAmount).toFixed(2)) : '';

					totalDeduction = parseFloat(totalContAmount) + parseFloat(totalLoanAmount) + parseFloat(pagibigTwoAmount) + Number(totalLWOP) + Number(totalOtherDeduction);

					netPay = parseFloat(totalEarnings) - parseFloat(totalDeduction);
					netPayAfterAdj = Number(netPay) + Number(adjAmount);

					total_deduction = (totalDeduction !== 0) ? commaSeparateNumber(parseFloat(totalDeduction).toFixed(2)) : '';
					net_pay = (netPay !== 0) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';
					net_pay_after_adj = (netPayAfterAdj !== 0) ? commaSeparateNumber(parseFloat(netPayAfterAdj).toFixed(2)) : '';

					// dividedNetPay = parseFloat(netPay) / 2;
					// if(netPayAfterAdj !== 0){
					// 	dividedNetPay = parseFloat(netPayAfterAdj) / 2;
					// }

					// getDecimal = parseFloat(dividedNetPay).toFixed(2).split('.');
					// getDecimal = parseFloat(getDecimal[1])/100;
					// secondHalfAmount = parseFloat(dividedNetPay) + getDecimal;

					// secondHalfAmount = Math.round(secondHalfAmount * 100) /100;

					firstHalfAmount = (data.transaction.net_first_half) ? data.transaction.net_first_half : 0;
					secondHalfAmount = (data.transaction.net_second_half) ? data.transaction.net_second_half : 0;

					firsthalf_amount = (firstHalfAmount !== 0) ? commaSeparateNumber(firstHalfAmount) : '';
					secondhalf_amount = (secondHalfAmount !== 0) ? commaSeparateNumber(secondHalfAmount) : '';
					$('.other_deduct_amount').text(other_deduct_amount);
					$('#total_deduction').text(total_deduction);
					$('#netpay').text(net_pay);
					$('#firsthalf_amount').text(firsthalf_amount);
					$('#secondhalf_amount').text(secondhalf_amount);
					$('#net_pay_after_adj').text(net_pay_after_adj);

					// Other Compensation / Special Payroll
					row2 = [];
					$('#compensation').addClass('hidden');
					if(data.spayroll.length !== 0){
						$('#compensation').removeClass('hidden');
						$.each(data.spayroll,function(k,v){

							sName = "";
							if(v.status == 'eme'){
								sName = "EME";
							}else if(v.status == 'cea'){
								sName = "Communication Allowance";
							}

							// if(v.status == 'ua'){
							// 	sName = "Clothing Allowance";
							// }else if(v.status == 'eme'){
							// 	sName = "EME";
							// }else if(v.status == 'cea'){
							// 	sName = "Communication Allowance";
							// }else if(v.status == 'pei'){
							// 	sName = "Productivity Ehancement Incentive";
							// }else if(v.status == 'pbb'){
							// 	sName = "Performance Base Bonus";
							// }else if(v.status == 'midyearbonus'){
							// 	sName = "Mid Year Bonus";
							// }else if(v.status == 'yearendbonus'){
							// 	sName = "Year End Bonus";
							// }else if(v.status == 'honoraria'){
							// 	sName = "Year End Bonus";
							// }


							sAmount 	= (v.amount) ? v.amount : 0;
							meAmount 	= (v.me_amount) ? v.me_amount : 0;
							if(meAmount){
								sAmount = Number(sAmount) + Number(meAmount);
							}

							s_amount = (sAmount !== 0) ? commaSeparateNumber(parseFloat(sAmount).toFixed(2)) : '';

							if(sName !== ""){
								row2 += '<div class="row" style="margin-left: -5px;">';
								row2 +=	'<div class="col-md-4">';
								row2 +=		'<span style="padding-left: 30px;">'+sName+'</span>';
								row2 +=	'</div>';
								row2 +=	'<div class="col-md-2 text-right">';
								row2 +=		'<span>'+s_amount+'</span>';
								row2 +=	'</div>';
								// row2 +=	'<div class="col-md-6 text-right">';
								// row2 +=		'<span>₱0.00</span>';
								// row2 +=	'</div>';
								row2 +=  '</div>';
							}
						});

					}

					taAmount 	 = 0;
					raAmount 	 = 0;
					raDiffAmount = 0;
					raDiffAmount = 0;
					if(data.rtransact !== null){
						$('#compensation').removeClass('hidden');
						taAmount = (data.rtransact.transportation_amount) ? data.rtransact.transportation_amount : 0;
						raAmount = (data.rtransact.representation_amount) ? data.rtransact.representation_amount : 0;
						raDiffAmount = (data.rtransact.ra_diff_amount) ? data.rtransact.ra_diff_amount : 0;
						taDiffAmount = (data.rtransact.ta_diff_amount) ? data.rtransact.ta_diff_amount : 0;

						ta_amount = Number(taAmount) - Number(taDiffAmount);
						ra_amount = Number(raAmount) - Number(raDiffAmount);

						ta_amount = (ta_amount !== 0) ? commaSeparateNumber(parseFloat(ta_amount).toFixed(2)) : '';
						ra_amount = (ra_amount !== 0) ? commaSeparateNumber(parseFloat(ra_amount).toFixed(2)) : '';

						if(ta_amount){
							row2 += '<div class="row" style="margin-left: -5px;">';
							row2 +=	'<div class="col-md-4">';
							row2 +=		'<span style="padding-left: 30px;">Transportation Allowance</span>';
							row2 +=	'</div>';
							row2 +=	'<div class="col-md-2 text-right">';
							row2 +=		'<span>'+ta_amount+'</span>';
							row2 +=	'</div>';
							// row2 +=	'<div class="col-md-6 text-right">';
							// row2 +=		'<span>₱0.00</span>';
							// row2 +=	'</div>';
							row2 +=  '</div>';
						}

						if(ra_amount){
							row2 += '<div class="row" style="margin-left: -5px;">';
							row2 +=	'<div class="col-md-4">';
							row2 +=		'<span style="padding-left: 30px;">Representation Allowance</span>';
							row2 +=	'</div>';
							row2 +=	'<div class="col-md-2 text-right">';
							row2 +=		'<span>'+ra_amount+'</span>';
							row2 +=	'</div>';
							// row2 +=	'<div class="col-md-6 text-right">';
							// row2 +=		'<span>₱0.00</span>';
							// row2 +=	'</div>';
							row2 +=  '</div>';
						}
					}

					$('.other_compensation_list').html(row2);

					$('#btnModal').trigger('click');

				}else{

					swal({
						  title: "No Record Found",
						  type: "warning",
						  showCancelButton: false,
						  confirmButtonClass: "btn-danger",
						  confirmButtonText: "Yes",
						  closeOnConfirm: false

					});
				}

			}
		})

	}


});

$(document).on('click','#print',function(){
	$('#reports').printThis();
});


})
</script>
@endsection