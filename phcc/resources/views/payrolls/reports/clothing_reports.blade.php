@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Mid Left</span>
							<select class="form-control font-style2 select2" id="sign_mid_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_mid_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Mid Right</span>
							<select class="form-control font-style2 select2" id="sign_mid_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_mid_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Bottom Left</span>
							<select class="form-control font-style2 select2" id="sign_bot_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var signTopLeft;
	var positionTopLeft;
	$('#sign_top_left').change(function(){
		signTopLeft = "";
		positionTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		positionTopLeft = $(this).find(':selected').data('position_top_left')
	});

	var signTopRight;
	var positionTopRight;
	$('#sign_top_right').change(function(){
		signTopRight = "";
		positionTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		positionTopRight = $(this).find(':selected').data('position_top_right')
	});

	var signMidRight;
	var positionMidRight;
	$('#sign_mid_right').change(function(){
		signMidRight = "";
		positionMidRight  = "";
		signMidRight = $(this).find(':selected').text()
		positionMidRight = $(this).find(':selected').data('position_mid_right')
	});

	var signMidLeft;
	var positionMidLeft;
	$('#sign_mid_left').change(function(){
		signMidLeft = "";
		positionMidLeft  = "";
		signMidLeft = $(this).find(':selected').text()
		positionMidLeft = $(this).find(':selected').data('position_mid_left')
	});

	var signBotLeft;
	var positionBotLeft;
	$('#sign_bot_left').change(function(){
		signBotLeft = "";
		positionBotLeft  = "";
		signBotLeft = $(this).find(':selected').text()
		positionBotLeft = $(this).find(':selected').data('position_bot_left')
	});
	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						netCtr = 0;

						coveredPeriod = months[_Month]+' '+_Year;

						netAmount = 0;

						body += '<table class="table" style="border:none;">';
						body += '<thead class="text-center">';
						body += '<tr>';
						body += '<td class="text-left" colspan="9" style="border-left:none;border-right:none;">';
						body += '<img src="{{ url("images/new-logo.jpg") }}" style="height: 80px;">';
	       				body += '<h5><b>CLOTHING ALLOWANCE FOR CY 2019</b> :'+coveredPeriod+'</h5></td>';
						body += '</tr>';
						body += '<tr class="text-center" style="font-weight:bold">';
						body += '<td rowspan="2" style="vertical-align:middle;">#</td>';
						body += '<td rowspan="2" style="vertical-align:middle;" >NAME</td>';
						body += '<td rowspan="2" style="vertical-align:middle;">REMARKS</td>';
						body += '<td colspan="4" style="vertical-align:middle;">CLOTHING ALLOWANCE '+_Year+'</td>';
						body += '</tr>';
						body += '<tr class="text-center font-weight-bold">';
						body += '<td>Position</td>';
						body += '<td>Assumption</td>';
						body += '<td>6th Month</td>';
						body += '<td>Amount</td>';
						body += '</tr>';
						body += '</thead>';

						body += '<tbody>';

						$.each(data.transaction,function(key,val){

							body += '<tr>';
							body += '<td style="font-weight:bold;" colspan="9">'+key+'</td>'
							body += '</tr>';

							subAmount = 0;
							remarks = "";
							// ======== BODY =======
							$.each(val,function(k,v){

								lastname = (v.employees) ? v.employees.lastname : '';
								firstname = (v.employees) ? v.employees.firstname : '';
								middlename = (v.employees) ? v.employees.middlename : '';
								fullname = lastname+' '+firstname+' '+middlename;
								position = (v.positions) ? v.positions.Name : '';
								assumption_date = (v.employeeinformation) ? v.employeeinformation.assumption_date : '';
								remarks = (v.special_remarks) ? v.special_remarks : '';
								amount = (v.amount) ? v.amount : 0;
								// ===== SUB TOTAL COMPUTATION =====

								subAmount += parseFloat(amount);

								amount = (amount) ? commaSeparateNumber(parseFloat(amount).toFixed(2)) : '';

								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

								body += '<tr class="text-center">';
								body += '<td>'+ctr+'</td>'
								body += '<td class="text-left" nowrap>'+fullname+'</td>'
								body += '<td class="text-left">'+remarks+'</td>'
								body += '<td class="text-left">'+position+'</td>'
								body += '<td >'+assumption_date+'</td>'
								body += '<td ></td>'
								body += '<td class="text-right">'+amount+'</td>'
								body += '</tr>';

								ctr++;

							});
							// ======== BODY =======

							ctr = parseInt(ctr) - 1;

							netCtr += parseInt(ctr);
							// ===== COMPUTE NET AMOUNT  =====

							netAmount += parseFloat(subAmount);

							sub_amount = (subAmount) ? commaSeparateNumber(parseFloat(subAmount).toFixed(2)) : '';

							body += '<tr style="font-weight:bold;">';
							body += '<td class="text-center">'+ctr+'</td>'
							body += '<td class="text-center">SUB TOTAL</td>'
							body += '<td class="text-left"></td>'
							body += '<td ></td>'
							body += '<td ></td>'
							body += '<td class="text-right"></td>'
							body += '<td class="text-right">'+sub_amount+'</td>'
							body += '</tr>';

							// ======= SUB TOTAL ======
						ctr = 1;

						});

						net_amount = (netAmount) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';

						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';

						// ======= GRAND TOTAL ======
						body += '<tr  style="font-weight:bold;" class="text-center">';
						body += '<td >'+netCtr+'</td>'
						body += '<td > GRAND TOTAL</td>'
						body += '<td ></td>'
						body += '<td ></td>'
						body += '<td ></td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-right">'+net_amount+'</td>'
						body += '</tr>';
						// ======= GRAND TOTAL ======

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						positionTopLeft = (positionTopLeft) ? positionTopLeft : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						positionTopRight = (positionTopRight) ? positionTopRight : '';
						signMidRight = (signMidRight) ? signMidRight : '';
						positionMidRight = (positionMidRight) ? positionMidRight : '';
						signMidLeft = (signMidLeft) ? signMidLeft : '';
 						positionMidLeft = (positionMidLeft) ? positionMidLeft : '';
 						signBotLeft = (signBotLeft) ? signBotLeft : '';
 						positionBotLeft = (positionBotLeft) ? positionBotLeft : '';

						body += '<tr class="text-justify borderless">';
						body +=	'<td style="border:none;padding-top:50px;" colspan="10" >Certification: This is to certify that the aforementioned personnel satisfy the government service requirement to receive the Uniform/Clothing Allowance (U/CA) as </td>';
						body += '</tr>';
						body += '<tr>';
						body += '<td style="border:none;" colspan="3"><b>'+signTopLeft+'</b> <br> '+positionTopLeft+'</td>';
						body += '<td style="border:none;" colspan="7"><b>'+signTopRight+'</b> <br> '+positionTopRight+'</td>';
						body += '</tr>';
						body += '<tr>';
						body +=	'<td style="border:none;padding-top:50px;"  colspan="3">';
						body += 'Certified: Supporting documents complete and proper, <br> and cash available in the amount of Php _________________________.';
						body += '</td>';
						body +=	'<td style="border:none;padding-top:50px;" colspan="7" >Approved for Payment:</td>';
						body += '</tr>';
						body += '<tr>';
						body += '<td style="border:none;padding-top:50px;" colspan="3"><b>'+signMidLeft+'</b> <br> '+positionMidLeft+' </td>';
						body += '<td style="border:none;padding-top:50px;" colspan="7"><b>'+signMidRight+'</b> <br> '+positionMidRight+'	</td>';
						body += '<tr>';
						body +=	'<td style="border:none;padding-top:50px;" colspan="10" >Certified: Each employee whose name appears on the payroll has been <br> paid the amount as indicated opposite his/her name.</td>';
						body += '</tr>';
						body += '<tr>';
						body += '<td style="border:none;padding-top:50px;" colspan="10"><b>'+signBotLeft+'</b> <br> '+positionBotLeft+'</td>';
						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						body += '</tr>';
						body += '<tr class="text-left">';
						body += '<td style="border:none;padding-top:50px;"  class="text-left" colspan="10"><i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i></td>';
						body += '</tr>';



						body += '</tbody></table>';

						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection