@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">
<style type="text/css">
	.table2>thead>tr>td, .table2>tbody>tr>td{
		padding: 3px !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>

	<div class="panel panel-default" style="padding: 15px;">
		<div class="panel-body">
			@include('payrolls.reports.includes._table-posted')
			<form action="{{ url($module_prefix.'/'.$module) }}" method="post" id="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="signatory_one" id="signatory_one">
				<input type="hidden" name="signatory_two" id="signatory_two">
				<input type="hidden" name="signatory_three" id="signatory_three">
				<input type="hidden" name="signatory_four" id="signatory_four">

				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Left</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_one">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_one" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Mid One</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_two">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_two" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Mid Two</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_three">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_three" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Right</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_four">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_four" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-8"></div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Degree</span>
							<input type="text" name="add_degree" id="add_degree" class="form-control font-style2">
						</div>
					</div>
				</div>
			</div>
			@include('payrolls.reports.includes._post-button')
		</form>
	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       				<!-- <table class="table table2" style="border:none;margin-top:10px;">
						<thead id="tbl_header"></thead>
						<tbody id="tbl_body"></tbody>
						</table> -->
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')

<script type="text/javascript">
$(document).ready(function(){

	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var signatoryOne;
	var positionOne;
	$('#select_signatory_one').change(function(){
		signatoryOne = "";
		positionOne  = "";
		signatoryOne = $(this).find(':selected').text()
		positionOne = $(this).find(':selected').data('position')
	});

	var signatoryTwo;
	var positionTwo;
	$('#select_signatory_two').change(function(){
		signatoryTwo = "";
		positionTwo  = "";
		signatoryTwo = $(this).find(':selected').text()
		positionTwo = $(this).find(':selected').data('position')
	});

	var signatoryThree;
	var positionThree;
	$('#select_signatory_three').change(function(){
		signatoryThree = "";
		positionThree  = "";
		signatoryThree = $(this).find(':selected').text()
		positionThree = $(this).find(':selected').data('position')
	});

	var signatoryFour;
	var positionFour;
	$('#select_signatory_four').change(function(){
		signatoryFour = "";
		positionFour  = "";
		signatoryFour = $(this).find(':selected').text()
		positionFour = $(this).find(':selected').data('position')
	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var addDegree;
	$('#add_degree').on('change',function(){
		addDegree = $(this).val();
	});

	$('.signatory').change(function(){
		id 			= $(this).find(':selected').data('id');
		position 	= $(this).find(':selected').data('position');
		text 		= $(this).find(':selected').text();

		$('#'+id).val(text+'|'+position);
	});

	$('.view').on('click',function(){
		_Year 		= $(this).data('year');
		_Month 		= $(this).data('month');
		signOne 	= $(this).data('signatory_one').split('|');
		signTwo 	= $(this).data('signatory_two').split('|');
		signThree 	= $(this).data('signatory_three').split('|');
		signFour 	= $(this).data('signatory_four').split('|');
		addDegree 	= $(this).data('degree');

		signatoryOne 	= signOne[0];
		positionOne 	= signOne[1];

		signatoryTwo 	= signTwo[0];
		positionTwo 	= signTwo[1];

		signatoryThree 	= signThree[0];
		positionThree 	= signThree[1];

		signatoryFour 	= signFour[0];
		positionFour 	= signFour[1];

		$('#preview').trigger('click');
	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						ctr2 = 0;
						netCtr = 0;
						arr_gsis = [];
						arr_pagibig = [];
						arr_subtotal_gsis = [];
						arr_subtotal_pagibig = [];

						arr_deduction = [];
						arr_subtotal_deduction = [];

						gsisLoanCount 	 = (data.gsisLoanCount) ? data.gsisLoanCount : 0;
						pagibigLoanCount = (data.pagibigLoanCount) ? data.pagibigLoanCount : 0;
						deductionCount 	 = (data.deductionCount) ? data.deductionCount : 0;

						deductionCol = (parseFloat(gsisLoanCount) + parseInt(pagibigLoanCount) + Number(deductionCount) + 7);
						gsisCol = (parseFloat(gsisLoanCount) + 2);
						pagibigCol = (parseInt(pagibigLoanCount) + 3)
						totalCol = deductionCol + 14;

						tr_loan_total = 0;
						netBasicAmount 			= 0;
						netPeraAmount 			= 0;
						netGsisContAmount 		= 0;
						netPagibigContAmount 	= 0;
						netPhilhealthContAmount = 0;
						netPagibig2Amount 		= 0;
						netTaxContAmount 		= 0;
						netLoanAmount 			= 0;
						netLwopAmount 			= 0;
						netGrossPayAmount 		= 0;
						netTotalDeductionAmount = 0;
						NetAmount 				= 0;
						netTotalGsisAmount 		= 0;
						netLessGrossBasicAmount = 0;
						netTotalPagibigAmount 	= 0;
						netLWOPPeraAmount 		= 0;
						netLessGrossPeraAmount 	= 0;

						coveredPeriod = months[_Month]+" "+_Year;


						body += '<table class="table table2" style="border:none;margin-top:10px;">';
						body += '<thead>';
						body += '<tr>';
						body +=	'<td class="text-left" style="border-left:none;border-right:none;" colspan="'+totalCol+'">';
						body += '<img src="{{ url("images/new-logo.jpg") }}" style="height: 60px;">';
			       		body +=	'<h5><b>PAYROLL TRANSFER</b> <br> Covered Period: '+coveredPeriod+'</h5>';
			       		// body += '<img src="{{ url("images/new-logo.jpg") }}" style="height: 80px;">';
					 	body +=	'</td>';
						body += '</tr>';
						body += '<tr class="text-center" style="font-weight:bold">';
						body += '<td rowspan="3" ></td>';
						body += '<td rowspan="3"><br><br>EMPLOYEE NAME</td>';
						body += '<td rowspan="3" style="vertical-align:middle;" >POSITION</td>';
						body += '<td rowspan="3" style="vertical-align:middle;" >ASSUMPTION</td>';
						body += '<td rowspan="3" style="vertical-align:middle;">JG</td>';
						body += '<td colspan="2" style="vertical-align:middle;">SALARY</td>';
						body += '<td colspan="2" style="vertical-align:middle;">LWOP</td>';
						body += '<td colspan="2" style="vertical-align:middle;">GROSS LESS(LWOP)</td>';
						body += '<td rowspan="3" style="vertical-align:middle;">GROSS PAY</td>';
						body += '<td class="text-center" colspan="'+deductionCol+'" style="vertical-align:middle;">DEDUCTIONS</td>';
						body += '<td rowspan="3" style="vertical-align:middle;"><br>TOTAL <br> DEDUCTION</td>';
						// body += '<td rowspan="3" style="vertical-align:middle;"><br>NET <br> PAY <br>(BEFORE) <br>ADJUSTMENT</td>';
						// body += '<td rowspan="3"  style="vertical-align:middle;">ADJ</td>';
						body += '<td rowspan="3" class="text-center" style="vertical-align:middle;">NET PAY</td>';
						body += '</tr>';
						body += '<tr class="text-center" style="font-weight:bold">';
						body += '<td rowspan="2" style="vertical-align:middle;">BASIC</td>';
						body += '<td rowspan="2" style="vertical-align:middle;">PERA</td>';
						body += '<td rowspan="2" style="vertical-align:middle;">BASIC</td>';
						body += '<td rowspan="2" style="vertical-align:middle;">PERA</td>';
						body += '<td rowspan="2" style="vertical-align:middle;">BASIC</td>';
						body += '<td rowspan="2" style="vertical-align:middle;">PERA</td>';
						body += '<td class="text-center" colspan="'+gsisCol+'">GSIS</td>';
						body += '<td rowspan="2" style="vertical-align:middle;">PHILHEALTH</td>';
						body += '<td rowspan="2" style="vertical-align:middle;">W/TAX</td>';
						body += '<td colspan="'+pagibigCol+'" class="text-center">PAGIBIG</td>';
						body += '</tr>';
						body += '<tr class="text-center" style="font-weight:bold">';
						body += '<td style="vertical-align:middle;">PREMIUM</td>';
						if(data.gsisLoanList.length !== 0){
							$.each(data.gsisLoanList,function(k1,v1){
								body += '<td style="vertical-align:middle;">'+v1.loans.name+'</td>';
								arr_gsis[v1.loan_id+'_id'] = v1.loan_id;
								arr_subtotal_gsis[v1.loan_id+'_id']  = 0;
							})
						}
						body += '<td style="vertical-align:middle;">TOTAL GSIS</td>';
						body += '<td style="vertical-align:middle;">PAGIBIG CONT.</td>';
						body += '<td style="vertical-align:middle;">MP2</td>';
						if(data.pagibigLoanList.length !== 0){
							$.each(data.pagibigLoanList,function(k1,v1){
								body += '<td style="vertical-align:middle;">'+v1.loans.name+'</td>';
								arr_pagibig[v1.loan_id+'_id'] = v1.loan_id;
								arr_subtotal_pagibig[v1.loan_id+'_id']  = 0;
							})
						}
						body += '<td style="vertical-align:middle;">TOTAL PAGIBIG</td>';
						if(data.deductionList.length !== 0){
							$.each(data.deductionList,function(k1,v1){
								body += '<td style="vertical-align:middle;">'+v1.deductions.name+'</td>';
								arr_deduction[v1.deduction_id+'_id'] = v1.deduction_id;
								arr_subtotal_deduction[v1.deduction_id+'_id']  = 0;
							})
						}

						body += '</tr>';
						body += '</thead>';
						// $('#tbl_header').html(body)

						body += '<tbody>';
						$.each(data.transaction,function(key,val){

							subBasicAmount 			= 0;
							subPeraAmount 			= 0;
							subGsisContAmount 		= 0;
							subPagibigContAmount 	= 0;
							subPhilhealthContAmount = 0;
							subPagibig2Amount 		= 0;
							subTaxContAmount 		= 0;
							subLoanAmount 			= 0;
							subLwopAmount 			= 0;
							subGrossPayAmount 		= 0;
							subTotalDeductionAmount = 0;
							subNetAmount 			= 0;
							subTotalGsisAmount 		= 0;
							subLessGrossBasicAmount = 0;
							subTotalPagibigAmount 	= 0;
							own_loan_id 			= 0;
							subLWOPPeraAmount 		= 0;
							subLessGrossPeraAmount 	= 0;
							subLoan = [];
							body += '<tr>';
							body += '<td style="font-weight:bold;" colspan="'+totalCol+'">'+key+'</td>'
							body += '</tr>';
							temp1 = [];
							temp = 0;

							// ======== BODY =======
							$.each(val,function(k,v){
								var lwopPeraAmount = 0;
								var lwopAmount 	   = 0;
								var lessGrossBasicAmount = 0;
								var lessGrossPeraAmount = 0;

								lastname = (v.employees) ? v.employees.lastname : '0.00';
								firstname = (v.employees) ? v.employees.firstname : '0.00';
								middlename = (v.employees) ? v.employees.middlename : '0.00';
								fullname = lastname+' '+firstname+' '+middlename;
								position = (v.positions) ? v.positions.Name : '0.00';
								basicAmount = (v.actual_basicpay_amount) ? v.actual_basicpay_amount : 0;
								job_grade = (v.salaryinfo.jobgrade) ? v.salaryinfo.jobgrade.Code : 0;
								assumption_date = (v.employeeinformation) ? v.employeeinformation.assumption_date : '0.00';
								peraAmount = (v.benefitinfo) ? v.benefitinfo.benefit_amount : 0;
								gsisContAmount = (v.gsis_ee_share) ? v.gsis_ee_share : 0;
								pagibigPersonal = (v.mp1_amount) ? v.mp1_amount : 0;
								pagibigContAmount = (v.pagibig_share) ? v.pagibig_share : 0;
								philhealthContAmount = (v.phic_share) ? v.phic_share : 0;
								pagibig2Amount = (v.mp2_amount) ? v.mp2_amount : 0;
								taxContAmount = (v.tax_amount) ? v.tax_amount : 0;
								taxContAmountTwo = (v.additional_tax_amount) ? v.additional_tax_amount : 0;
								totalLoanAmount = (v.total_loan) ? v.total_loan : 0;
								pagibigLoanAmount = (v.pagibig_loan) ? v.pagibig_loan : 0;
								gsisLoanAmount = (v.gsis_loan) ? v.gsis_loan : 0;
								hasAbsent = (v.actual_absences) ? v.actual_absences : 0;
								otherDeduction = (v.total_otherdeduct) ? v.total_otherdeduct : 0;

								// additional tax Deduction
								addTaxDeduction = 0;
								if(v.deduction_transactions.length !== 0){
									$.each(v.deduction_transactions,function(k1,v1){
										switch(v1.deductions.code){
											case 'TA':
												addTaxDeduction = (v1.amount) ? v1.amount : 0;
												break;
										}
									})
								}

								totalTaxContAmount = parseFloat(taxContAmount) + parseFloat(taxContAmountTwo) + Number(addTaxDeduction);

								// ===== COMPUTATION =====
								pagibigContAmount = parseFloat(pagibigContAmount) + parseFloat(pagibigPersonal);

								totalPagibig = parseFloat(pagibigContAmount) + parseFloat(pagibig2Amount);

								totalPagibigAmount = parseFloat(totalPagibig) + parseFloat(pagibigLoanAmount);

								totalGsisAmount = parseFloat(gsisContAmount) + parseFloat(gsisLoanAmount);

								// if(hasAbsent){
									lwopPeraAmount = (v.benefit_transactions) ? v.benefit_transactions.amount : 0;
									lwopAmount = (v.total_absences_amount) ? v.total_absences_amount : 0;
									lessGrossBasicAmount = parseFloat(basicAmount) - parseFloat(lwopAmount);
									lessGrossPeraAmount = parseFloat(peraAmount) - parseFloat(lwopPeraAmount);
								// }

								grossPayAmount = (parseFloat(basicAmount) + parseFloat(peraAmount) - parseFloat(lwopAmount) - Number(lessGrossPeraAmount));

								totalDeductionAmount = (parseFloat(totalGsisAmount) + parseFloat(totalPagibigAmount) + parseFloat(totalTaxContAmount) + parseFloat(philhealthContAmount) + Number(otherDeduction));

								netAmount = (parseFloat(grossPayAmount) - parseFloat(totalDeductionAmount));

								// ===== COMPUTATION =====

								// ===== SUB TOTAL COMPUTATION =====

								subBasicAmount += parseFloat(basicAmount);
								subPeraAmount += parseFloat(peraAmount);
								subGsisContAmount += parseFloat(gsisContAmount);
								subPagibigContAmount += parseFloat(pagibigContAmount);
								subPhilhealthContAmount += parseFloat(philhealthContAmount);
								subPagibig2Amount += parseFloat(pagibig2Amount);
								subTaxContAmount += parseFloat(totalTaxContAmount);
								subLoanAmount += parseFloat(totalLoanAmount);
								subLwopAmount += parseFloat(lwopAmount);
								subGrossPayAmount += parseFloat(grossPayAmount);
								subTotalDeductionAmount += parseFloat(totalDeductionAmount);
								subTotalGsisAmount += parseFloat(totalGsisAmount);
								subNetAmount += parseFloat(netAmount);
								subLessGrossBasicAmount += parseFloat(lessGrossBasicAmount);
								subTotalPagibigAmount += parseFloat(totalPagibigAmount);
								subLWOPPeraAmount += parseFloat(lwopPeraAmount);
								subLessGrossPeraAmount += parseFloat(lessGrossPeraAmount);
								// ===== SUB TOTAL COMPUTATION =====

								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
								basic_salary_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '0.00';
								pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '0.00';
								gross_earned_amount = (grossPayAmount !== 0) ? commaSeparateNumber(parseFloat(grossPayAmount).toFixed(2)) : '0.00';
								gsis_cont_amount = (gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(gsisContAmount).toFixed(2)) : '0.00';
								pagibig2_amount = (pagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(pagibig2Amount).toFixed(2)) : '0.00';
								tax_cont_amount = (totalTaxContAmount !== 0) ? commaSeparateNumber(parseFloat(totalTaxContAmount).toFixed(2)) : '0.00';
								total_deduction_amount = (totalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(totalDeductionAmount).toFixed(2)) : '0.00';
								net_amount = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '0.00';
								lwop_amount = (lwopAmount !== 0) ? commaSeparateNumber(parseFloat(lwopAmount).toFixed(2)) : '0.00';
								philhealth_cont_amount = (philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(philhealthContAmount).toFixed(2)) : '0.00';
								pagibig_cont_amount = (pagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(pagibigContAmount).toFixed(2)) : '0.00';
								total_gsis = (totalGsisAmount !== 0) ? commaSeparateNumber(parseFloat(totalGsisAmount).toFixed(2)) : '0.00';
								total_pagibig = (totalPagibigAmount !== 0) ? commaSeparateNumber(parseFloat(totalPagibigAmount).toFixed(2)) : '0.00';
								less_gross_basic = (lessGrossBasicAmount !== 0) ? commaSeparateNumber(parseFloat(lessGrossBasicAmount).toFixed(2)) : '0.00';
								lwop_pera_amount = (lwopPeraAmount !== 0) ? commaSeparateNumber(parseFloat(lwopPeraAmount).toFixed(2)) : '0.00';
								less_gross_pera_amount = (lessGrossPeraAmount !== 0) ? commaSeparateNumber(parseFloat(lessGrossPeraAmount).toFixed(2)) : '0.00';
								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

								body += '<tr>';
								body += '<td>'+ctr+'</td>'
								body += '<td nowrap>'+fullname+'</td>'
								body += '<td nowrap>'+position+'</td>'
								body += '<td>'+assumption_date+'</td>'
								body += '<td  class="text-center">'+job_grade+'</td>'
								body += '<td class="text-right">'+basic_salary_amount+'</td>'
								body += '<td  class="text-right">'+pera_amount+'</td>'
								body += '<td class="text-right">'+lwop_amount+'</td>' // LWOP BASIC
								body += '<td  class="text-right">'+less_gross_pera_amount+'</td>' //LWOP PERA
								body += '<td class="text-right">'+less_gross_basic+'</td>' // GROSS LESS BASIC
								body += '<td class="text-right">'+lwop_pera_amount+'</td>' // GROSS PERA
								body += '<td  class="text-right">'+gross_earned_amount+'</td>' // GROSS PAY
								body += '<td  class="text-right">'+gsis_cont_amount+'</td>' // GSIS CONT

								if(data.gsisLoanList.length !== 0){
									$.each(data.gsisLoanList,function(k2,v2){
										loan = [];
										loan_dispay = [];
										loan_id = v2.loan_id;
										$.each(v.loaninfo_transaction,function(k3,v3){
											own_loan_id = v3.loan_id;
											loan_dispay['loan_amount_'+own_loan_id] = v3.amount;
										});

										if(loan_id){
											if (loan_dispay['loan_amount_'+loan_id]) {
												arr_subtotal_gsis[loan_id+'_id'] += parseFloat(loan_dispay['loan_amount_'+loan_id]);
											}
											loan_dispay['loan_amount_'+loan_id] = (loan_dispay['loan_amount_'+loan_id]) ? commaSeparateNumber(parseFloat(loan_dispay['loan_amount_'+loan_id]).toFixed(2)) : '0.00';
											body += '<td class="text-right" >'+loan_dispay['loan_amount_'+loan_id]+'</td>'
										}else{
											body += '<td class="text-right"></td>';
										}

									});
								}

								body += '<td class="text-right">'+total_gsis+'</td>'
								body += '<td class="text-right">'+philhealth_cont_amount+'</td>'
								body += '<td class="text-right">'+tax_cont_amount+'</td>'
								body += '<td class="text-right">'+pagibig_cont_amount+'</td>'
								body += '<td class="text-right">'+pagibig2_amount+'</td>'
								if(data.pagibigLoanList.length !== 0){
									$.each(data.pagibigLoanList,function(k2,v2){
										loan = [];
										loan_dispay = [];
										loan_id = v2.loan_id;

										$.each(v.loaninfo_transaction,function(k3,v3){
											own_loan_id = v3.loan_id;
											loan_dispay['loan_amount_'+own_loan_id] = v3.amount;
										});

										if(loan_id){
											if (loan_dispay['loan_amount_'+loan_id]) {
												arr_subtotal_pagibig[loan_id+'_id'] += parseFloat(loan_dispay['loan_amount_'+loan_id]);
											}
											loan_dispay['loan_amount_'+loan_id] = (loan_dispay['loan_amount_'+loan_id]) ? commaSeparateNumber(parseFloat(loan_dispay['loan_amount_'+loan_id]).toFixed(2)) : '0.00';
											body += '<td class="sub_total text-right">'+loan_dispay['loan_amount_'+loan_id]+'</td>'
										}else{
											body += '<td class="text-right"></td>'
										}
									});
								}

								body += '<td class="text-right">'+total_pagibig+'</td>';
								if(data.deductionList.length !== 0){
									$.each(data.deductionList,function(k2,v2){
										deduction = [];
										deduction_dispay = [];
										deduction_id = v2.deduction_id;

										$.each(v.deduction_transactions,function(k3,v3){
											own_deduction_id = v3.deduction_id;
											deduction_dispay['deduction_amount_'+own_deduction_id] = v3.amount;
										});

										if(deduction_id){
											if (deduction_dispay['deduction_amount_'+deduction_id]) {
												arr_subtotal_deduction[deduction_id+'_id'] += parseFloat(deduction_dispay['deduction_amount_'+deduction_id]);
											}

											deduction_dispay['deduction_amount_'+deduction_id] = (deduction_dispay['deduction_amount_'+deduction_id]) ? commaSeparateNumber(parseFloat(deduction_dispay['deduction_amount_'+deduction_id]).toFixed(2)) : '0.00';
											body += '<td class="sub_total text-right">'+deduction_dispay['deduction_amount_'+deduction_id]+'</td>'
										}else{
											body += '<td class="text-right"></td>'
										}
									});
								}
								body += '<td class="text-right">'+total_deduction_amount+'</td>'
								// body += '<td class="text-right"></td>'
								// body += '<td class="text-right"></td>'
								body += '<td class="text-right">'+net_amount+'</td>'
								body += '</tr>';


								ctr++;

							});
							// ======== BODY =======

							ctr = parseInt(ctr) - 1;

							netCtr += parseInt(ctr);
							// ===== COMPUTE NET AMOUNT  =====

							netBasicAmount += parseFloat(subBasicAmount);
							netPeraAmount += parseFloat(subPeraAmount);
							netGsisContAmount += parseFloat(subGsisContAmount);
							netPagibigContAmount += parseFloat(subPagibigContAmount);
							netPhilhealthContAmount += parseFloat(subPhilhealthContAmount);
							netPagibig2Amount += parseFloat(subPagibig2Amount);
							netTaxContAmount += parseFloat(subTaxContAmount);
							netLoanAmount += parseFloat(subLoanAmount);
							netLwopAmount += parseFloat(subLwopAmount);
							netGrossPayAmount += parseFloat(subGrossPayAmount);
							netTotalDeductionAmount += parseFloat(subTotalDeductionAmount);
							NetAmount += parseFloat(subNetAmount);
							netTotalGsisAmount += parseFloat(subTotalGsisAmount);
							netLessGrossBasicAmount += parseFloat(subLessGrossBasicAmount);
							netTotalPagibigAmount += parseFloat(subTotalPagibigAmount);
							netLWOPPeraAmount += parseFloat(subLWOPPeraAmount);
							netLessGrossPeraAmount += parseFloat(subLessGrossPeraAmount);

							// ===== COMPUTE NET AMOUNT =====

							sub_basic_salary_amount = (subBasicAmount !== 0) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '0.00';
							sub_pera_amount = (subPeraAmount !== 0) ? commaSeparateNumber(parseFloat(subPeraAmount).toFixed(2)) : '0.00';
							sub_gsis_cont_amount = (subGsisContAmount !== 0) ? commaSeparateNumber(parseFloat(subGsisContAmount).toFixed(2)) : '0.00';
							sub_pagibig_cont_amount = (subPagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(subPagibigContAmount).toFixed(2)) : '0.00';
							sub_philhealth_cont_amount = (subPhilhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(subPhilhealthContAmount).toFixed(2)) : '0.00';
							sub_pagibig2_amount = (subPagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(subPagibig2Amount).toFixed(2)) : '0.00';
							sub_tax_cont_amount = (subTaxContAmount !== 0) ? commaSeparateNumber(parseFloat(subTaxContAmount).toFixed(2)) : '0.00';
							sub_lwop_amount = (subLwopAmount !== 0) ? commaSeparateNumber(parseFloat(subLwopAmount).toFixed(2)) : '0.00';
							sub_gross_earned_amount = (subGrossPayAmount !== 0) ? commaSeparateNumber(parseFloat(subGrossPayAmount).toFixed(2)) : '0.00';
							sub_total_deduction_amount = (subTotalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalDeductionAmount).toFixed(2)) : '0.00';
							sub_total_gsis_amount = (subTotalGsisAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalGsisAmount).toFixed(2)) : '0.00';
							sub_net_amount = (subNetAmount !== 0) ? commaSeparateNumber(parseFloat(subNetAmount).toFixed(2)) : '0.00';
							sub_total_pagibig_amount = (subTotalPagibigAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalPagibigAmount).toFixed(2)) : '0.00';
							sub_lwop_pera_amount = (subLWOPPeraAmount !== 0) ? commaSeparateNumber(parseFloat(subLWOPPeraAmount).toFixed(2)) : '0.00';
							sub_less_gross_pera_amount = (subLessGrossPeraAmount !== 0) ? commaSeparateNumber(parseFloat(subLessGrossPeraAmount).toFixed(2)) : '0.00';
							sub_less_gross_basic_amount = (subLessGrossBasicAmount !== 0) ? commaSeparateNumber(parseFloat(subLessGrossBasicAmount).toFixed(2)) : '0.00';


							// ======= SUB TOTAL ======

							body += '<tr style="font-weight:bold;">';
							body += '<td class="text-center">'+ctr+'</td>'
							body += '<td></td>'
							body += '<td></td>'
							body += '<td class="text-center">SUB TOTAL</td>'
							body += '<td  class="text-center"></td>'
							body += '<td class="text-right">'+sub_basic_salary_amount+'</td>'
							body += '<td  class="text-right">'+sub_pera_amount+'</td>'
							body += '<td class="text-right">'+sub_lwop_amount+'</td>' // LWOP BASIC
							body += '<td  class="text-right">'+sub_less_gross_pera_amount+'</td>' //LWOP PERA
							body += '<td class="text-right">'+sub_less_gross_basic_amount+'</td>' // GROSS LESS BASIC
							body += '<td class="text-right">'+sub_lwop_pera_amount+'</td>' // GROSS PERA
							body += '<td  class="text-right">'+sub_gross_earned_amount+'</td>' // GROSS PAY
							body += '<td  class="text-right">'+sub_gsis_cont_amount+'</td>' // GSIS CONT

							if(data.gsisLoanList.length !== 0){
								for (var k in arr_gsis){
								    if (typeof arr_gsis[k] !== 'function') {
								    	arr_subtotal_gsis[arr_gsis[k]+'_id'] = (arr_subtotal_gsis[arr_gsis[k]+'_id'] !== 0) ? commaSeparateNumber(parseFloat(arr_subtotal_gsis[arr_gsis[k]+'_id']).toFixed(2)) : '0.00';
								         body += '<td  class="text-right">'+arr_subtotal_gsis[arr_gsis[k]+'_id']+'</td>';
								         arr_subtotal_gsis[arr_gsis[k]+'_id'] = 0;
								    }
								}
							}

							body += '<td class="text-right">'+sub_total_gsis_amount+'</td>'
							body += '<td class="text-right">'+sub_philhealth_cont_amount+'</td>'
							body += '<td class="text-right">'+sub_tax_cont_amount+'</td>'
							body += '<td class="text-right">'+sub_pagibig_cont_amount+'</td>'
							body += '<td class="text-right">'+sub_pagibig2_amount+'</td>'
							if(data.pagibigLoanList.length !== 0){
								for (var k in arr_pagibig){
								    if (typeof arr_pagibig[k] !== 'function') {
								    	arr_subtotal_pagibig[arr_pagibig[k]+'_id'] = (arr_subtotal_pagibig[arr_pagibig[k]+'_id']) ? commaSeparateNumber(parseFloat(arr_subtotal_pagibig[arr_pagibig[k]+'_id']).toFixed(2)) : '0.00';
								        body += '<td  class="text-right">'+arr_subtotal_pagibig[arr_pagibig[k]+'_id']+'</td>';
								        arr_subtotal_pagibig[arr_pagibig[k]+'_id'] = 0;
								    }
								}
							}
							body += '<td class="text-right">'+sub_total_pagibig_amount+'</td>'
							if(data.deductionList.length !== 0){
								for (var k in arr_deduction){
								    if (typeof arr_deduction[k] !== 'function') {
								    	arr_subtotal_deduction[arr_deduction[k]+'_id'] = (arr_subtotal_deduction[arr_deduction[k]+'_id']) ? commaSeparateNumber(parseFloat(arr_subtotal_deduction[arr_deduction[k]+'_id']).toFixed(2)) : '0.00';
								        body += '<td  class="text-right">'+arr_subtotal_deduction[arr_deduction[k]+'_id']+'</td>';
								        arr_subtotal_deduction[arr_deduction[k]+'_id'] = 0;
								    }
								}
							}
							body += '<td class="text-right">'+sub_total_deduction_amount+'</td>'
							// body += '<td class="text-right"></td>'
							// body += '<td class="text-right"></td>'
							body += '<td class="text-right">'+sub_net_amount+'</td>'
							body += '</tr>';



							// ======= SUB TOTAL ======
						ctr = 1;

						});


						net_basic_salary_amount = (netBasicAmount !== 0) ? commaSeparateNumber(parseFloat(netBasicAmount).toFixed(2)) : '0.00';
						net_pera_amount = (netPeraAmount !== 0) ? commaSeparateNumber(parseFloat(netPeraAmount).toFixed(2)) : '0.00';
						net_gsis_cont_amount = (netGsisContAmount !== 0) ? commaSeparateNumber(parseFloat(netGsisContAmount).toFixed(2)) : '0.00';
						net_pagibig_cont_amount = (netPagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(netPagibigContAmount).toFixed(2)) : '0.00';
						net_philhealth_cont_amount = (netPhilhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(netPhilhealthContAmount).toFixed(2)) : '0.00';
						net_pagibig2_amount = (netPagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(netPagibig2Amount).toFixed(2)) : '0.00';
						net_tax_cont_amount = (netTaxContAmount !== 0) ? commaSeparateNumber(parseFloat(netTaxContAmount).toFixed(2)) : '0.00';
						net_lwop_amount = (netLwopAmount !== 0) ? commaSeparateNumber(parseFloat(netLwopAmount).toFixed(2)) : '0.00';
						net_gross_earned_amount = (netGrossPayAmount !== 0) ? commaSeparateNumber(parseFloat(netGrossPayAmount).toFixed(2)) : '0.00';
						net_total_deduction_amount = (netTotalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalDeductionAmount).toFixed(2)) : '0.00';
						net_total_gsis_amount = (netTotalGsisAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalGsisAmount).toFixed(2)) : '0.00';
						net_net_amount = (NetAmount !== 0) ? commaSeparateNumber(parseFloat(NetAmount).toFixed(2)) : '0.00';
						net_total_pagibig_amount = (netTotalPagibigAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalPagibigAmount).toFixed(2)) : '0.00';
						net_lwop_pera_amount = (netLWOPPeraAmount !== 0) ? commaSeparateNumber(parseFloat(netLWOPPeraAmount).toFixed(2)) : '0.00';
						net_less_pera_amount = (netLessGrossPeraAmount !== 0) ? commaSeparateNumber(parseFloat(netLessGrossPeraAmount).toFixed(2)) : '0.00';
						net_less_basic_amount = (netLessGrossBasicAmount !== 0) ? commaSeparateNumber(parseFloat(netLessGrossBasicAmount).toFixed(2)) : '0.00';

						// // ======= GRAND TOTAL ======
						body += '<tr  style="font-weight:bold;" class="style-td">';
						body += '<td class="text-center">'+netCtr+'</td>'
						body += '<td>Total Employees</td>'
						body += '<td></td>'
						body += '<td class="text-center">GRAND TOTAL</td>'
						body += '<td  class="text-center"></td>'
						body += '<td class="text-right">'+net_basic_salary_amount+'</td>'
						body += '<td  class="text-right">'+net_pera_amount+'</td>'
						body += '<td class="text-right">'+net_lwop_amount+'</td>' // LWOP BASIC
						body += '<td  class="text-right">'+net_less_pera_amount+'</td>' //LWOP PERA
						body += '<td class="text-right">'+net_less_basic_amount+'</td>' // GROSS LESS BASIC
						body += '<td class="text-right">'+net_lwop_pera_amount+'</td>' // GROSS PERA
						body += '<td  class="text-right">'+net_gross_earned_amount+'</td>' // GROSS PAY
						body += '<td  class="text-right">'+net_gsis_cont_amount+'</td>' // GSIS CONT

						if(data.gsisLoanList.length !== 0){
							loanId = 0;
							$.each(data.gsisLoanList,function(k,v){
								body += '<td class="text-right">'+commaSeparateNumber(parseFloat(v.net_amount).toFixed(2))+'</td>';
							});
						}
						body += '<td class="text-right">'+net_total_gsis_amount+'</td>'
						body += '<td class="text-right">'+net_philhealth_cont_amount+'</td>'
						body += '<td class="text-right">'+net_tax_cont_amount+'</td>'
						body += '<td class="text-right">'+net_pagibig_cont_amount+'</td>'
						body += '<td class="text-right">'+net_pagibig2_amount+'</td>'
						if(data.pagibigLoanList.length !== 0){
							$.each(data.pagibigLoanList,function(k2,v2){
								loan = [];
								loan_dispay = [];
								loan_id = v2.loan_id;

								body += '<td class="text-right">'+commaSeparateNumber(parseFloat(v2.net_amount).toFixed(2))+'</td>';
							});
						}
						body += '<td class="text-right">'+net_total_pagibig_amount+'</td>'
						if(data.deductionList.length !== 0){
							$.each(data.deductionList,function(k2,v2){
								deduction = [];
								deduction_dispay = [];
								deduction_id = v2.deduction_id;

								body += '<td class="text-right">'+commaSeparateNumber(parseFloat(v2.net_amount).toFixed(2))+'</td>';
							});
						}
						body += '<td class="text-right">'+net_total_deduction_amount+'</td>'
						// body += '<td class="text-right"></td>'
						// body += '<td class="text-right"></td>'
						body += '<td class="text-right">'+net_net_amount+'</td>'
						body += '</tr>';
						// ======= GRAND TOTAL ======

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';
						signatoryFour = (signatoryFour) ? signatoryFour : '';
						positionFour = (positionFour) ? positionFour : '';

						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						addDegree = (addDegree) ? addDegree : '';

						body +=	'<tr class="text-left borderless" >';
						body +=	'<td style="border:none;" colspan="'+totalCol+'">'
						body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:20px;">';
						body 	+= '<div class="col-md-3">';
						body 	+=	'<b>Certified:</b> Services rendered per submitted <br>';
						body 	+=	'Daily Time Records approved by their <br>';
						body 	+=	'Directors/Supervisors.';
						body 	+= '</div>';
						body 	+= '<div class="col-md-3">';
						body 	+= '</div>';
						body 	+= '<div class="col-md-3">';
						body 		+=	'<b>Certified:</b> Supporting documents complete and <br>';
						body 		+=	'proper, and cash available in the amount of<br>';
						body 		+=	'Php _________________________.';
						body 	+= '</div>';
						body 	+= '<div class="col-md-3">';
						body 	+=	'<span>Approved for Payment:</span>';
						body 	+= '</div>';
						body += '</div>';
						body 	+= '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:20px;">';
						body 	+= '<div class="col-md-3">';
						body 	+=	'<span><b>'+signatoryOne+'</b><br>'+positionOne+' </span>';
						body 	+= '</div>';
						body 	+= '<div class="col-md-3">';
						body 	+=	'<span><b>'+signatoryTwo+'</b><br>'+positionTwo+'</span>';
						body 	+= '</div>';
						body 	+= '<div class="col-md-3">';
						body 	+=	'<span><b>'+signatoryThree+'</b><br>'+positionThree+'</span>';
						body 	+= '</div>';
						body 	+= '<div class="col-md-3">';
						body 	+=	'<span><b>'+signatoryFour+' '+addDegree+'</b><br>'+positionFour+'</span>';
						body 	+= '</div>';
						body 	+= '</div>';
						body 	+= '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:20px;">';
						body 	+= '<div class="col-md-12">';
						body 	+= '<i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
						body 	+= '</div>';
						body 	+= '</div>';
						body += '</td>';
						body += '</tr>';
						body += '</tbody></table>';
						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
});

})
</script>
@endsection