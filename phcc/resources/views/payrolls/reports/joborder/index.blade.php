@extends('app-reports')


@section('reports-content')
<head>
<style type="text/css">
.table>tbody>tr>td{
	line-height: 0.428571;
}
</style>
</head>

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td ><span><b>Employee Name</b></span></td>
		</tr>
		<tr>
			<td>
				<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
					<option value=""></option>
					@foreach($employeeinfo as $value)
					<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg" id="post">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 700px;">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports">
	       		<div class="row">
	       			<div class="col-md-12">
	       				<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
	       				<h5><b>JO INDIVIDUAL SALARY REPORT</b></h5>
	       				<div style="padding-left: 20px;">
	       					<div class="row">
		       					<div class="col-md-3">
		       						Name of Employee
		       					</div>
		       					<div class="col-md-1"style="width: 25px;">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-4" >
		       						<b><span id="employee_name"></span></b>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						Regular Salary
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						<span style="padding-left: 20px;">Daily Rate</span>
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="daily_rate_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						<span style="padding-left: 20px;">Hourly Rate</span>
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="hourly_rate_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						Hour Overtime
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-3">
		       						Rate
		       					</div>
		       					<div class="col-md-3">
		       						Amount
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						<span style="padding-left: 20px;">Regular</span>
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="regular_overtime_rate">125%</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="regular_overtime_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						<span style="padding-left: 20px;">Weekend/Holiday</span>
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="weekend_overtime_rate">150%</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="weekeend_overtime_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						LBP S/A #
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2">
		       						<span id="land_bank_account_number"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						EW/Tax
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span id="ewtax"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						PW/Tax
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span id="pwtax"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						TIN
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right" >
		       						<span id="tax_id_number"></span>
		       					</div>
	       					</div>
	       					<div class="row" style="padding:30px 0px 30px 0px">
	       						<span style="border-bottom: 2px solid #000;font-weight: bold">SALARY FOR THE PERIOD COVERING <span id="period_cover_date"></span></span> 
	       						
	       					</div>
	       					<div class="row">
	       						<div class="col-md-6 text-right">
	       							<b>Days/Hrs Worked</b>
	       						</div>
	       						<div class="col-md-2 text-right">
	       							<b>Amount</b>
	       						</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						BASIC
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span id="number_of_working_days"></span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span id="basic_pay"></span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						(LESS)
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-8">
		       					</div>
		       					<div class="col-md-2 text-right">
		       						EW/TAX<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-left">
		       						<span id="ewtax_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row">
	       						<div class="col-md-8">
	       							Less: Late/UT (hours)
		       					</div>
		       					<div class="col-md-2 text-right">
		       						PW/TAX<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-left">
		       						<span id="pwtax_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						<span style="padding-left: 20px;">Hours</span>
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-3 text-right">
		       						<span style="padding-left:20px; " id="actual_undertime"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						<span style="padding-left: 40px;">Conversions</span>
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="total_diff"></span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="total_diff_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row" style="padding:30px 0px 0px 0px">
	       						<span>Add: Overtime for the month: </span>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						<span style="padding-left: 20px;">Regular</span>
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						<span style="padding-left:20px; " id="overtime_regular"></span>
		       					</div>
		       					<div class="col-md-2 text-right" >
		       						<span style="padding-left:20px; " id="overtime_regular_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row">
		       					<div class="col-md-3">
		       						<span style="padding-left: 20px;">Weekend/Holiday</span>
		       					</div>
		       					<div class="col-md-1">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right " style="width: 17%;margin-right: 5px;border-bottom:1px solid #ababab;">
		       						<span style="padding-left:20px; " id="total_special_and_weekend"></span>
		       					</div>
		       					<div class="col-md-2 text-right" style="width: 17%;border-bottom:1px solid #ababab;">
		       						<span style="padding-left:20px; " id="total_special_and_weekend_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row" style="padding-top: 20px;">
		       					<div class="col-md-3">
		       						GROSS PAY
		       					</div>
		       					<div class="col-md-3">
		       						<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right" >
		       						<span id="gross_pay_amount" ></span>
		       					</div>
		       					<div class="col-md-2 text-right">
		       						TOTAL TAX	<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-right" style="width: 10%;border-top: 1px solid #ababab;">
		       						<span  id="total_tax_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row" style="padding-top: 20px;">
		       					<div class="col-md-8">
		       					</div>
		       					<div class="col-md-2 text-right">
		       						NET PAY	<span style="font-weight: bold;">:</span>
		       					</div>
		       					<div class="col-md-2 text-left" style="font-weight: bold">
		       						<span  id="net_pay_amount"></span>
		       					</div>
	       					</div>
	       					<div class="row">
	       						<div class="col-md-12">
	       							<b>Attachments:</b>	 <br>
									1. DTR for the period <br>
									2. Accomplishment Report <br>
									3. Trip Ticket <br>
									4. Authority to Render OT <br><br>
									Certified Correct
	       						</div>
	       					</div>
	       					<div class="row" style="padding-top: 20px;">
		       					<div class="col-md-6">
		       						<b>RIMEL D. EVARISTO</b> <br>
		       						HRMO III, HRDD
		       					</div>
		       					<div class="col-md-6">
		       						<b>ANTONIA LYNNELY L. BAUTISTA</b> <br>
		       						Chief Admin Officer, HRDD
		       					</div>
	       					</div>
	       				</div>
	       				<br><br>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
}
$('#select_month').html(mArr);

// ************************************************
var _Year;
var _Month;
var _empid;
var _searchvalue;
var _emp_status;
var _emp_type;
var _searchby;
var _monthNumber;
$('.select2').select2();

$('#select_year').select2({
	allowClear:true,
    placeholder: "Year",
});

$('#select_month').select2({
	allowClear:true,
    placeholder: "Month",
});

$(document).on('change','#select_year',function(){
	_Year = $(this).find(':selected').val();

})
$(document).on('change','#select_month',function(){
	_Month = $(this).find(':selected').data('month');
	_monthNumber = $(this).find(':selected').val();


})
$(document).on('change','#employee_id',function(){
	_empid = "";
	_empid = $(this).find(':selected').val();

})

var _payPeriod;
var _semiPayPeriod;
$(document).on('change','#pay_period',function(){
	_payPeriod = $(this).find(':selected').val();
	switch(_payPeriod){
		case 'semimonthly':
			$('#semi_pay_period').removeClass('hidden');
		break;
		default:
			$('#semi_pay_period').addClass('hidden');
		break;
	}
});

$(document).on('change','#semi_pay_period',function(){
	_semiPayPeriod = $(this).find(':selected').val();
})



$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month || !_empid){
		swal({
			  title: "Select Year, Month and Employee First!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{

		switch(_payPeriod){
			case 'monthly':
				displayReport();
			break;
			case 'semimonthly':
			console.log(_semiPayPeriod);
				if(_semiPayPeriod){
					displayReport();
				}else{
					swal({
					  title: "Select First Half or Second Half!",
					  type: "warning",
					  showCancelButton: false,
					  confirmButtonClass: "btn-danger",
					  confirmButtonText: "Yes",
					  closeOnConfirm: false

					});
				}
			break;
			default:
				swal({
				  title: "Select Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

				});
			break;
		}

	}

});

function displayReport(){
	$.ajax({
		url:base_url+module_prefix+module+'/getJobOrder',
		data:{
			'id':_empid,
			'year':year,
			'month':month,
			'emp_type':emp_type,
			'emp_status':emp_status,
			'pay_period':_payPeriod,
			'sub_pay_period':_semiPayPeriod,
			'category':category,
			'searchby':searchby,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			console.log(data)
			if(data.transaction !== null){
				fullname = data.transaction.employees.lastname+' '+data.transaction.employees.firstname+' '+data.transaction.employees.middlename;
				$('#employee_name').text(fullname);

				daily_rate = (data.employeeinfo.daily_rate_amount) ? data.employeeinfo.daily_rate_amount : 0;
				hourly_rate_amount = (daily_rate/8);

				monthly_rate = (data.transaction.actual_basicpay) ? data.transaction.actual_basicpay : 0;
				number_of_working_days = data.attendanceinfo.actual_workdays;

				regular_overtime_amount = (hourly_rate_amount) ? (hourly_rate_amount*1.25) : 0;
				weekeend_overtime_amount = (hourly_rate_amount) ? (hourly_rate_amount*1.50) : 0;

				$('#number_of_working_days').text(number_of_working_days);

				percentage_one = 0;
				if(data.employeeinfo.taxpolicy_id !==  null){
					tax_policy_one = data.employeeinfo.taxpolicy_one.policy_name.split(' ');

					switch(tax_policy_one[0]){
						case 'EWT':
							$('#ewtax').text(tax_policy_one[1]+' %');
							percentage_one = (tax_policy_one[1]) ? tax_policy_one[1] : ' ';
							$('#ewtax_percentage').text(percentage_one);
						break;
						case 'PWT':
							$('#pwtax').text(tax_policy_one[1]+' %');
							percentage_one = (tax_policy_one[1]) ? tax_policy_one[1] : ' ';
							$('#pwtax_percentage').text(percentage_one);
						break;
					}
				}
				percentage_two = 0;
				if(data.employeeinfo.taxpolicy_two !==  null){
					tax_policy_two = data.employeeinfo.taxpolicy_two.policy_name.split(' ');

					switch(tax_policy_two[0]){
						case 'EWT':
							$('#ewtax').text(tax_policy_two[1]+' %');
							percentage_two = (tax_policy_two[1]) ? tax_policy_two[1] : ' ';
							$('#ewtax_percentage').text(percentage_two);
						break;
						case 'PWT':
							$('#pwtax').text(tax_policy_two[1]+' %');
							percentage_two = (tax_policy_two[1]) ? tax_policy_two[1] : ' ';
							$('#pwtax_percentage').text(percentage_two);
						break;
					}
				}
				overtime_special_amount = 0;
				overtime_regular = 0;
				overtime_special = 0;
				total_special_and_weekend = 0;
				overtime_regular_amount = 0;
				overtime_weekend_amount = 0;
				overtime_special_amount = 0;
				total_special_and_weekend_amount = 0;
				if(data.overtime !== null){
					overtime_regular = (data.overtime.actual_regular_overtime !== null) ? data.overtime.actual_regular_overtime : 0;
					overtime_weekend = (data.overtime.actual_regular_holiday_overtime !== null) ? data.overtime.actual_regular_holiday_overtime : 0;
					overtime_special = (data.overtime.actual_special_overtime !== null) ? data.overtime.actual_special_overtime : 0;

					total_special_and_weekend = (parseInt(overtime_weekend) + parseInt(overtime_special));

					overtime_regular_amount = (data.overtime.total_regular_amount !== null) ? data.overtime.total_regular_amount : 0;
					overtime_weekend_amount = (data.overtime.total_regular_holiday_amount !== null) ? data.overtime.total_regular_holiday_amount : 0;
					overtime_special_amount = (data.overtime.total_special_amount !== null) ? data.overtime.total_special_amount : 0;

					total_special_and_weekend_amount = (parseFloat(overtime_weekend_amount) + parseFloat(overtime_special_amount));

				}

				lwop 	  = (data.attendanceinfo !== null) ? data.attendanceinfo.actual_absence/60 : 0;
				undertime = (data.attendanceinfo.actual_undertime !== null) ? data.attendanceinfo.actual_undertime : 0;
				tardines  = (data.attendanceinfo.actual_tardines !== null) ? data.attendanceinfo.actual_tardines : 0;

				lwop_amount 	 = (data.attendanceinfo !== null) ? data.attendanceinfo.total_absence : 0;
				undertime_amount = (data.attendanceinfo !== null) ? data.attendanceinfo.total_undertime : 0;
				tardines_amount = (data.attendanceinfo !== null) ? data.attendanceinfo.total_tardines : 0;

				total_diff = (parseInt(undertime) + parseInt(tardines) + parseInt(lwop));
				total_diff_amount = (parseFloat(undertime_amount) + parseFloat(tardines_amount) + parseFloat(lwop_amount));

				gross_pay_amount = (parseFloat(monthly_rate) + parseFloat(overtime_regular_amount) + parseFloat(overtime_special_amount)) - parseFloat(total_diff_amount);

				tax_amount_one = (gross_pay_amount*(percentage_one/100));
				tax_amount_two = (gross_pay_amount*(percentage_two/100));

				tax_amount_one = (tax_amount_one) ? tax_amount_one : 0;
				tax_amount_two = (tax_amount_two) ? tax_amount_two : 0;

				// --SUM OF TWO TAXES
				total_tax_amount = (parseFloat(tax_amount_one) + parseFloat(tax_amount_two));
				// --DIFF OF MONTHLY RATE AND TWO TAXES

				net_pay_amount = (gross_pay_amount - total_tax_amount);

				daily_rate = (daily_rate !== 0) ? commaSeparateNumber(parseFloat(daily_rate).toFixed(2)) : '';
				hourly_rate_amount = (hourly_rate_amount !== 0) ? commaSeparateNumber(parseFloat(hourly_rate_amount).toFixed(2)) : '';
				regular_overtime_amount = (regular_overtime_amount !== 0) ? commaSeparateNumber(parseFloat(regular_overtime_amount).toFixed(2)) : '';
				weekeend_overtime_amount = (weekeend_overtime_amount !== 0) ? commaSeparateNumber(parseFloat(weekeend_overtime_amount).toFixed(2)) : '';
				total_diff = (total_diff !== 0) ? total_diff : '';
				total_diff_amount = (total_diff_amount !== 0) ? commaSeparateNumber(parseFloat(total_diff_amount).toFixed(2)) : '';

				total_tax_amount = (total_tax_amount) ? commaSeparateNumber(parseFloat(total_tax_amount).toFixed(2)) : '';
				net_pay_amount = (net_pay_amount) ? commaSeparateNumber(parseFloat(net_pay_amount).toFixed(2)) : '';
				tax_amount_one = (tax_amount_one) ? commaSeparateNumber(parseFloat(tax_amount_one).toFixed(2)) : '';
				tax_amount_two = (tax_amount_two) ? commaSeparateNumber(parseFloat(tax_amount_two).toFixed(2)) : '';
				monthly_rate = (monthly_rate) ? commaSeparateNumber(parseFloat(monthly_rate).toFixed(2)) : '';
				overtime_regular = (overtime_regular) ? commaSeparateNumber(parseFloat(overtime_regular).toFixed(2)) : '';
				overtime_regular_amount = (overtime_regular_amount) ? commaSeparateNumber(parseFloat(overtime_regular_amount).toFixed(2)) : '';
				total_special_and_weekend = (total_special_and_weekend) ? commaSeparateNumber(parseFloat(total_special_and_weekend).toFixed(2)) : '';
				total_special_and_weekend_amount = (total_special_and_weekend_amount) ? commaSeparateNumber(parseFloat(total_special_and_weekend_amount).toFixed(2)) : '';
				gross_pay_amount = (gross_pay_amount) ? commaSeparateNumber(parseFloat(gross_pay_amount).toFixed(2)) : '';
				$('#daily_rate_amount').text(daily_rate);
				$('#hourly_rate_amount').text(hourly_rate_amount);
				$('#regular_overtime_amount').text(regular_overtime_amount);
				$('#weekeend_overtime_amount').text(weekeend_overtime_amount);
				$('#basic_pay').text(monthly_rate);
				$('#total_diff').text(total_diff);
				$('#total_diff_amount').text(total_diff_amount)
				$('#total_tax_amount').text(total_tax_amount);
				$('#monthly_rate').text(monthly_rate);
				$('#ewtax_amount').text(tax_amount_one);
				$('#pwtax_amount').text(tax_amount_two);
				$('#overtime_regular').text(overtime_regular);
				$('#overtime_regular_amount').text(overtime_regular_amount);
				$('#total_special_and_weekend').text(total_special_and_weekend);
				$('#total_special_and_weekend_amount').text(total_special_and_weekend_amount);
				$('#gross_pay_amount').text(gross_pay_amount);
				$('#net_pay_amount').text(net_pay_amount);
				$('#bank_account_number').text(data.employeeinfo.atm_no);

				days = daysInMonth(_monthNumber,_Year)

				if(_payPeriod == 'monthly'){
					_coveredPeriod = _Month+' 1-'+days+', '+_Year;
				}else{
					switch(_semiPayPeriod){
						case 'firsthalf':
							_coveredPeriod = _Month+' 1-15, '+_Year;
						break;
						default:
							_coveredPeriod =_Month+' 16-'+days+', '+_Year;
						break;
					}
				}
				$('#period_cover_date').text(_coveredPeriod);
				$('#btnModal').trigger('click');

			}else{
				swal({
					title: "No Records Found",
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
			}
		}
	})
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

$(document).on('click','#print',function(){
	$('#reports').printThis();
});
})
</script>
@endsection