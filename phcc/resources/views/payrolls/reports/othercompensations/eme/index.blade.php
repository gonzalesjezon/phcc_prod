@extends('app-othercompensations')

@section('othercompensations-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
.table2>thead>tr>td, .table2>tbody>tr>td{
    padding: 3px !important;
 }
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<div class="panel panel-default" style="padding: 15px;">
		<div class="panel-body">
			@include('payrolls.reports.includes._table-posted')
			<form action="{{ url($module_prefix.'/'.$module) }}" method="post" id="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="signatory_one" id="signatory_one">
				<input type="hidden" name="signatory_two" id="signatory_two">
				<input type="hidden" name="signatory_three" id="signatory_three">
				<input type="hidden" name="signatory_four" id="signatory_four">
				<input type="hidden" name="signatory_five" id="signatory_five">
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_one">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_one" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid One</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_two">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_two" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid Two</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_three">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_three" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Top Right One</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_four">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_four" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<span>Degree</span>
							<input type="text" name="add_degree" id="add_degree" class="form-control font-style2">
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right Two</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_five">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_five" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<br>
				@include('payrolls.reports.includes._post-button')
			</form>
			@include('payrolls.reports.includes._csv-button')
		</div>
	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
@include('payrolls.reports.includes._post-button-script')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$('#select_year').trigger('change');
	$('#select_month').trigger('change');

	var signatoryOne;
	var positionOne;
	$('#select_signatory_one').change(function(){
		signatoryOne = "";
		positionOne  = "";
		signatoryOne = $(this).find(':selected').text()
		positionOne = $(this).find(':selected').data('position')

	});

	var signatoryTwo;
	var positionTwo;
	$('#select_signatory_two').change(function(){
		signatoryTwo = "";
		positionTwo  = "";
		signatoryTwo = $(this).find(':selected').text()
		positionTwo = $(this).find(':selected').data('position')
	});

	var signatoryThree;
	var positionThree;
	$('#select_signatory_three').change(function(){
		signatoryThree = "";
		positionThree  = "";
		signatoryThree = $(this).find(':selected').text()
		positionThree = $(this).find(':selected').data('position')
	});

	var signatoryFour;
	var positionFour;
	$('#select_signatory_four').change(function(){
		signatoryFour = "";
		positionFour  = "";
		signatoryFour = $(this).find(':selected').text()
		positionFour = $(this).find(':selected').data('position')
	});

	var signatoryFive;
	var positionFive;
	$('#select_signatory_five').change(function(){
		signatoryFive = "";
		positionFive  = "";
		signatoryFive = $(this).find(':selected').text()
		positionFive = $(this).find(':selected').data('position')
	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	$('.signatory').change(function(){
		id 			= $(this).find(':selected').data('id');
		position 	= $(this).find(':selected').data('position');
		text 		= $(this).find(':selected').text();

		$('#'+id).val(text+'|'+position);
	});

	var addDegree;
	$('#add_degree').on('change',function(){
		addDegree = $(this).val();
	});

	$('.view').on('click',function(){
		_Year 		= $(this).data('year');
		_Month 		= $(this).data('month');
		signOne 	= $(this).data('signatory_one').split('|');
		signTwo 	= $(this).data('signatory_two').split('|');
		signThree 	= $(this).data('signatory_three').split('|');
		signFour 	= $(this).data('signatory_four').split('|');
		signFive 	= $(this).data('signatory_five').split('|');
		addDegree 	= $(this).data('degree');

		signatoryOne 	= signOne[0];
		positionOne 	= signOne[1];

		signatoryTwo 	= signTwo[0];
		positionTwo 	= signTwo[1];

		signatoryThree 	= signThree[0];
		positionThree 	= signThree[1];

		signatoryFour 	= signFour[0];
		positionFour 	= signFour[1];

		signatoryFive 	= signFive[0];
		positionFive 	= signFive[1];

		$('#preview').trigger('click');
	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						netCtr = 0;
						netEEAmount = 0;
						netMEAmount = 0;
						netEMEAmount = 0;
						coveredPeriod = months[_Month]+' '+_Year;


						body += '<table class="table table2" style="border:none;">';
						body += '<thead class="text-center">';
						body += '<tr>';
						body += '<td class="text-center" colspan="9" style="border-left:none;border-right:none;font-weight:bold;">PHILIPPINE COMPETITION COMMISSION <br> GENERAL PAYROLL FOR EXTRAORDINARY AND MISCELLANEOUS EXPENSES </td>';
						body += '</tr>';
						body +=	'<tr ><td ><b >#</b></td>'
						body +=	'<td ><b >EMPLOYEE NAME</b></td>';
						body +=	'<td ><b >POSITION/DESIGNATION</b></td>';
						body +=	'<td ><b>OFFICE</b></td>';
						body +=	'<td ><b>EXTRAORDINARY EXPENSES</b></td>';
						body +=	'<td ><b>MISCELLANEOUS EXPENSE</b></td>';
						body +=	'<td ><b>TOTAL</b></td>';
						body +=	'<td ><b >SIGNATURE</b></td>';
						body +=	'<td ><b>REMARKS</b></td></tr>';
						body += '</thead>';

						body += '<tbody>';

						body += '<tr>';
						body += '<td colspan="9">';
						body += 'Pursuant to COA Circular No. 89-300 dated March 21, 1989, it is hereby CERTIFY to have incurred the total amount provided chargeable against Extraordinary and Miscellaneous Expenses	for the month of <b>'+coveredPeriod+'</b>, in connection with and for the purpose on activities completed, authorized under Sec. 43 of GAA, FY 2018 particularly with respect to meetings and conferences, official entertainment, public relations, education and cultural activities, and other similar expenses not supported by the regular budget allocation, respectively, in relation to, or by reason of our respective positions as supported by the attached Certificates issued by the concerned Officers';
						body += '</td>';
						body += '</tr>';
						$.each(data.transaction,function(key,val){

							subEEAmount = 0;
							subMEAmount = 0;
							subEMEAmount = 0;

							body += '<tr>';
							body += '<td colspan="9" style="font-weight:bold;">'+key+'</td>';
							body += '</tr>';
							$.each(val,function(k,v){
								var eeAmount = 0;
								var meAmount = 0;

								firstname = (v.employees) ? v.employees.firstname : '';
								lastname = (v.employees) ? v.employees.lastname : '';
								middlename = (v.employees) ? v.employees.middlename : '';
								jobgrade = (v.salaryinfo) ? v.salaryinfo.jobgrade.Code : '';
								position = (v.positions) ? v.positions.Name : '';
								office = (v.offices) ? v.offices.Name : '';
								remarks = (v.special_remarks) ? v.special_remarks : '';
								eeAmount = (v.amount) ? v.amount : 0;
								meAmount = (v.me_amount) ? v.me_amount : 0;

								fullname = lastname+' '+firstname+' '+middlename;

								totalEMEAmount = parseFloat(eeAmount) + parseFloat(meAmount);

								subEEAmount += parseFloat(eeAmount);
								subMEAmount += parseFloat(meAmount);
								subEMEAmount += parseFloat(totalEMEAmount);

								ee_amount = (eeAmount !== 0) ? commaSeparateNumber(parseFloat(eeAmount).toFixed(2)) : '0.00';
								me_amount = (meAmount !== 0) ? commaSeparateNumber(parseFloat(meAmount).toFixed(2)) : '0.00';
								total_amount = (totalEMEAmount !== 0) ? commaSeparateNumber(parseFloat(totalEMEAmount).toFixed(2)) : '0.00';
								body += '<tr>';
								body += '<td>'+ctr+'</td>';
								body += '<td  nowrap>'+fullname+'</td>';
								// body += '<td  nowrap>'+jobgrade+'</td>';
								body += '<td nowrap>'+position+'</td>';
								body += '<td nowrap>'+office+'</td>';
								body += '<td class="text-right">'+ee_amount+'</td>';
								body += '<td class="text-right">'+me_amount+'</td>';
								body += '<td class="text-right">'+total_amount+'</td>';
								body += '<td><div style="width:150px;">&nbsp;</div></td>';
								body += '<td>'+remarks+'</td>';
								body += '</tr>';
								ctr++;
							});

							ctr = parseInt(ctr) - 1;
							netCtr += parseInt(ctr);

							netEEAmount += parseFloat(subEEAmount);
							netMEAmount += parseFloat(subMEAmount);
							netEMEAmount += parseFloat(subEMEAmount);

							sub_ee_amount = (subEEAmount !== 0) ? commaSeparateNumber(parseFloat(subEEAmount).toFixed(2)) : '0.00';
							sub_me_amount = (subMEAmount !== 0) ? commaSeparateNumber(parseFloat(subMEAmount).toFixed(2)) : '0.00';
							sub_total_amount = (subEMEAmount !== 0) ? commaSeparateNumber(parseFloat(subEMEAmount).toFixed(2)) : '0.00';

							body += '<tr style="border:1px solid #c0c0c0;font-weight:bold;">';
							body += '<td class="text-center">'+ctr+'</td>';
							body += '<td></td>';
							body += '<td></td>';
							body += '<td style="font-weight:bold;">SUB TOTAL</td>';
							body += '<td class="text-right">'+sub_ee_amount+'</td>';
							body += '<td class="text-right">'+sub_me_amount+'</td>';
							body += '<td class="text-right">'+sub_total_amount+'</td>';
							body += '<td></td>';
							body += '<td></td>';
							body += '</tr>';
							ctr = 1;
						});

						net_ee_amount = (netEEAmount) ? commaSeparateNumber(parseFloat(netEEAmount).toFixed(2)) : '0.00';
						net_me_amount = (netMEAmount) ? commaSeparateNumber(parseFloat(netMEAmount).toFixed(2)) : '0.00';
						net_total 	  = (netEMEAmount) ? commaSeparateNumber(parseFloat(netEMEAmount).toFixed(2)) : '0.00';

						body += '<tr style="border:1px solid #c0c0c0;font-weight:bold;">';
						body += '<td class="text-center">'+netCtr+'</td>';
						body += '<td></td>';
						body += '<td></td>';
						body += '<td style="font-weight:bold;">GRAND TOTAL</td>';
						body += '<td class="text-right">'+net_ee_amount+'</td>';
						body += '<td class="text-right">'+net_me_amount+'</td>';
						body += '<td class="text-right">'+net_total+'</td>';
						body += '<td></td>';
						body += '<td></td>';
						body += '</tr>';

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';
						signatoryFour = (signatoryFour) ? signatoryFour : '';
						positionFour = (positionFour) ? positionFour : '';
						signatoryFive = (signatoryFive) ? signatoryFive : '';
						positionFive = (positionFive) ? positionFive : '';
						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						addDegree = (addDegree) ? addDegree : '';

						body += '<tr>';
					 	body += '<td style="border:none;" colspan="9" >';
					 	body += '<div style="height:20px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4"> <div style="height:40px;"></div> Certified Correct:</div>';
					 	body += '<div class="col-md-4"></div>';
					 	body += '<div class="col-md-4">';
					 	body +=	'<span><b>Certified:</b> Supporting documents complete and </span>';
						body +=	'<span>proper, and cash available in the amount of</span><br>';
						body +=	'<span>Php _________________________.</span>';
					 	body += '</div>'
					 	body += '</div>'

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signatoryOne+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionOne+'</span>';
					 	body += '</div>'
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signatoryTwo+'</b>'
						body +=	'<br>'
						body +=	'<span>'+positionTwo+'</span>'
					 	body += '</div>'
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signatoryThree+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionThree+'</span>';
					 	body += '</div>'
					 	body += '</div>'

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4"> Approved for Payment</div>';
					 	body += '<div class="col-md-4"></div>';
					 	body += '<div class="col-md-4">';
					 	body +=	'<span><b>Certified:</b> Each employee whose name appears on the payroll has </span>';
						body +=	'<span>been paid the amount as indicated opposite his/her name.</span>';
					 	body += '</div>'
					 	body += '</div>'

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signatoryFour+' '+addDegree+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionFour+'</span>';
					 	body += '</div>'
					 	body += '<div class="col-md-4"></div>';
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signatoryFive+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionFive+'</span>';
					 	body += '</div>'
					 	body += '</div>'

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-12">';
					 	body += '<i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
					 	body += '</div>'
					 	body += '</div>'

					 	body +=	'</td>';

					 	body += '<tbody></table>';



						$('#payroll_transfer').html(body);


						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})
})
</script>
@endsection