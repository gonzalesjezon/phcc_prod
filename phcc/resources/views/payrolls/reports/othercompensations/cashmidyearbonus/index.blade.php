@extends('app-othercompensations')


@section('othercompensations-content')

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 1240px;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
	       				<h5><b>{{ $title }}</b></h5>
	       				<h6><b>Period Covered:</b> <span id="month_year"></span></h6>
	       			</div>
					<table class="table">
						<thead class="text-center">
							<tr >
								<td rowspan="2"><b >#</b></td>
								<td rowspan="2"><b >EMPLOYEE NAME</b></td>
								<td rowspan="2"><b >POSITION</b></td>
								<td rowspan="2"><b >STATUS OF APPOINTMENT</b></td>
								<td rowspan="2"><b >ASSUMPTION</b></td>
								<td rowspan="2"><b>CIVIL STATUS</b></td>
								<td rowspan="2"><b >JG</b></td>
								<td colspan="5">SALARY</td>
								<td rowspan="2"><b >NET PAY</b></td>
							</tr>
							<tr>
								<td><b>BASIC</b></td>
								<td><b>MID YEAR</b></td>
								<td><b>1/2 CASH GIFT</b></td>
								<td><b>GROSS PAY</b></td>
								<td><b>WTAX</b></td>
							</tr>
						</thead>
						<tfoot >
						 	<tr style="border-top:1px solid #c0c0c0;border-bottom:1px solid #c0c0c0;">
						 		<td style="border:none;" colspan="6"></td>
						 		<td style="border:none;" class="text-right"><b>Grand Total</b></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span class="net_basic_pay"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span class="net_basic_pay"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span id="net_cash_gift"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span class="net_gross_pay"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span class="net_gross_pay"></span></td>
						 		<!-- <td style="border:none;font-weight: bold"><span id="net_er_pagibig_share"></span></td> -->
						 <!-- 		<td style="border:none;font-weight: bold;border-right:1px solid #c0c0c0;"><span id="net_pagibig"></span></td> -->
						 	</tr>
						 	<tr class="text-left" >
						 		<td style="border:none;" colspan="3">
							 		Certified Correct
							 		<br><br><br><br><br>
							 		<b>GWEN GRECIA-DE VERA</b>
							 		<br>
							 		<span>Executive Director</span>
						 		</td>
						 		<td  style="border:none;" colspan="5">
							 		Approved for Payment
							 		<br><br><br><br><br>
							 		<b>GWEN GRECIA-DE VERA</b>
							 		<br>
							 		<span>Executive Director</span>
						 		</td>
						 		<td style="border:none;border-right: 1px solid #c0c0c0;" colspan="5">
							 		<span><b>Certified:</b> Supporting documents complete <br>and </span>
							 		<span>proper, and cash available in the amount of</span><br>
							 		<span>Php _________________________.</span><br><br><br>
							 		<span><b>CAROLYN V. AQUINO</b></span><br>
							 		<span>Accountant III, FPMO</span>
						 		</td>
						 	</tr>
					 	</tfoot>
					 <tbody id="tbl_body">
					 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);

					if(data.length !== 0){
						body = [];
						ctr = 0;
						netBasicAmount 	= 0;
						netYearEndAmount 	= 0;
						netCashGiftAmount = 0;
						netGrossPayAmount = 0;
						netTaxAmount 		= 0;
						netNetAmount 		= 0;

						$.each(data,function(key,val){
							subBasicAmount 	= 0;
							subYearEndAmount 	= 0;
							subCashGiftAmount = 0;
							subGrossPayAmount = 0;
							subTaxAmount 		= 0;
							subNetAmount 		= 0;

							body += '<tr style="border:1px solid #c0c0c0;">';
							body += '<td style="border-right:none;"></td>';
							body += '<td  colspan="12" style="border-left:none;"><b>'+key+'</b></td>';
							body += '</tr>';

							$.each(val,function(k,v){

							fullname = v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename;
							position = (v.positions) ? v.positions.Name : '';
							jobGrade = (v.salaryinfo) ? v.salaryinfo.jobgrade.job_grade : '';
							assumptionDate = (v.employeeinformation.assumption_date) ? v.employeeinformation.assumption_date : '';
							basicPay = (v.salaryinfo) ? v.salaryinfo.salary_new_rate : 0;
							yearEndAmount = (v.amount) ? v.amount : 0;
							cashGiftAmount = (v.cash_gift_amount) ? v.cash_gift_amount : 0;


							body += '<tr style="border:1px solid #c0c0c0;" class="text-center">';
								body += '<td >'+(ctr+1)+'</td>';
								body += '<td >&nbsp;'+fullname+'</td>';
								body += '<td >'+position+'</td>';
								body += '<td >'+jobGrade+'</td>';
								body += '<td >'+assumptionDate+'</td>';

							body += '</tr>';
							ctr++;
							});

							ctr = 0;

						});


						$('#tbl_body').html(body);

						$('#month_year').text(_Month+' '+_Year);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}


	});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})
})
</script>
@endsection