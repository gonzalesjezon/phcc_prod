@extends('app-remittances')


@section('remittances-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<div class="panel panel-default" style="padding: 15px;">
		<div class="panel-body">
			@include('payrolls.reports.includes._table-posted')
			<form action="{{ url($module_prefix.'/'.$module) }}" method="post" id="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="signatory_one" id="signatory_one">
				<input type="hidden" name="signatory_two" id="signatory_two">
				<input type="hidden" name="signatory_three" id="signatory_three">
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_one">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_one" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_two">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_two" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2 signatory" id="select_signatory_three">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-id="signatory_three" data-position="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Degree</span>
							<input type="text" name="add_degree" id="add_degree" class="form-control font-style2">
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes._post-button')
			</form>
		</div>
	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 1336px;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports">
	       		<div class="row">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
@include('payrolls.reports.includes._post-button-script')
<script type="text/javascript">
$(document).ready(function(){

	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var signatoryOne;
	var positionOne;
	$('#select_signatory_one').change(function(){
		signatoryOne = "";
		positionOne  = "";
		signatoryOne = $(this).find(':selected').text()
		positionOne = $(this).find(':selected').data('position')

	});

	var signatoryTwo;
	var positionTwo;
	$('#select_signatory_two').change(function(){
		signatoryTwo = "";
		positionTwo  = "";
		signatoryTwo = $(this).find(':selected').text()
		positionTwo = $(this).find(':selected').data('position')
	});

	var signatoryThree;
	var positionThree;
	$('#select_signatory_three').change(function(){
		signatoryThree = "";
		positionThree  = "";
		signatoryThree = $(this).find(':selected').text()
		positionThree = $(this).find(':selected').data('position')
	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	$('.signatory').change(function(){
		id 			= $(this).find(':selected').data('id');
		position 	= $(this).find(':selected').data('position');
		text 		= $(this).find(':selected').text();

		$('#'+id).val(text+'|'+position);
	});

	var addDegree;
	$('#add_degree').on('change',function(){
		addDegree = $(this).val();
	});

	$('.view').on('click',function(){
		_Year 		= $(this).data('year');
		_Month 		= $(this).data('month');
		signOne 	= $(this).data('signatory_one').split('|');
		signTwo 	= $(this).data('signatory_two').split('|');
		signThree 	= $(this).data('signatory_three').split('|');
		addDegree 	= $(this).data('degree');

		signatoryOne 	= signOne[0];
		positionOne 	= signOne[1];

		signatoryTwo 	= signTwo[0];
		positionTwo 	= signTwo[1];

		signatoryThree 	= signThree[0];
		positionThree 	= signThree[1];

		$('#preview').trigger('click');
	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr 		= 1;
						netCtr 		= 0;
						arr_gsis = [];
						arr_subtotal = [];
						own_loan_id = 0;
						gsisLoanCount = (data.gsisLoanCount) ? data.gsisLoanCount : 0;

						count2 = (parseFloat(gsisLoanCount) + 2);
						count1 = (parseFloat(gsisLoanCount) + 7);
						count3 = (parseFloat(gsisLoanCount) + 9);

						netBasicAmount 		 = 0;
						netGsisEEContAmount  = 0;
						netGsisERContAmount  = 0;
						netEccAmount 		 = 0;
						netPSAmount 		 = 0;
						netGSISAmount 		 = 0;
						netGSISECIPAmount 	 = 0;

						coveredPeriod = months[_Month]+' '+_Year

						body += '<table class="table table2" style="margin-top:10px;border:none;">'
						body += '<thead class="text-center" style="font-weight:bold;">';
						body += '<tr>';
						body += '<td class="text-left" colspan="'+count1+'" style="border-left:none;border-right:none;">';
						body += '<img src="{{ url("images/new-logo.jpg") }}" style="height: 60px;"> <br>';
						body += '<b>GSIS PREMIUMS/LOANS AND ECIP CONTRIBUTION</b> <br> BP NUMBER: 1000052031';
						body += '</td>';
						body += '<td colspan="2" style="border-left:none;border-right:none;" class="text-right">';
						body += '<i>HRDD COPY</i> <br> <i>ACCOUNTING COPY</i> <br> <i>CASHIERS COPY</i>'
						body += '</td>';
						body += '</tr>';
						body +=	'<tr>';
						body += '<td rowspan="2">#</td>';
						body += '<td rowspan="2" >EMPLOYEE NAME</td>';
						body += '<td rowspan="2" >BASIC SALARY RECEIVED</td>';
						body += '<td colspan="'+count2+'">PERSONAL SHARE (PS)</td>';
						body += '<td rowspan="2">GOVERNMENT SHARE</td>';
						body += '<td rowspan="2">TOTAL GSIS</td>';
						body += '<td rowspan="2">ECIP</td>';
						body += '<td rowspan="2">TOTAL GSIS AND ECIP</td>';
						body +=	'</tr>';
						body += '<tr>';
						body += '<td>PREMIUM</td>';
						$.each(data.gsisLoanList,function(k,v){
							body += '<td>'+v.loans.name+'</td>';
							arr_gsis[v.loan_id+'_id'] = v.loan_id;
							arr_subtotal[v.loan_id+'_id']  = 0;
						});
						body += '<td>TOTAL PS</td>';
						body += '</tr>';
						body += '</thead>';

						body += '<tbody>';
						$.each(data.transaction,function(key,val){

							body += '<tr>';
							body += '<td></td>'
							body += '<td style="font-weight:bold;" colspan="'+count3+'">'+key+'</td>'
							body += '</tr>';

							subBasicAmount 		 = 0;
							subGsisEEContAmount  = 0;
							subGsisERContAmount  = 0;
							subEccAmount 		 = 0;
							subPSAmount 		 = 0;
							subGSISAmount 		 = 0;
							subGSISECIPAmount 	 = 0;

							$.each(val,function(k,v){

								firstname = (v.employees.firstname) ? v.employees.firstname : '';
								lastname = (v.employees.lastname) ? v.employees.lastname : '';
								middlename = (v.employees.middlename) ? v.employees.middlename : '';

								fullname = lastname+' '+firstname+' '+middlename;

								basicAmount = (v.actual_basicpay_amount) ? v.actual_basicpay_amount : 0;
								gsisEEContAmount = (v.gsis_ee_share) ? v.gsis_ee_share : 0;
								gsisERContAmount = (v.gsis_er_share) ? v.gsis_er_share : 0;
								eccAmount = (v.ecc_amount) ? v.ecc_amount : 0;

								basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '0.00';
								ee_share = (gsisEEContAmount) ? commaSeparateNumber(parseFloat(gsisEEContAmount).toFixed(2)) : '0.00';
								er_share = (gsisERContAmount) ? commaSeparateNumber(parseFloat(gsisERContAmount).toFixed(2)) : '0.00';
								ecc_amount = (eccAmount) ? commaSeparateNumber(parseFloat(eccAmount).toFixed(2)) : '0.00';

								body += '<tr class="text-right">';
								body += '<td class="text-center">'+ctr+'</td>';
								body += '<td class="text-left" nowrap>'+fullname+'</td>';
								body += '<td >'+basic_amount+'</td>';
								body += '<td >'+ee_share+'</td>';

								loanAmount = 0;
								if(data.gsisLoanList.length !== 0){
									$.each(data.gsisLoanList,function(k2,v2){
										loan = [];
										loan_dispay = [];
										loan_id = v2.loan_id;

										$.each(v.loaninfo_transaction,function(k3,v3){
											own_loan_id = v3.loan_id;
											loan_dispay['loan_amount_'+own_loan_id] = v3.amount;

										});

										if(loan_id){
											if (loan_dispay['loan_amount_'+loan_id]) {
												arr_subtotal[loan_id+'_id'] += parseFloat(loan_dispay['loan_amount_'+loan_id]);
												loanAmount += parseFloat(loan_dispay['loan_amount_'+loan_id]);
											}
											loan_dispay['loan_amount_'+loan_id] = (loan_dispay['loan_amount_'+loan_id]) ? commaSeparateNumber(parseFloat(loan_dispay['loan_amount_'+loan_id]).toFixed(2)) : '0.00';

											body += '<td class="sub_total text-right">'+loan_dispay['loan_amount_'+loan_id]+'</td>'
										}else{
											body += '<td class="text-right"></td>'
										}
									});
								}

								totalPSAmount = parseFloat(gsisEEContAmount) + parseFloat(loanAmount);
								totalGSISAmount = parseFloat(totalPSAmount) + parseFloat(gsisERContAmount);
								totalGSISECIPAmount = parseFloat(totalGSISAmount) + parseFloat(eccAmount);



								total_ps_amount = (totalPSAmount) ? commaSeparateNumber(parseFloat(totalPSAmount).toFixed(2)) : '0.00';
								total_gsis_amount = (totalGSISAmount) ? commaSeparateNumber(parseFloat(totalGSISAmount).toFixed(2)) : '0.00';
								total_gsisecip_amount = (totalGSISECIPAmount) ? commaSeparateNumber(parseFloat(totalGSISECIPAmount).toFixed(2)) : '';

								body += '<td >'+total_ps_amount+'</td>';
								body += '<td >'+er_share+'</td>';
								body += '<td >'+total_gsis_amount+'</td>';
								body += '<td >'+ecc_amount+'</td>';
								body += '<td >'+total_gsisecip_amount+'</td>';
								body += '</tr>';

								subBasicAmount += parseFloat(basicAmount);
								subGsisEEContAmount += parseFloat(gsisEEContAmount);
								subGsisERContAmount += parseFloat(gsisERContAmount);
								subEccAmount += parseFloat(eccAmount);
								subPSAmount += parseFloat(totalPSAmount);
								subGSISAmount += parseFloat(totalGSISAmount);
								subGSISECIPAmount += parseFloat(totalGSISECIPAmount);

								ctr++;
							});


							ctr = 1;

							netBasicAmount += parseFloat(subBasicAmount);
							netGsisEEContAmount += parseFloat(subGsisEEContAmount);
							netGsisERContAmount += parseFloat(subGsisERContAmount);
							netEccAmount += parseFloat(subEccAmount);
							netPSAmount += parseFloat(subPSAmount);
							netGSISAmount += parseFloat(subGSISAmount);
							netGSISECIPAmount += parseFloat(subGSISECIPAmount);

							sub_basic_amount = (subBasicAmount) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '0.00';
							sub_ee_share = (subGsisEEContAmount) ? commaSeparateNumber(parseFloat(subGsisEEContAmount).toFixed(2)) : '0.00';
							sub_er_share = (subGsisERContAmount) ? commaSeparateNumber(parseFloat(subGsisERContAmount).toFixed(2)) : '0.00';
							sub_ecc_amount = (subEccAmount) ? commaSeparateNumber(parseFloat(subEccAmount).toFixed(2)) : '0.00';

							sub_ps_amount = (subPSAmount) ? commaSeparateNumber(parseFloat(subPSAmount).toFixed(2)) : '0.00';
							sub_gsis_amount = (subGSISAmount) ? commaSeparateNumber(parseFloat(subGSISAmount).toFixed(2)) : '0.00';
							sub_gsisecip_amount = (subGSISECIPAmount) ? commaSeparateNumber(parseFloat(subGSISECIPAmount).toFixed(2)) : '0.00';


							body += '<tr style="font-weight:bold;" class="text-right">';
							body += '<td class="text-center"></td>';
							body += '<td class="text-center">SUB TOTAL</td>';
							body += '<td >'+sub_basic_amount+'</td>';
							body += '<td >'+sub_ee_share+'</td>';
							loanAmount = 0;
							if(data.gsisLoanList.length !== 0){
								for (var k in arr_gsis){
								    if (typeof arr_gsis[k] !== 'function') {
								    	arr_subtotal[arr_gsis[k]+'_id'] = (arr_subtotal[arr_gsis[k]+'_id'] !== 0) ? commaSeparateNumber(parseFloat(arr_subtotal[arr_gsis[k]+'_id']).toFixed(2)) : '0.00';
								         body += '<td  class="text-right">'+arr_subtotal[arr_gsis[k]+'_id']+'</td>';
								         arr_subtotal[arr_gsis[k]+'_id'] = 0;
								    }
								}
							}

							body += '<td >'+sub_ps_amount+'</td>';
							body += '<td >'+sub_er_share+'</td>';
							body += '<td >'+sub_gsis_amount+'</td>';
							body += '<td >'+sub_ecc_amount+'</td>';
							body += '<td >'+sub_gsisecip_amount+'</td>';
							body += '</tr>';

						});

						net_basic_amount = (netBasicAmount) ? commaSeparateNumber(parseFloat(netBasicAmount).toFixed(2)) : '0.00';
						net_ee_share = (netGsisEEContAmount) ? commaSeparateNumber(parseFloat(netGsisEEContAmount).toFixed(2)) : '0.00';
						net_er_share = (netGsisERContAmount) ? commaSeparateNumber(parseFloat(netGsisERContAmount).toFixed(2)) : '0.00';
						net_ecc_amount = (netEccAmount) ? commaSeparateNumber(parseFloat(netEccAmount).toFixed(2)) : '0.00';

						net_ps_amount = (netPSAmount) ? commaSeparateNumber(parseFloat(netPSAmount).toFixed(2)) : '0.00';
						net_gsis_amount = (netGSISAmount) ? commaSeparateNumber(parseFloat(netGSISAmount).toFixed(2)) : '0.00';
						net_gsisecip_amount = (netGSISECIPAmount) ? commaSeparateNumber(parseFloat(netGSISECIPAmount).toFixed(2)) : '0.00';

						body += '<tr  style="font-weight:bold;" class="style-td text-right">';
						body += '<td class="text-center"></td>';
						body += '<td class="text-center">GRAND TOTAL</td>';
						body += '<td >'+net_basic_amount+'</td>';
						body += '<td >'+net_ee_share+'</td>';
						loanAmount = 0;
						if(data.gsisLoanList.length !== 0){
							loanId = 0;
							$.each(data.gsisLoanList,function(k,v){
								body += '<td class="text-right">'+commaSeparateNumber(parseFloat(v.net_amount).toFixed(2))+'</td>';
							});
						}

						body += '<td >'+net_ps_amount+'</td>';
						body += '<td >'+net_er_share+'</td>';
						body += '<td >'+net_gsis_amount+'</td>';
						body += '<td >'+net_ecc_amount+'</td>';
						body += '<td >'+net_gsisecip_amount+'</td>';
						body += '</tr>';

						signatoryOne = (signatoryOne) ? signatoryOne : '';
						positionOne = (positionOne) ? positionOne : '';
						signatoryTwo = (signatoryTwo) ? signatoryTwo : '';
						positionTwo = (positionTwo) ? positionTwo : '';
						signatoryThree = (signatoryThree) ? signatoryThree : '';
						positionThree = (positionThree) ? positionThree : '';
						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						addDegree = (addDegree) ? addDegree : '';

						body += '<tr>';
						body += '<td colspan="12" style="border:none;padding-top:20px;">&nbsp;</td>';
						body += '</tr>';

						body += '<tr class="text-left">';
						body += '<td colspan="12" style="border:none;">';
						body += '<div class="row" style="padding-top:50px;padding-left:15px;">';
						body += '<div class="col-md-4">';
						body +=  'Certified Correct';
						body += '</div>';
						body += '<div class="col-md-4">';
						body += '</div>';
						body += '<div class="col-md-4">';
						body +=  'Approved for Payment';
						body += '</div></div>';
						body += '</td>';

						body += '<tr class="text-left">';
						body += '<td colspan="12" style="border:none;">';
						body += '<div class="row" style="padding-top:50px;padding-left:15px;">';
						body += '<div class="col-md-4">';
						body +=  signatoryOne+' <br> '+positionOne;
						body += '</div>';
						body += '<div class="col-md-4">';
						body +=  signatoryTwo+' <br> '+positionTwo;
						body += '</div>';
						body += '<div class="col-md-4">';
						body +=  signatoryThree+', '+addDegree+' <br> '+positionThree;
						body += '</div></div>';
						body += '</td>';

						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';

						body += '</tr>';
						body += '<tr class="text-left">';
						body += '<td style="border:none;padding-top:50px;"  class="text-left" colspan="'+(parseInt(count3)+1)+'"></td>';
						body += '</tr>';

						body += '<tr class="text-left">';
						body += '<td colspan="'+(parseInt(count3)+1)+'" style="border:none;">';
						body += '<div class="row" style="padding-top:50px;padding-left:15px;">';
						body += '<div class="col-md-4 text-left">';
						body += '<i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
						body += '</div>';
						body += '</div>';
						body += '</td>';
						body += '</tr>';

						body += '</tbody></table>';
						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}


	});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})

})
</script>
@endsection