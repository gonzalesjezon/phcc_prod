@extends('app-remittances')


@section('remittances-content')


<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 1336px;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid ">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
	       				<h5>GSIS PREMIUMS/LOANS	</h5>
	       				<h5>BP NUMBER: <span id="bp_no"></span>	</h5>
	       				<h6>Month/Year: <span id="month_year"></span></h6>
	       			</div>
					<table class="table">
						<thead class="text-center">
							<tr>
								<td rowspan="2" style="line-height: 50px;">#</td>
								<td rowspan="2" style="line-height: 50px;" >EMPLOYEE NAME</td>
								<td rowspan="2" style="line-height: 50px;" >BASIC SALARY RECEIVED</td>
								<td colspan="7">PERSONAL SHARE (PS)</td>
								<td rowspan="2" style="line-height: 50px;">GOVERNMENT SHARE</td>
								<td rowspan="2" style="line-height: 50px;">ECIP</td>
								<td rowspan="2" style="line-height: 50px;">TOTAL</td>
							</tr>
							<tr>
								<td>PREMIUM</td>
								<td>CONSO</td>
								<td>POLICY</td>
								<td>OPTIONAL LIFE</td>
								<td>EDUCATION LOAN</td>
								<td>ER/CALAMITY</td>
								<td>TOTAL PS</td>
							</tr>
						</thead>
						 <tfoot class="text-center">
						 	<tr style="border-top:1px solid #e3e3e3;border-bottom:1px solid #e3e3e3;">
						 		<td style="border:none;"></td>
						 		<td colspan="2" style="border:none;" class="text-left"><b>Total</b></td>
						 		<td style="border:none; font-weight: bold;"><span id="net_premium"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_consoloan"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_policy"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_optionalife"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_educ"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_emergency"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_ps"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_government_share"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_ecip"></span></td>
						 		<td style="border:none;font-weight: bold;"><span id="net_gsis"></span></td>
						 	</tr>
						 	<tr class="text-left">
						 		<td style="border:none;"></td>
						 		<td colspan="7" style="border:none;" >
						 		Certified Correct
						 		<br><br>
						 		<b>ANTONIA LYNNELY L. BAUTISTA</b>
						 		<br>
						 		<span>Chief Admin Officer, HRDD</span>
						 		</td>
						 		<td colspan="5" style="border:none;">
						 		Approved for Payment
						 		<br>
						 		<br>
						 		<b>GWEN GRECIA-DE VERA</b>
						 		<br>
						 		<span>Executive Director</span>
						 		</td>
						 	</tr>
					 	</tfoot>
						 <tbody id="tbl_body">
						 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);

					if(data.length !== 0){
						arr = [];
						ctr 			= 0;
						net_premium 	= 0;
						net_consoloan 	= 0;
						net_policy 		= 0;
						net_optionalife = 0;
						net_educ 		= 0;
						net_emergency 	= 0;
						net_ps 			= 0;
						net_gsis 		= 0;
						net_ecip 		= 0;

						$.each(data,function(key,val){
							total_salary_rate 				= 0;
							total_gsis_contribution 		= 0;
							total_allowance 				= 0;
							loan_amortization 				= 0;
							total_gross_pay					= 0;
							total_premium					= 0;
							total_consoloan					= 0;
							total_policy_loan				= 0;
							total_optional_life				= 0;
							total_education_loan			= 0;
							total_emergency_loan			= 0;
							total_gsis						= 0;
							total_pagibig_amount			= 0;
							pagibig_contribution 			= 0;
							philhealth_contribution 		= 0;
							total_pagibig2 					= 0;
							total_pagibig 					= 0;
							total_mpl 						= 0;

							policy_amount 					= 0;
							consoloan_amount 				= 0;
							optional_life_amount 			= 0;
							education_loan_amount 			= 0;
							calamity_amount 				= 0;
							gsis_contribution 				= 0;
							net 							= 0;
							net_total 						= 0;
							premium_amount 					= 0;
							employee_percentage 			= 0;
							government_share 				= 0;
							total_government_share 			= 0;
							ecip 							= 0;

							arr += '<tr>';
							arr += '<td style="border-right:none"></td>';
							arr += '<td style="border-left:none" colspan="12"><b>'+key+'</b></td>';
							arr += '</tr>';

							$.each(val,function(k,v){

							arr += '<tr class="border-black">';
								arr += '<td class="border-black text-center" >'+(ctr+1)+'</td>';
								arr += '<td class="border-black">&nbsp;'+v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename+'</td>';
								salary_rate = (v.salaryinfo.salary_new_rate) ? v.salaryinfo.salary_new_rate : 0;

								arr += '<td class="border-black text-center">'+commaSeparateNumber(salary_rate) +'</td>';
								total_salary_rate += salary_rate;

								gsis_contribution = (v.employeeinfo.gsis_contribution) ? v.employeeinfo.gsis_contribution  : 0;
								employee_percentage =  ((gsis_contribution) * .09);
								premium_amount = (parseFloat(gsis_contribution) + parseFloat(employee_percentage));
								total_gsis_contribution += parseFloat(premium_amount);

								premium_amount = (premium_amount) ? parseFloat(premium_amount).toFixed(2) : 0;

								arr += '<td class="border-black text-center">'+commaSeparateNumber(premium_amount) +'</td>';

								$.each(v.employeeinfo.loaninfo,function(k1,v1){
									switch(v1.loans.name){
										case 'CONSOLOAN':
											consoloan_amount = (v1.loan_amortization) ? v1.loan_amortization : 0
										break;
										case 'POLICY LOAN':
											policy_amount = (v1.loan_amortization) ? v1.loan_amortization : 0 ;
										break;
										case 'OPTIONAL LIFE':
											optional_life_amount = (v1.loan_amortization) ? v1.loan_amortization : 0 ;
										break;
										case 'EDUCATION LOAN':
											education_loan_amount = (v1.loan_amortization) ? v1.loan_amortization : 0 ;
										break;
										case 'EMERGENCY/CALAMITY LOAN':
											calamity_amount = (v1.loan_amortization) ? v1.loan_amortization : 0 ;
										break;
									}
								});

								total_consoloan += parseFloat(consoloan_amount);
								consoloan_amount = (consoloan_amount == 0) ? '-' : consoloan_amount;
								arr += '<td class="border-black text-center">'+commaSeparateNumber(consoloan_amount) +'</td>';

								total_policy_loan += parseFloat(policy_amount);
								policy_amount = (policy_amount == 0) ? '-' : policy_amount;
								arr += '<td class="border-black text-center">'+commaSeparateNumber(policy_amount) +'</td>';

								total_optional_life += parseFloat(optional_life_amount);
								optional_life_amount = (optional_life_amount == 0) ? '-' : optional_life_amount;
								arr += '<td class="border-black text-center">'+commaSeparateNumber(optional_life_amount) +'</td>';

								total_education_loan += parseFloat(education_loan_amount);
								education_loan_amount = (education_loan_amount == 0) ? '-' : education_loan_amount;
								arr += '<td class="border-black text-center">'+commaSeparateNumber(education_loan_amount) +'</td>';

								total_emergency_loan += parseFloat(calamity_amount);
								calamity_amount = (calamity_amount == 0) ? '-' : calamity_amount;
								arr += '<td class="border-black text-center">'+commaSeparateNumber(calamity_amount) +'</td>';

								gsis_contribution = (gsis_contribution !== '-') ? gsis_contribution : 0;
								consoloan_amount = (consoloan_amount !== '-') ? consoloan_amount : 0;
								policy_amount = (policy_amount !== '-') ? policy_amount : 0;
								optional_life_amount = (optional_life_amount !== '-') ? optional_life_amount : 0;
								education_loan_amount = (education_loan_amount !== '-') ? education_loan_amount : 0;
								calamity_amount = (calamity_amount !== '-') ? calamity_amount : 0;


								total_loan = (parseFloat(premium_amount) + parseFloat(consoloan_amount) + parseFloat(optional_life_amount) + parseFloat(education_loan_amount) + parseFloat(calamity_amount));

								total_gsis += parseFloat(total_loan);
								total_loan = (total_loan) ? parseFloat(total_loan).toFixed(2) : 0;
								arr += '<td class="border-black text-center">'+commaSeparateNumber(total_loan)+'</td>';

								government_share_percentage = (gsis_contribution * .12);
								government_share = (parseInt(gsis_contribution) + parseInt(government_share_percentage));
								total_government_share += parseFloat(government_share);
								government_share = (government_share) ? parseFloat(government_share).toFixed(2) : 0;
								arr += '<td class="border-black text-center">'+commaSeparateNumber(government_share)+'</td>';
								ecip += parseFloat(100);
								arr += '<td class="border-black text-center">100</td>';

								net = (parseFloat(total_loan) + parseFloat(government_share));
								net_total += parseFloat(net);
								net = (net) ? parseFloat(net).toFixed(2) : 0;
								arr += '<td class="border-black text-center">'+commaSeparateNumber(net)+'</td>';

							arr += '</tr>';
							ctr++;

							});
							ctr = 0;
							arr += '<tr style="border:1px solid #c0c0c0;">';
							arr += '<td style="border:none"></td>';
							arr += '<td colspan="2" style="border-left:none;border-right:none;"><b>Sub Total</b></td>';

							net_premium += parseFloat(total_gsis_contribution);

							total_gsis_contribution = (total_gsis_contribution == 0 ) ? '-' : parseFloat(total_gsis_contribution).toFixed(2);
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(total_gsis_contribution) +'</b></td>';

							net_consoloan += parseFloat(total_consoloan);
							total_consoloan = (total_consoloan) ? parseFloat(total_consoloan).toFixed(2) : '-';
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(total_consoloan) +'</b></td>';

							net_policy += parseFloat(total_policy_loan);
							total_policy_loan = (total_policy_loan) ? parseFloat(total_policy_loan).toFixed(2) : '-';
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(total_policy_loan) +'</b></td>';

							net_optionalife += parseFloat(total_optional_life);
							total_optional_life = (total_optional_life) ? parseFloat(total_optional_life).toFixed(2) : '-';
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(total_optional_life) +'</b></td>';

							net_educ += parseFloat(total_education_loan);
							total_education_loan = (total_education_loan) ? parseFloat(total_education_loan).toFixed(2) : '-';
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(total_education_loan) +'</b></td>';

							net_emergency += parseFloat(total_emergency_loan);
							total_emergency_loan = (total_emergency_loan) ? parseFloat(total_emergency_loan).toFixed(2) : '-';
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(total_emergency_loan) +'</b></td>';

							net_ps += parseFloat(total_gsis)
							total_gsis = (total_gsis) ? parseFloat(total_gsis).toFixed(2) : '-';
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(total_gsis) +'</b></td>';

							net_government_share = parseFloat(total_government_share);
							total_government_share = (total_government_share) ? parseFloat(total_government_share).toFixed(2) : '-';
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(total_government_share) +'</b></td>';
							net_ecip += parseFloat(ecip);
							arr += '<td class="text-center" style="border:none"><b>'+ecip.toFixed(2)+'</b></td>';

							net_gsis += parseFloat(net_total);
							net_total = (net_total) ? parseFloat(net_total).toFixed(2) : '-';
							arr += '<td class="text-center" style="border:none"><b>'+commaSeparateNumber(net_total) +'</b></td>';


							arr += '</tr>';
						});

						net_premium = (net_premium) ? net_premium.toFixed(2) : '-';
						$('#net_premium').text(commaSeparateNumber(net_premium) );
						net_consoloan = (net_consoloan) ? net_consoloan.toFixed(2) : '-';
						$('#net_consoloan').text(commaSeparateNumber(net_consoloan));
						net_policy = (net_policy) ? net_policy.toFixed(2) : '-';
						$('#net_policy').text(commaSeparateNumber(net_policy));
						net_optionalife = (net_optionalife) ? net_optionalife.toFixed(2) : '-';
						$('#net_optionalife').text(commaSeparateNumber(net_optionalife));
						net_educ = (net_educ) ? net_educ.toFixed(2) : '-';
						$('#net_educ').text(commaSeparateNumber(net_educ));
						net_emergency = (net_emergency) ? net_emergency.toFixed(2) : '-';
						$('#net_emergency').text(commaSeparateNumber(net_emergency));
						$('#net_ps').text(net_ps.toFixed(2));
						net_government_share = (net_government_share) ? net_government_share.toFixed(2) : '-';
						$('#net_government_share').text(commaSeparateNumber(net_government_share));
						net_ecip = (net_ecip) ? net_ecip.toFixed(2) : '-';
						$('#net_ecip').text(commaSeparateNumber(net_ecip));
						net_gsis = (net_gsis) ? net_gsis.toFixed(2) : '-';
						$('#net_gsis').text(commaSeparateNumber(net_gsis));

						$('#tbl_body').html(arr);

						$('#month_year').text(_Month+' '+_Year);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}


	});


})
</script>
@endsection