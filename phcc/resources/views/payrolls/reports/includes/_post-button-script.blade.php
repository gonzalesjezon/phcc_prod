<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click','#btn_posted',function(){
			$(this).html('Posting...').prop('disabled',true);
			$('.submit').trigger('click');
		});

		$(document).on('click','#generate_csv',function(){
			let selectedYear 	= $('#select_year').find(':selected').val();
			let selectedMonth = $('#select_month').find(':selected').val();
			let payPeriod 	  = $('#select_pay_period').find(':selected').val();

			$('#_year').val(selectedYear);
			$('#_month').val(selectedMonth);
			$("#_payperiod").val(payPeriod);

			// $.ajax({
			// 	url:base_url+module_prefix+module+'/generateTextFile',
			// 	data:{
			// 		'_token':`{{ csrf_token() }}`,
			// 		'year':selectedYear,
			// 		'month':selectedMonth,
			// 		'pay_period':payPeriod
			// 	},
			// 	type:'POST',
			// 	success:function(res){
			// 		par = JSON.parse(res);

			// 		if(par.status)
			// 		{
			// 			swal({
			// 				  title: 'Text File Generated Successfully!',
			// 				  type: "success",
			// 				  showCancelButton: false,
			// 				  confirmButtonClass: "btn-success",
			// 				  confirmButtonText: "OK",
			// 				  closeOnConfirm: false
			// 			});
			// 		}else{
			// 			swal({
			// 				  title: 'Text File Generate Failed!',
			// 				  text:'No Record Found',
			// 				  type: "warning",
			// 				  showCancelButton: false,
			// 				  confirmButtonClass: "btn-warning",
			// 				  confirmButtonText: "OK",
			// 				  closeOnConfirm: false
			// 			});
			// 		}
			// 	}
			// });
		});
	});
</script>