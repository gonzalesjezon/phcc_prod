<style type="text/css">
.table thead>tr>td{
	font-size: 12px !important;
}

.table tbody>tr>td{
	font-size: 12px !important;
}

.tr td{
	border-bottom: none;
}
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
</style>
<div class="row" style="margin-left: -5px;margin-right: -5px;">
	<div class="col-md-12">
		<div class="form-group">
			<table class="table noborder border datatable" id="tblPosted">
				<thead>
					<tr>
						<th>Date Posted</th>
						<th>Transaction Date</th>
						<th>Pay Period</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($posted as $key => $value)
					<tr class="text-center">
						<td>{{$value->created_at}}</td>
						<td>{{date("F", strtotime($value->year."-".$value->month."-01"))}} {{$value->year}}</td>
						<td>{{ @$value->pay_period }}</td>
						<td>
							<a class="btn btn-success btn-xs view" data-employee_id="{{$value->employee_id}}" data-year="{{$value->year}}" data-month="{{$value->month}}" data-report_type="{{$value->report_type}}" data-signatory_one="{{@$value->signatory->signatory_one}}" data-signatory_two="{{@$value->signatory->signatory_two}}" data-signatory_three="{{@$value->signatory->signatory_three}}" data-signatory_four="{{@$value->signatory->signatory_four}}" data-signatory_five="{{@$value->signatory->signatory_five}}" data-signatory_six="{{@$value->signatory->signatory_six}}" data-pay_period="{{$value->pay_period}}" data-semi_pay_period="{{$value->semi_pay_period}}" data-degree="{{ @$value->signatory->add_degree }}" data-monthno="{{$value->month}}">View</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var table = $('#tblPosted').DataTable({
			'paging':true,
			"oLanguage": {
				"oPaginate": {
				"sFirst": "<<", // This is the link to the first page
				"sPrevious": "<", // This is the link to the previous page
				"sNext": ">", // This is the link to the next page
				"sLast": ">>" // This is the link to the last page
				}
			}
		});
	})
</script>