@extends('app-front')

@section('content')
<style type="text/css">
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
.panel{
	padding: 25px;
}

</style>
<div class="row" style="padding: 40px 10px 0px 10px;">
	<div class="col-md-12">
		{!! $controller->showInitialSalary() !!}
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<input type="text" name="filter_search" class="form-control search1" placeholder="Search here">
		<div style="height: 5px;"></div>
		<div class="sub-panelnamelist ">
			{!! $controller->show() !!}
		</div>
	</div>

	<div class="col-md-9">
		<div class="row" style="margin-left: 2px;">
			<div class="col-md-12">
				<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="newSalary" data-btnnew="newSalary" data-btncancel="cancelSalary" data-btnedit="editSalary" data-btnsave="saveSalary"><i class="fa fa-save"></i> New</a>

					<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editSalary" data-btnnew="newSalary" data-btncancel="cancelSalary" data-btnedit="editSalary" data-btnsave="saveSalary"><i class="fa fa-save"></i> Edit</a>

					<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newSalary" data-btncancel="cancelSalary" data-btnedit="editSalary" data-btnsave="saveSalary" id="saveSalary"><i class="fa fa-save"></i> Save</a>
					<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newSalary" data-btncancel="cancelSalary" data-form="myform" data-btnedit="editSalary" data-btnsave="saveSalary"id="cancelSalary"> Cancel</a>
				</div>
			</div>
		</div>
		<div class="row" style="padding: 20px;">
			<div class="col-md-12">
			<label id="employee_name" style="margin-left:5px; "></label>
			<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="employee_id" id="employee_id">
				<input type="hidden" name="transaction_id" id="transaction_id">
				<div class="col-md-5">
					<div class="panel">
						<table class="table noborder borderless">
							<tbody class="noborder-top">
								<tr>
									<td colspan="2">
										<span>Transaction Period</span>
									</td>
								</tr>
								<tr class="newSalary">
									<td colspan="2">
										<div class="col-md-6">
											<select class="employee-type form-control font-style2 select2" id="select_month" name="month" placeholder="Year" >
												<option value=""></option>
											</select>
										</div>
										<div class="col-md-6">
											<select class="employee-type form-control font-style2 select2" id="select_year" name="year" placeholder="Month">
												<option value=""></option>
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td>From</td>
									<td>To</td>
								</tr>
								<tr class="newSalary">
									<td>
										<input type="text" name="date_from" id="date_from" class="form-control font-style2 datepicker">
									</td>
									<td>
										<input type="text" name="date_to" id="date_to" class="form-control font-style2 datepicker">
									</td>
								</tr>
								<tr>
									<td>Actual Work Days</td>
									<td>
										<input type="text" name="actual_workdays" id="actual_workdays" class="form-control font-style2 onlyNumber actual_workdays" maxlength="3" readonly>
									</td>
								</tr>
								<tr>
									<td>Basic Pay</td>
									<td>
										<input type="text" name="basic_amount" id="basic_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td>Pera</td>
									<td>
										<input type="text" name="pera_amount" id="pera_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td>Total</td>
									<td>
										<input type="text" name="total_amount" id="total_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel">
						<table class="table noborder borderless">
							<tbody class="noborder-top">
								<tr>
									<td>Gross Pay</td>
									<td>
										<input type="text" name="gross_amount" id="gross_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<span>Less</span>
									</td>
								</tr>
								<tr>
									<td>Gsis Contribution</td>
									<td>
										<input type="text" name="gsis_cont_amount" id="gsis_cont_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td>Philhealth Contribution</td>
									<td>
										<input type="text" name="philhealth_cont_amount" id="philhealth_cont_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td>Pagibig Contribution</td>
									<td>
										<input type="text" name="pagibig_amount" id="pagibig_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td>Withholding Tax</td>
									<td>
										<input type="text" name="tax_amount" id="tax_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td>Total Deductions</td>
									<td>
										<input type="text" name="total_deduction_amount" id="total_deduction_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td>Net Amount</td>
									<td>
										<input type="text" name="net_amount" id="net_amount" class="form-control font-style2" readonly>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</form>
			</div>
		</div>
	</div>
</div>
<!-- <div class="ajax-loader">
  <img src="{{ asset('images/ajax-loader1.gif') }}" class="img-responsive" />
</div> -->
<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option value="'+month[m]+'"">'+month[m]+'</option>';
}
$('#select_month').html(mArr);


// ************************************************
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	$('#year').val(_Year);

})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	$('#month').val(_Month);
})

$('.select2').select2();

$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});

	var _monthlyRate;
	var taxAmountBR;
	var _taxDue;

	$('.newSalary :input').attr('disabled',true);
	$('.newSalary').attr('disabled',true);

	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
		_taxDue = 0;
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btndelete).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('#'+btndelete).addClass('hidden');
		$('.weekly').addClass('hidden');
		$('.semi-monthly').addClass('hidden');

		$('#employee_name').text('');
		$('#otherpayroll_id').val('');
		form = $(this).data('form');
		clear_form_elements(form);
		clear_form_elements('nonplantilla');
		$('.error-msg').remove();

	});

	$('.select2').select2();

	$('.onlyNumber').keypress(function (event) {
		return isNumber(event, this)
	});

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});

	var firstAmount = 0;
    var secondAmount = 0;
    var thirdAmount = 0;
    var fourthAmount = 0;
    $(document).on('keyup','#first_amount',function(){
    	firstAmount = $(this).val().replace(/\,/g,'');
    	compute_deductions(firstAmount,secondAmount,thirdAmount,fourthAmount);
    });
    $(document).on('keyup','#second_amount',function(){
    	secondAmount = $(this).val().replace(/\,/g,'');
    	compute_deductions(firstAmount,secondAmount,thirdAmount,fourthAmount);
    });
    $(document).on('keyup','#third_amount',function(){
    	thirdAmount = $(this).val().replace(/\,/g,'');
    	compute_deductions(firstAmount,secondAmount,thirdAmount,fourthAmount);
    });
    $(document).on('keyup','#fourth_amount',function(){
    	fourthAmount = $(this).val().replace(/\,/g,'');
    	compute_deductions(firstAmount,secondAmount,thirdAmount,fourthAmount);
    });

    function compute_deductions(first,second,third,fourth){
    	totalAmount = parseFloat(first) + parseFloat(second) + parseFloat(third) + parseFloat(fourth);

    	total_amount = (totalAmount) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';

    	$('#total_deduction').val(total_amount);
    }

	// DATE PICKER
	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});

	var actualWorkdays = 0;
	$('#actual_workdays').keyup(function(){
		actualWorkdays = $(this).val();
		$('#date_to').trigger('change');
	})



// =============================================== //
// ===== SALARY ADJUSTMENT COMPUTATION ========== //
// ============================================= //

var daysInMonth;
var dateFrom;
var dateTo;
var countedDays;
var diffAmount;
var newPhContAmount;
var basicAmount;
var gsisContribution;
var totalDeduction;
var netAmount;
var oldPhicAmount;
var monthlyRate = 0;
var peraAmount = 0;

$(document).on('change','#select_year',function(){

	$.ajax({
		url:base_url+module_prefix+module+'/getDaysInAMonth',
		data:{
			'year':_Year,
			'month':_Month,
		},
		type:'get',
		dataType:'JSON',
		success:function(res){
			daysInMonth = res.workdays;
		}
	})
});

$(document).on('change','#date_from',function(){
	dateFrom = $(this).val();
	$.ajax({
		url:base_url+module_prefix+module+'/getCountedDays',
		data:{
			'date_from':dateFrom,
			'date_to':dateTo,
		},
		type:'get',
		dataType:'JSON',
		success:function(res){
			countedDays = res.counted_days;

			$('.actual_workdays').val(countedDays);

			basicAmount = compute_basic(countedDays,monthlyRate);
			peraAmount = compute_pera(countedDays,peraAmount);


		}
	})

	$('#date_to').val(dateFrom);
});

$(document).on('change','#date_to',function(){
	dateTo = $(this).val();

	$.ajax({
		url:base_url+module_prefix+module+'/getCountedDays',
		data:{
			'date_from':dateFrom,
			'date_to':dateTo,
		},
		type:'get',
		dataType:'JSON',
		success:function(res){

			dateOne = dateFrom.split('-')[2];
			dateTwo = dateTo.split('-')[2];

			var ctr2 = 0;
			for(i = parseInt(dateOne); i <= parseInt(dateTwo); i++){
				ctr2++;
			}

			countedDays = res.counted_days;
			$('.actual_workdays').val(countedDays);
			basicAmount = compute_basic(countedDays,monthlyRate);
			peraAmount = compute_pera(countedDays,peraAmount);
			gsisContribution = compute_gsisCont(ctr2,daysInMonth,monthlyRate);
			pagibigAmount = 100;
			varPh = compute_philhealth(monthlyRate);

			eePhAmount = varPh['ee_share'];
			erPhAmount = varPh['er_share'];

			gsis_cont_amount = (gsisContribution) ? commaSeparateNumber(parseFloat(gsisContribution).toFixed(2)) : '';
			ph_cont_amount   = (erPhAmount) ? commaSeparateNumber(parseFloat(erPhAmount).toFixed(2)) : '';
			pagibig_amount   = (pagibigAmount) ? commaSeparateNumber(parseFloat(pagibigAmount).toFixed(2)) : '';

			basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
			pera_amount = (peraAmount) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';

			totalAmount = parseFloat(basicAmount) + parseFloat(peraAmount);
			total_amount = (totalAmount) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';

			totalDeduction = parseFloat(gsisContribution) + parseFloat(eePhAmount) + parseFloat(pagibigAmount);

			grossSalary = parseFloat(basicAmount) - parseFloat(totalDeduction);

			$.ajax({
				url:base_url+module_prefix+module+'/getTax',
				data:{
					'gross_salary':grossSalary,
				},
				type:'get',
				dataType:'JSON',
				success:function(tax){
					taxAmount = tax;

					wTaxAmount = parseFloat(totalDeduction) + parseFloat(taxAmount);
					netPay = parseFloat(totalAmount) - parseFloat(wTaxAmount);

					tax_amount = (taxAmount) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
					total_deduction = (wTaxAmount) ? commaSeparateNumber(parseFloat(wTaxAmount).toFixed(2)) : '';
					net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';


					$('#tax_amount').val(tax_amount);
					$('#total_deduction_amount').val(total_deduction);
					$('#net_amount').val(net_pay);
				}
			});


			$('#basic_amount').val(basic_amount);
			$('#pera_amount').val(pera_amount);
			$('#total_amount').val(total_amount);
			$('#gross_amount').val(total_amount);
			$('#gsis_cont_amount').val(gsis_cont_amount);
			$('#philhealth_cont_amount').val(ph_cont_amount);
			$('#pagibig_amount').val(pagibig_amount);

		}
	})
});


function compute_wtax(salary_adjustment,gsis_cont,phic_amount){
	wtax = (parseFloat(salary_adjustment) - parseFloat(gsis_cont) - parseFloat(phic_amount)) * .25;
	return wtax;
}

function compute_philhealth(salary){


	if(salary >= 41999){
		ee_share = 550;
		er_share = 550;
	}else{
		var phicEEContAmount = 0;
		var phicERContAmount = 0;

		phicAmount = (parseFloat(salary)*0.0275);
		phicAmount = phicAmount.toFixed(2);
		newAmount = parseFloat(phicAmount) / 2;
		newAmount = newAmount.toFixed(2);
		checkAmount = parseFloat(newAmount) + parseFloat(newAmount);

		if(checkAmount != phicAmount){
			er_share = parseFloat(newAmount) + 0.01;
			ee_share = newAmount;
		}else{
			er_share = newAmount;
			ee_share =  newAmount;
		}

	}

	arr = Array();
	arr['ee_share'] = ee_share;
	arr['er_share'] = er_share;

	return arr;

}


function compute_basic(actual_workdays,basic_amount){
	days = actual_workdays / 22;
	salary = parseFloat(basic_amount) * days ;
	return salary;
}

function compute_pera(actual_workdays,pera_amount){
	days = actual_workdays / 22;
	pera = parseFloat(pera_amount) * days ;

	return pera;
}

function compute_gsisCont(count_days,work_days,basic_amount){
	console.log(count_days+' '+work_days+' '+basic_amount)
	qou_amount = (parseFloat(basic_amount)/work_days);
	pro_amount = (parseFloat(qou_amount)*count_days);
	gsis_cont  = (parseFloat(pro_amount)*.09);

	return gsis_cont
}

function compute_totalDeduction(gsis_cont,ph_cont){
	gsis_cont = (gsis_cont) ? gsis_cont : 0;
	ph_cont = (ph_cont) ? ph_cont : 0;

	deduction_amount = (parseFloat(gsis_cont) + parseFloat(ph_cont));

	return deduction_amount;
}

function compute_netAmount(adjustment,deductions){
	adjustment = (adjustment) ? adjustment : 0;
	deductions = (deductions) ? deductions : 0;

	net_amount = (parseFloat(adjustment) - parseFloat(deductions));

	return net_amount;
}

function compute_salaryDiff(oldRate,newRate){
	amount = parseFloat(newRate) - parseFloat(oldRate);

	return amount;
}


var tAdjustment = $('#tbl_initial_salary').DataTable();

$(document).on('click','#namelist tr',function(){

	employee_id = $(this).data('empid');
	$('#employee_id').val(employee_id);
	fullname = $(this).data('fullname');
	$('#employee_name').text(fullname);

	$.ajax({
		url:base_url+module_prefix+module+'/getInitialSalary',
		data:{
			'id':employee_id
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			clear_form_elements('myform');
			$('.error-msg').text('');
			$('.btn_edit').addClass('hidden');
			$('.btn_cancel').addClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_new').removeClass('hidden');
			tAdjustment.clear().draw();
			if(data !== null){

				monthlyRate = (data.new_rate_amount) ? data.new_rate_amount : 0;
				peraAmount =(data.pera_amount) ? data.pera_amount : 0;

			}

			if(data.initialsalary.length !== 0){

				datatable(data);

			}

		}
	});
});

function datatable(data){
	tAdjustment.clear().draw();
	$.each(data.initialsalary,function(k,v){

		grossAmount = (v.gross_amount) ? v.gross_amount : 0;
		totalDeductionAmount = (v.total_deduction_amount) ? v.total_deduction_amount : 0;
		netAmount = (v.net_amount) ? v.net_amount : 0;
		year = (v.year) ? v.year : '';
		month = (v.month) ? v.month : '';
		for_month = month+' '+year;

		gross_amount = (grossAmount !== 0) ? commaSeparateNumber(parseFloat(grossAmount).toFixed(2)) : '';
		deduction_amount = (totalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(totalDeductionAmount).toFixed(2)) : '';
		net_amount = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';

		tAdjustment.row.add( [
			for_month,
        	gross_amount,
        	deduction_amount,
        	net_amount,
        	'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteInitialSalary" data-id="'+v.id+'"  data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'
        ]).draw( false );

        tAdjustment.rows(k).nodes().to$().attr("data-id", v.id);
        tAdjustment.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
        tAdjustment.rows(k).nodes().to$().attr("data-basic_amount", v.basic_amount);
        tAdjustment.rows(k).nodes().to$().attr("data-gsis_cont_amount", v.gsis_cont_amount);
        tAdjustment.rows(k).nodes().to$().attr("data-philhealth_cont_amount", v.philhealth_cont_amount);
        tAdjustment.rows(k).nodes().to$().attr("data-provident_fund_amount", v.provident_fund_amount);
        tAdjustment.rows(k).nodes().to$().attr("data-actual_workdays", v.actual_workdays);
        tAdjustment.rows(k).nodes().to$().attr("data-date_from", v.date_from);
        tAdjustment.rows(k).nodes().to$().attr("data-date_to", v.date_to);
        tAdjustment.rows(k).nodes().to$().attr("data-year", v.year);
        tAdjustment.rows(k).nodes().to$().attr("data-month", v.month);
        tAdjustment.rows(k).nodes().to$().attr("data-btnnew", "newSalary");
        tAdjustment.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
        tAdjustment.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
        tAdjustment.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
	});
}

//SUBMIT FORM
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);
	form = $(this).data('form');

		$('#'+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							// $('.btn_cancel').trigger('click');
							window.location.href = base_url+module_prefix+module;


						});// end swal

				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}// end of main IF STATUS

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});


var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	category  	= $('#select_searchvalue :selected').val();
	empstatus   = $('#emp_status :selected').val();
	emp_type    = $('#emp_type :selected').val();
	searchby    = $('#searchby :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {'category':category,'empstatus':empstatus,'emp_type':emp_type,'searchby':searchby },
					   beforeSend:function(){
					   		// $('#loading').removeClass('hidden');
					   },
					   complete:function(){
					   		// $('#loading').addClass('hidden');
					   },
					   success: function(res){
					      $(".sub-panelnamelist").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


$(document).on('keyup','.search1',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {"q":$('.search1').val(),'limit':$(".limit").val()},
			   beforeSend:function(){

			   },
			   success: function(res){
			      $(".sub-panelnamelist").html(res);

			   },
			   complete:function(){

			   }
			});
		},500);
});


$(document).on('click','.delete_item',function(){
	id 				= $(this).data('id');
	date 			= $(this).data('transaction_date');
	employee_id 	= $(this).data('employee_id');
	function_name 	= $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){

				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'date':date,
						'employee_id':employee_id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(data){

						swal({
							  title: 'Delete Successfully',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						})

						datatable(data);
					}

				})

			}else{
				return false;
			}
		});
	}
})

})
</script>
@endsection
